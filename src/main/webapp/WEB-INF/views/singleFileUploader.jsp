<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
 <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Obaju : e-commerce template
    </title>

    <meta name="keywords" content="">

</head>
<body>

	<div class="form-container">
		<h1>Spring 4 MVC File Upload Example</h1>
		<form:form action="singleupload?${_csrf.parameterName}=${_csrf.token}" method="POST" modelAttribute="fileBucket" enctype="multipart/form-data">

			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="file">Upload a file</label>
					<div class="col-md-7">
						<form:input type="file" path="file" id="file" class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="file" class="help-inline" />
						</div>
						<form:input path="nombre" id="nombre" class="form-control input-sm" />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="form-actions floatRight">
<!-- 					<input type="submit" value="Upload" class="btn btn-primary btn-sm"> -->
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					<button type="submit" name="Save" value="Save" class="btn btn-primary"><i class="fa fa-save"></i> Guardar cambios</button>
				</div>
			</div>
		</form:form>
		<a href="<c:url value='/welcome' />">Home</a>
	</div>
</body>
</html>