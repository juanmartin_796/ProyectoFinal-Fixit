<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<script type="text/javascript">
	window.onload = function() {
		$("#liMisUrgencias").attr("class", "active");
	}
</script>
</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Mis urgencias</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9" id="customer-orders">
					<div class="box">
						<h1>Mis servicios urgentes pedidos</h1>

						<!--                         <p class="lead">Seleccione los servicios urgentes a los cuales puede atender inmediatamente.</p> -->
						<p class="text-muted">�ste es tu historial de servicios
							urgentes que solicitaste</p>

						<hr>

						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Problema</th>
										<th>Categoria</th>
										<th>A menos de</th>
										<th>Solicitado</th>
										<th>Finaliza</th>
										<th>Estado</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaUrgentes}" var="servicioUrgente">
										<c:choose>
											<c:when
												test="${servicioUrgente.notificacionLeida== false and servicioUrgente.especialista.id!= null}">
												<tr bgcolor="#FFFF99">
											</c:when>
											<c:otherwise>
												<tr>
											</c:otherwise>
										</c:choose>

										<c:choose>
											<c:when test="${servicioUrgente.especialista.id != null}">
												<td>${servicioUrgente.descripcion}</td>
											</c:when>
											<c:otherwise>
												<td><a
													href="publicar-servicio-urgente-${servicioUrgente.id}">${servicioUrgente.descripcion}</a></td>
											</c:otherwise>
										</c:choose>
										<td>${servicioUrgente.categoria.nombre}</td>
										<th>${servicioUrgente.distanciaMax}Km</th>
										<td>Hace ${servicioUrgente.diferenciaHoraActual}</td>
										<td><fmt:formatDate var="tiempoLimit"
												value="${servicioUrgente.tiempoLimiteParaRespuesta}"
												pattern="dd/MM/yyyy HH:mm" /> <jsp:useBean id="now"
												class="java.util.Date" /> <fmt:formatDate var="actual"
												value="${now}" pattern="dd/MM/yyyy HH:mm" /> <c:choose>
												<c:when test="${tiempoLimit le actual }">
													<span class="label label-danger"> ${tiempoLimit} </span>
												</c:when>
												<c:otherwise>
													<span class="label label-success"> ${tiempoLimit} </span>
												</c:otherwise>
											</c:choose></td>





										<c:choose>
											<c:when
												test="${tiempoLimit le actual  and servicioUrgente.especialista.id == null}">
												<td><a
													title="Solicitud caducada. Nadie respondio a tu urgencia"
													class="btn btn-primary btn-sm"> <i
														class="fa fa-clock-o"> </i>
												</a></td>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${servicioUrgente.especialista.id != null}">
														<td><a title="Aceptado"
															class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
															<a
															href="mensaje-urgente-${servicioUrgente.especialista.id}-${servicioUrgente.id}"
															title="Aceptado por ${servicioUrgente.especialista.user.username}. Click para enviar mensaje">
																<i class="fa fa-envelope"></i>
														</a></td>
													</c:when>
													<c:otherwise>
														<td><a
															title="Aun no han aceptado tu solicitud de urgencia"
															id="botonReloj${servicioUrgente.id}"
															class="btn btn-warning btn-sm"> <i
																class="fa fa-clock-o"> </i>
														</a> <a title="Eliminar solicitud de urgencia"
															id="botonEliminar${servicioUrgente.id}" href="#"
															data-toggle="modal"
															onclick="setValorBotonEliminarModal(this); return false;"
															value="eliminar-urgente-pedido-${servicioUrgente.id}.html"
															data-target="#confirm-delete"> <i
																id="iconbotonEliminar${servicioUrgente.id}"
																class="fa fa-times"> </i>
														</a></td>


														<!-- 														<td class=""><a href="#" cass="btn-warning btn-sm">Espera</a> -->
														<!-- 															<a -->
														<!-- 															onclick="setValorBotonEliminarModal(this); return false;" -->
														<%-- 															value="eliminar-urgente-pedido-${servicioUrgente.id}.html" --%>
														<!-- 															href="#" data-toggle="modal" -->
														<!-- 															data-target="#confirm-delete" title="Eliminar" -->
														<!-- 															class="btn-danger btn-sm"> X </a></td> -->
													</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***____________________ -->
		<%@include file="footer.jsp"%>

		<div class="modal fade" id="confirm-delete" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">Eliminar urgencia</div>
					<div class="modal-body">�Est� seguro que desea eliminar la
						solicitud de urgencia?</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a href="#" id="buttonEliminarModal" class="btn btn-danger btn-ok">Eliminar</a>
					</div>
				</div>
			</div>
		</div>
</body>

<script type="text/javascript">
	function setValorBotonEliminarModal(obj) {
		$("#buttonEliminarModal").attr("href", obj.getAttribute("value"));
		return false;
	}
</script>

<script type="text/javascript">
	function cambiarIconoaEliminar(obj) {
		$("#icon" + obj.getAttribute("id")).attr("class", "fa fa-times");
		//$("#"+obj.getAttribute("id")).attr("class", "btn btn-primary btn-sm")
	}
	function cambiarIconoaReloj(obj) {
		$("#icon" + obj.getAttribute("id")).attr("class", "fa fa-clock-o");
		//$("#"+obj.getAttribute("id")).attr("class", "btn btn-primary btn-sm")
	}
</script>
</html>

