<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">
<link rel="stylesheet" href="css/chat.css">
</head>
<body>
	<div id="divMensajes" style="position: relative; height: 60vh; overflow-y: scroll;">
		<div class="menuChat">
			<div class="back" onclick="$('#chatContactos').show(1000); $('#chat').hide(1000); clearTimeout(ajaxRefreshMensajesDePublicacion); clearTimeout(ajaxRefreshMensajes); window.setTimeout('location.reload()', 1000);">
				<i class="fa fa-chevron-left"></i> <img
					src="https://i.imgur.com/DY6gND0.png" draggable="false" />
			</div>
			<c:choose>
				<c:when test="${rolLogueado== 'Especialista'}">
					<div class="name">${listaMensajes[0].cliente.user.username}</div>
					<div class="last">${listaMensajes[0].asunto}</div>
				</c:when>
				<c:otherwise>
					<div class="name">${listaMensajes[0].especialista.user.username}</div>
					<div class="last">${listaMensajes[0].asunto}</div>
				</c:otherwise>
			</c:choose>
		</div>
		<ol class="chat" id="mensajesRefresh">
 		</ol>
		<form:form action="enviar-mensaje" type="POST" modelAttribute="mensajeNuevo" name="mensajeNuevo" id="mensajeNuevo" acceptCharset="ISO-8859-1">
			<form:input path="cuerpo" id="textAreaMensaje" class="textarea" type="text"
				placeholder="Escriba su mensaje aqui" required="required"></form:input>
				<form:hidden path="especialista.id"/>
				<form:hidden path="cliente.id" />
				<form:hidden path="remitente" />
				<form:hidden path="asunto" />
				<form:hidden path="urgencia"/>
				<form:hidden path="servicioUrgente.id"/>
				<form:hidden path="servicio.id"/>
				<form:hidden path="publicacion.id"/>	
		</form:form>
	</div>
</body>

<script type="text/javascript">
	
$('#mensajeNuevo').on("submit",function(e) {
    e.preventDefault(); // cancel the actual submit
    enviarMensaje();
    document.getElementById("textAreaMensaje").value = "";
  });
	
	function enviarMensaje() {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
// 		var text = document.getElementById("textAreaMensaje").value;
// 		//var idServicio = $('#buttonEliminarModal').attr("name");
// 		var datos = {
// 				id : idServicio,
// 				comentario : text
// 			};
		var str = $("#mensajeNuevo").serialize();
		$.ajax({
				type : "POST",
				//contentType : "application/json",
				url : 'enviar-mensaje',
				//data : JSON.stringify(datos),
				//data :  $('form[name=mensajeNuevo]').serialize(),
				data : str,
				dataType : 'json',
				timeout : 100000,
				beforeSend: function(request) {
				    request.setRequestHeader(header, token);
				 	},
				success : function(datos) {
					console.log("SUCCESS: ", datos);
					//refreshMensajes(obj);
					var objDiv = document.getElementById("divMensajes");
					objDiv.scrollTop = objDiv.scrollHeight;
				},
				error : function(e) {
					console.log("ERROR: ", e);
				},
				done : function(e) {
					console.log("DONE");
				}
			});
	}
	
	if (${listaMensajes[0].servicioUrgente!= null}){
		refreshMensajes(${listaMensajes[0].servicioUrgente.id});
	} else if (${listaMensajes[0].publicacion!=null}){
		refreshMensajesDePublicacion(${listaMensajes[0].publicacion.id});
	}
	var ajaxRefreshMensajes;
	function refreshMensajes(obj) {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
// 		var text = document.getElementById("textAreaComentario").value;
// 		var idServicio = $('#buttonEliminarModal').attr("name");
		//var idUrgencia = obj.getAttribute("value");
		var idUrgencia = obj;
		var datos = {
			id : idUrgencia
		};
		$.ajax({
			type : "GET",
			//contentType : "application/json",
			url : 'chat-refresh-'+idUrgencia,

			//dataType : 'json',
			//timeout : 100000,
			async:true,
			beforeSend : function(request) {
				request.setRequestHeader(header, token);
			},
			success : function(datos) {
				console.log("SUCCESS: ", datos);
				$('#chatContactos').hide(1000);
				$("#mensajesRefresh").html( datos );
				//window.setTimeout('ajaxRecuperarMensajes('+idUrgencia+')', 5000)
				var objDiv = document.getElementById("divMensajes");
				console.log("SCROLLBAR", objDiv.scrollTop)
				console.log("SCROLLBAR", objDiv.scrollHeight)
				console.log("SCROLLBAR", objDiv.scrollTop - objDiv.scrollHeight)
				if (objDiv.scrollTop - objDiv.scrollHeight > -600){
					objDiv.scrollTop = objDiv.scrollHeight;
				}
				ajaxRefreshMensajes= window.setTimeout('refreshMensajes('+idUrgencia+')', 3000)
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
	
	var ajaxRefreshMensajesDePublicacion;
	
	function refreshMensajesDePublicacion(obj) {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
// 		var text = document.getElementById("textAreaComentario").value;
// 		var idServicio = $('#buttonEliminarModal').attr("name");
		//var idUrgencia = obj.getAttribute("value");
		var idPublicacion = obj;
		var datos = {
			id : idPublicacion
		};
		$.ajax({
			type : "GET",
			//contentType : "application/json",
			url : 'chat-refresh-publicacion-'+idPublicacion,

			//dataType : 'json',
			//timeout : 100000,
			async:true,
			beforeSend : function(request) {
				request.setRequestHeader(header, token);
			},
			success : function(datos) {
				console.log("SUCCESS: ", datos);
				$('#chatContactos').hide(1000);
				$("#mensajesRefresh").html( datos );
				//window.setTimeout('ajaxRecuperarMensajes('+idUrgencia+')', 5000)
				var objDiv = document.getElementById("divMensajes");
				console.log("SCROLLBAR", objDiv.scrollTop)
				console.log("SCROLLBAR", objDiv.scrollHeight)
				console.log("SCROLLBAR", objDiv.scrollTop - objDiv.scrollHeight)
				if (objDiv.scrollTop - objDiv.scrollHeight > -600){
					objDiv.scrollTop = objDiv.scrollHeight;
				}
				ajaxRefreshMensajesDePublicacion= window.setTimeout('refreshMensajesDePublicacion('+idPublicacion+')', 3000)
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
	
</script>

</html>

