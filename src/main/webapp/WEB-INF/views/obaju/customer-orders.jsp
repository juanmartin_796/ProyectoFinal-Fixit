<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Obaju : e-commerce template
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">

<script type="text/javascript">
		window.onload=function() {
			$("#liServiciosUrgentes").attr("class", "active");
		}
	</script>

</head>

<body>
	<%@include file="header.jsp" %>
	
    <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Servicios urgentes</li>
                    </ul>

                </div>

                <%@include file="menu-lateral.jsp"%>

                <div class="col-md-9" id="customer-orders">
                    <div class="box">
                        <h1>Servicios urgentes sin atender</h1>

                        <p class="lead">Seleccione los servicios urgentes a los cuales puede atender inmediatamente.</p>
                        <p class="text-muted"> Una vez aceptado un trabajo, deber� acudir lo antes posible al mismo, caso contrario podr�a ser penalizado con el bloqueo de su cuenta</p>

                        <hr>

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                    	<th>Problema</th>
                                        <th>A menos de</th>
                                        <th>Solicitado</th>
                                        <th>Finaliza el</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<c:forEach items="${listaUrgentes}" var="servicioUrgente">
	                                    <tr>
	                                    	<td><a href="#">${servicioUrgente.descripcion}</a></td>
	                                        <th>${servicioUrgente.distanciaMax} Km</th>
	                                        <td>Hace ${servicioUrgente.diferenciaHoraActual}</td>
	                                        <td><span class="label label-info">
	                                        <fmt:formatDate value="${servicioUrgente.tiempoLimiteParaRespuesta}" pattern="dd/MM/yyyy HH:mm"/>
	                                         </span>
	                                        </td>
	                                        <c:choose>
		                                        <c:when test="${servicioUrgente.especialista.id != null}">
		                                        	<td><a title="Aceptado" class="btn btn-sm btn-success"><i class="fa fa-check"></i></a>
		                                        	<a href="mensaje-urgente-esp-${servicioUrgente.user.id}-${servicioUrgente.id}" title="Enviar mensaje" > <i class="fa fa-envelope"></i> </a> </td>
		                                        </c:when>
		                                        <c:otherwise>
		                                        	<td><a title="Aceptar urgencia" id="botonAceptar${servicioUrgente.id}" onmouseout="cambiarIconoaAceptar(this);" onmouseover="cambiarIconoaAceptado(this);" href="aceptarUrgencia-${servicioUrgente.id}" class="btn btn-warning btn-sm"><i id="iconbotonAceptar${servicioUrgente.id}" class="fa fa-clock-o"></i></a> </td>
		                                        </c:otherwise>
		                                    </c:choose>
	                                    </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


        <!-- *** FOOTER ***____________________ -->
        <%@include file="footer.jsp"%>
</body>

<script type="text/javascript">
	function cambiarIconoaAceptado(obj){
		$("#icon"+obj.getAttribute("id")).attr("class", "fa fa-check");
		$("#"+obj.getAttribute("id")).attr("class", "btn btn-sm btn-success")
	}
	function cambiarIconoaAceptar(obj){
		$("#icon"+obj.getAttribute("id")).attr("class", "fa fa-clock-o");
		$("#"+obj.getAttribute("id")).attr("class", "btn btn-warning btn-sm")
	}
</script>

</html>
