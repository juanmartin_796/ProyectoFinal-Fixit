<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<link href="css/star-rating.css" media="all" rel="stylesheet"
	type="text/css" />
<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">


<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

</head>

<body>
	<%@include file="header.jsp"%>
	<div id="all">

		<div id="content">
			<div class="container">

				<%@include file="lateral-encabezado.jsp"%>

				<div class="col-md-9">

					<div class="row" id="productMain">
						<div class="col-sm-6">
							<div id="mainImage">
								<img src="images-upload/${publicacion.foto1}" alt="" class="img-thumbnail center-block" style="min-height: 400px; max-height:400px">
							</div>

							<!--                             <div class="ribbon sale"> -->
							<!--                                 <div class="theribbon">NUEVO</div> -->
							<!--                                 <div class="ribbon-background"></div> -->
							<!--                             </div> -->
							<!-- /.ribbon -->

							<div class="ribbon new">
<!-- 								<div class="theribbon"> -->
<!-- 									5 <i class="fa fa-star" aria-hidden="true"></i> -->
<!-- 								</div> -->
								<div class="ribbon-background"></div>
							</div>
							<!-- /.ribbon -->

						</div>
						<div class="col-sm-6">
							<div class="box">
								<div style="text-align: right;">
									<a class="hidden-xs" href="denunciar-${publicacion.id}">Denunciar</a>
								</div>
								<div style="text-align: center;">
									<input disabled="disabled" id="input-21e"
										value="${califPromedio}" type="text" class="rating" data-min=0
										data-max=5 data-step=0.5 data-size="xs" title="">
								</div>
								<h1 class="text-center">${publicacion.titulo}</h1>
								<p class="goToDescription">
									<a href="#details" class="scroll-to">Ver descripcion del
										servicio</a>
								</p>
								<c:choose>
									<c:when test="${publicacion.precio== 0 }">
										<p class="price">Precio a convenir</p>
									</c:when>
									<c:otherwise>
										<p class="price">$${publicacion.precio }</p>
									</c:otherwise>

								</c:choose>
								<p class="text-center buttons">
									<c:choose>
										<c:when test="${publicacion.bajaLogica==false}">

											<a href="contratar-${publicacion.id}" class="btn btn-primary"><i
												class="fa fa-shopping-cart"></i> Contratar</a>
										</c:when>
										<c:otherwise>
											<button class="btn btn-primary" disabled="disabled">
												Publicacion eliminada</button>
										</c:otherwise>
									</c:choose>
									<a href="mensaje-${publicacion.especialista.id}-${publicacion.id}" class="btn btn-default"><i
										class="fa fa-comment"></i> Enviar mensaje</a>
								</p>


							</div>

							<div class="row" id="thumbs">
								<div class="col-xs-4">
<!-- 									<a href="img/product1.jpg" class="thumb"> <img -->
<!-- 										src="img/product1.jpg" alt="" class="img-responsive"> -->
<!-- 									</a> -->
									<a href="images-upload/${publicacion.foto1}" class="thumb"> <img
										src="images-upload/${publicacion.foto1}" alt="" class="img-responsive center-block" style= "min-height: 50px; max-height: 50px">
									</a>
								</div>
								<div class="col-xs-4">
									<a href="images-upload/${publicacion.foto2}" class="thumb"> <img
										src="images-upload/${publicacion.foto2}" alt="" class="img-responsive center-block" style= "min-height: 50px; max-height: 50px">
									</a>
								</div>
								<div class="col-xs-4">
									<a href="images-upload/${publicacion.foto3}" class="thumb"> <img
										src="images-upload/${publicacion.foto3}" alt="" class="img-responsive center-block" style= "min-height: 50px; max-height: 50px">
									</a>
								</div>
							</div>
						</div>

					</div>


					<div class="box" id="details">
						<p>
							<strong style="font-size: 18px">Especialista: </strong> <a
								href="trabajos-realizados-${publicacion.especialista.id }">
								${publicacion.especialista.user.username}
								 </a>
						</p>

						<h5>${publicacion.cuerpo}</h4>
						<!--                             <ul> -->
						<!--                                 <li>Polyester</li> -->
						<!--                                 <li>Machine wash</li> -->
						<!--                             </ul> -->
						<!--                             <h4>Size & Fit</h4> -->
						<!--                             <ul> -->
						<!--                                 <li>Regular fit</li> -->
						<!--                                 <li>The model (height 5'8" and chest 33") is wearing a size S</li> -->
						<!--                             </ul> -->

						<!--                             <blockquote> -->
						<!--                                 <p><em>Define style this season with Armani's new range of trendy tops, crafted with intricate details. Create a chic statement look by teaming this lace number with skinny jeans and pumps.</em> -->
						<!--                                 </p> -->
						<!--                             </blockquote> -->
						<hr>
						<div class="social">
							<h4>Compartelo con tus amigos!</h4>
							<p>
								<a href="#" class="external facebook" data-animate-hover="pulse"><i
									class="fa fa-facebook"></i></a> <a href="#" class="external gplus"
									data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
								<a href="#" class="external twitter" data-animate-hover="pulse"><i
									class="fa fa-twitter"></i></a> <a href="#" class="email"
									data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
							</p>
						</div>
					</div>

					<div class="row same-height-row">
						<div class="col-md-3 col-sm-6">
							<div class="box same-height">
								<h3>Tambien te pueden interesar</h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6">
							<div class="product same-height">
								<div class="flip-container">
									<div class="flipper">
										<div class="front">
											<a href="detail.html"> <img src="img/product2.jpg" alt=""
												class="img-responsive">
											</a>
										</div>
										<div class="back">
											<a href="detail.html"> <img src="img/product2_2.jpg"
												alt="" class="img-responsive">
											</a>
										</div>
									</div>
								</div>
								<a href="detail.html" class="invisible"> <img
									src="img/product2.jpg" alt="" class="img-responsive">
								</a>
								<div class="text">
									<h3>Algun servicio</h3>
									<p class="price">$Algun precio</p>
								</div>
							</div>
							<!-- /.product -->
						</div>

						<div class="col-md-3 col-sm-6">
							<div class="product same-height">
								<div class="flip-container">
									<div class="flipper">
										<div class="front">
											<a href="detail.html"> <img src="img/product1.jpg" alt=""
												class="img-responsive">
											</a>
										</div>
										<div class="back">
											<a href="detail.html"> <img src="img/product1_2.jpg"
												alt="" class="img-responsive">
											</a>
										</div>
									</div>
								</div>
								<a href="detail.html" class="invisible"> <img
									src="img/product1.jpg" alt="" class="img-responsive">
								</a>
								<div class="text">
									<h3>Algun servicio</h3>
									<p class="price">$algun precio</p>
								</div>
							</div>
							<!-- /.product -->
						</div>


						<div class="col-md-3 col-sm-6">
							<div class="product same-height">
								<div class="flip-container">
									<div class="flipper">
										<div class="front">
											<a href="detail.html"> <img src="img/product3.jpg" alt=""
												class="img-responsive">
											</a>
										</div>
										<div class="back">
											<a href="detail.html"> <img src="img/product3_2.jpg"
												alt="" class="img-responsive">
											</a>
										</div>
									</div>
								</div>
								<a href="detail.html" class="invisible"> <img
									src="img/product3.jpg" alt="" class="img-responsive">
								</a>
								<div class="text">
									<h3>Algun servicio</h3>
									<p class="price">$algun precio</p>

								</div>
							</div>
							<!-- /.product -->
						</div>

					</div>

					<!--                     <div class="row same-height-row"> -->
					<!--                         <div class="col-md-3 col-sm-6"> -->
					<!--                             <div class="box same-height"> -->
					<!--                                 <h3>Products viewed recently</h3> -->
					<!--                             </div> -->
					<!--                         </div> -->


					<!--                         <div class="col-md-3 col-sm-6"> -->
					<!--                             <div class="product same-height"> -->
					<!--                                 <div class="flip-container"> -->
					<!--                                     <div class="flipper"> -->
					<!--                                         <div class="front"> -->
					<!--                                             <a href="detail.html"> -->
					<!--                                                 <img src="img/product2.jpg" alt="" class="img-responsive"> -->
					<!--                                             </a> -->
					<!--                                         </div> -->
					<!--                                         <div class="back"> -->
					<!--                                             <a href="detail.html"> -->
					<!--                                                 <img src="img/product2_2.jpg" alt="" class="img-responsive"> -->
					<!--                                             </a> -->
					<!--                                         </div> -->
					<!--                                     </div> -->
					<!--                                 </div> -->
					<!--                                 <a href="detail.html" class="invisible"> -->
					<!--                                     <img src="img/product2.jpg" alt="" class="img-responsive"> -->
					<!--                                 </a> -->
					<!--                                 <div class="text"> -->
					<!--                                     <h3>Fur coat</h3> -->
					<!--                                     <p class="price">$143</p> -->
					<!--                                 </div> -->
					<!--                             </div> -->
					<!--                             /.product -->
					<!--                         </div> -->

					<!--                         <div class="col-md-3 col-sm-6"> -->
					<!--                             <div class="product same-height"> -->
					<!--                                 <div class="flip-container"> -->
					<!--                                     <div class="flipper"> -->
					<!--                                         <div class="front"> -->
					<!--                                             <a href="detail.html"> -->
					<!--                                                 <img src="img/product1.jpg" alt="" class="img-responsive"> -->
					<!--                                             </a> -->
					<!--                                         </div> -->
					<!--                                         <div class="back"> -->
					<!--                                             <a href="detail.html"> -->
					<!--                                                 <img src="img/product1_2.jpg" alt="" class="img-responsive"> -->
					<!--                                             </a> -->
					<!--                                         </div> -->
					<!--                                     </div> -->
					<!--                                 </div> -->
					<!--                                 <a href="detail.html" class="invisible"> -->
					<!--                                     <img src="img/product1.jpg" alt="" class="img-responsive"> -->
					<!--                                 </a> -->
					<!--                                 <div class="text"> -->
					<!--                                     <h3>Fur coat</h3> -->
					<!--                                     <p class="price">$143</p> -->
					<!--                                 </div> -->
					<!--                             </div> -->
					<!--                             /.product -->
					<!--                         </div> -->


					<!--                         <div class="col-md-3 col-sm-6"> -->
					<!--                             <div class="product same-height"> -->
					<!--                                 <div class="flip-container"> -->
					<!--                                     <div class="flipper"> -->
					<!--                                         <div class="front"> -->
					<!--                                             <a href="detail.html"> -->
					<!--                                                 <img src="img/product3.jpg" alt="" class="img-responsive"> -->
					<!--                                             </a> -->
					<!--                                         </div> -->
					<!--                                         <div class="back"> -->
					<!--                                             <a href="detail.html"> -->
					<!--                                                 <img src="img/product3_2.jpg" alt="" class="img-responsive"> -->
					<!--                                             </a> -->
					<!--                                         </div> -->
					<!--                                     </div> -->
					<!--                                 </div> -->
					<!--                                 <a href="detail.html" class="invisible"> -->
					<!--                                     <img src="img/product3.jpg" alt="" class="img-responsive"> -->
					<!--                                 </a> -->
					<!--                                 <div class="text"> -->
					<!--                                     <h3>Fur coat</h3> -->
					<!--                                     <p class="price">$143</p> -->

					<!--                                 </div> -->
					<!--                             </div> -->
					<!--                             /.product -->
					<!--                         </div> -->

					<!--                     </div> -->

				</div>
				<!-- /.col-md-9 -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***
 _________________________________________________________ -->
		<%@include file="footer.jsp"%>

		<script src="js/star-rating.js" type="text/javascript"></script>
</body>

</html>