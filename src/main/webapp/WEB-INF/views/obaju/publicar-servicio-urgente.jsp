<!DOCTYPE html>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="es">

<head>
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- <link rel="stylesheet" href="css/select.css"> -->

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">



<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<style>
/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
#map {
	height: 100%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}
</style>


</head>

<body>

	<%@include file="header.jsp"%>
	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Publicar</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9">


					<div class="row" id="contact">
						<div class="col-md-12">
						<h3>Publica que necesitas un servicio urgente, y pronto
							recibir�s ayuda !</h3>
						</div>

						<form:form action="?${_csrf.parameterName}=${_csrf.token}"
							method="POST" modelAttribute="servicioUrgente" acceptCharset="ISO-8859-1">


							<div class="col-md-3">
								<h5>Categoria</h5>
								<form:select path="categoria.id" id="categoria.id"
									items="${subcategorias}" itemLabel="nombre" itemValue="id"
									class="form-control" style="width: auto;">
									<option value="hide">-- Categoria --</option>
								</form:select>
							</div>
							<div class=col-md-9>
								<h5>Buscar especialistas con distancia menor a:</h5>
								<div class="col-md-9">
									<input onchange="showVal(this.value)" type="range" min="0"
										value="5" max="50" step="1" />

								</div>
								<div class="col-md-3">
									<label id="valorSlider">5</label> Km
									<form:hidden path="distanciaMax" id="distanciaMax" value="5" />
								</div>
							</div>
							<div class="col-md-12">
								<h5>Descripci�n de la urgencia</h5>
								<form:textarea path="descripcion" class="form-control"
									placeholder="Escribe una descripci�n de hasta 150 caracteres."
									name="descripcion" rows="3" cols="20" maxlength="150" />
								<form:errors path="descripcion" cssClass="error"/>
							</div>
							
							<div class="col-md-12">
							<h5>Posicion donde se requiere el servicio</h5>
							</div>
							<div class="col-md-12">
							<div id="map"></div>
							</div>
							<form:hidden path="latitud" id="latitud" />
							<form:hidden path="longitud" id="longitud" />
							<form:hidden path="especialista.id" />
							<form:hidden path="id" />
							<form:hidden path="user.id" />
<%-- 							<form:hidden path="dateTime" /> --%>
<%-- 							<form:hidden path="tiempoLimiteParaRespuesta" /> --%>

							
							<button type="submit" class="btn btn-primary center-block">Publicar
								servicio urgente</button>

						</form:form>
					</div>
				</div>
			</div>
			<!-- /.col-md-9 -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /#content -->

	<hr>

	<div id="footer" data-animate="fadeInUp">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<h4>Pages</h4>

					<ul>
						<li><a href="text.html">About us</a></li>
						<li><a href="text.html">Terms and conditions</a></li>
						<li><a href="faq.html">FAQ</a></li>
						<li><a href="contact.html">Contact us</a></li>
					</ul>

					<hr>

					<h4>User section</h4>

					<ul>
						<li><a href="#" data-toggle="modal"
							data-target="#login-modal">Login</a></li>
						<li><a href="register.html">Regiter</a></li>
					</ul>

					<hr class="hidden-md hidden-lg hidden-sm">

				</div>
				<!-- /.col-md-3 -->

				<div class="col-md-3 col-sm-6">

					<h4>Top categories</h4>

					<h5>Men</h5>

					<ul>
						<li><a href="category.html">T-shirts</a></li>
						<li><a href="category.html">Shirts</a></li>
						<li><a href="category.html">Accessories</a></li>
					</ul>

					<h5>Ladies</h5>
					<ul>
						<li><a href="category.html">T-shirts</a></li>
						<li><a href="category.html">Skirts</a></li>
						<li><a href="category.html">Pants</a></li>
						<li><a href="category.html">Accessories</a></li>
					</ul>

					<hr class="hidden-md hidden-lg">

				</div>
				<!-- /.col-md-3 -->

				<div class="col-md-3 col-sm-6">

					<h4>Where to find us</h4>

					<p>
						<strong>Obaju Ltd.</strong> <br>13/25 New Avenue <br>New
						Heaven <br>45Y 73J <br>England <br> <strong>Great
							Britain</strong>
					</p>

					<a href="contact.html">Go to contact page</a>

					<hr class="hidden-md hidden-lg">

				</div>
				<!-- /.col-md-3 -->



				<div class="col-md-3 col-sm-6">

					<h4>Get the news</h4>

					<p class="text-muted">Pellentesque habitant morbi tristique
						senectus et netus et malesuada fames ac turpis egestas.</p>

					<form>
						<div class="input-group">

							<input type="text" class="form-control"> <span
								class="input-group-btn">

								<button class="btn btn-default" type="button">Subscribe!</button>

							</span>

						</div>
						<!-- /input-group -->
					</form>
					<hr>

					<h4>Stay in touch</h4>

					<p class="social">
						<a href="#" class="facebook external" data-animate-hover="shake"><i
							class="fa fa-facebook"></i></a> <a href="#" class="twitter external"
							data-animate-hover="shake"><i class="fa fa-twitter"></i></a> <a
							href="#" class="instagram external" data-animate-hover="shake"><i
							class="fa fa-instagram"></i></a> <a href="#" class="gplus external"
							data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
						<a href="#" class="email external" data-animate-hover="shake"><i
							class="fa fa-envelope"></i></a>
					</p>
				</div>
			</div>
		</div>


		<div id="copyright">
			<div class="container">
				<div class="col-md-6">
					<p class="pull-left">Â© 2015 Your name goes here.</p>

				</div>
				<div class="col-md-6">
					<p class="pull-right">
						Template by <a
							href="https://bootstrapious.com/e-commerce-templates">Bootstrapious</a>
						<a href="https://fity.cz">Fity</a>
						<!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
					</p>
				</div>
			</div>
		</div>
	</div>

	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/bootstrap-hover-dropdown.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/front.js"></script>

</body>

<script type="text/javascript">
	function bs_input_file() {
		$(".input-file")
				.before(
						function() {
							if (!$(this).prev().hasClass('input-ghost')) {
								var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
								element.attr("name", $(this).attr("name"));
								element.change(function() {
									element.next(element).find('input').val(
											(element.val()).split('\\').pop());
								});
								$(this).find("button.btn-choose").click(
										function() {
											element.click();
										});
								$(this).find("button.btn-reset").click(
										function() {
											element.val(null);
											$(this).parents(".input-file")
													.find('input').val('');
										});
								$(this).find('input').css("cursor", "pointer");
								$(this).find('input').mousedown(
										function() {
											$(this).parents('.input-file')
													.prev().click();
											return false;
										});
								return element;
							}
						});
	}
	$(function() {
		bs_input_file();
	});
	
	// With JQuery
	$("#ex8").slider({
		tooltip: 'always'
	});

	var slider = new Slider("#ex8", {
		tooltip: 'always'
	});
	
</script>

<!-- <script src="js/select.js"></script> -->

<script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.

      function initMap() {

    	  
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 15
        });
        var infoWindow = new google.maps.InfoWindow({map: map});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            
            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                draggable:true,
                title:"Posicion actual"
            });
            
            marker.addListener('dragend', function() {
	   		    alert(marker.getPosition());
	   		 	document.getElementById('latitud').value = marker.getPosition().lat();
	   		 	document.getElementById('longitud').value = marker.getPosition().lng();
	   		  });

            //infoWindow.setPosition(pos);
            //infoWindow.setContent('Posición actual');
            map.setCenter(pos);
            document.getElementById('latitud').value = marker.getPosition().lat();
   		 	document.getElementById('longitud').value = marker.getPosition().lng();
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
      }
    </script>
<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkJNjUz1ss0ikB_RhZXQu3z0NmL_gOa58&callback=initMap">
    </script>

<script> 
    	function showVal(newVal){
    		document.getElementById("valorSlider").innerHTML=newVal;
    		document.getElementById("distanciaMax").value=newVal;
		} 
    </script>
</html>
