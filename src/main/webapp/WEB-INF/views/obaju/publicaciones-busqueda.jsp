<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">



</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Buscar "${busqueda}"</li>
					</ul>
				</div>

				<div class="col-md-3">
					<!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
					<div class="panel panel-default sidebar-menu">

						<div class="panel-heading">
							<h3 class="panel-title">Categories</h3>
						</div>

						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked category-menu">
								<li><a href="category.html">Men <span
										class="badge pull-right">42</span></a>
									<ul>
										<li><a href="category.html">T-shirts</a></li>
										<li><a href="category.html">Shirts</a></li>
										<li><a href="category.html">Pants</a></li>
										<li><a href="category.html">Accessories</a></li>
									</ul></li>



								<c:forEach items="${categoriasPadres}" var="categoriaPadre">
									<c:choose>
										<c:when
											test="${categoriaPadre.id == subcategoriaSeleccionada.id}">
											<li class="active"><a
												href="category-${categoriaPadre.id} }.html">${subcategoriaSeleccionada.nombre}
													<span class="badge pull-right">123</span>
											</a>
												</li>
										</c:when>
										<c:otherwise>
											<li><a href="category-${categoriaPadre.id}.html">${categoriaPadre.nombre}
													<span class="badge pull-right">42</span>
											</a></li>
										</c:otherwise>
									</c:choose>
									<ul>
										<c:forEach items="${categoriaPadre.subCategorias}"
											var="subcategoria">
											<li><a href="category-${subcategoria.id }.html">${subcategoria.nombre }</a>
											</li>
										</c:forEach>
									</ul>
									</li>
								</c:forEach>
							</ul>

						</div>
					</div>

					<div class="panel panel-default sidebar-menu">

						<div class="panel-heading">
							<h3 class="panel-title">
								Brands <a class="btn btn-xs btn-danger pull-right" href="#"><i
									class="fa fa-times-circle"></i> Clear</a>
							</h3>
						</div>

						<div class="panel-body">

							<form>
								<div class="form-group">
									<div class="checkbox">
										<label> <input type="checkbox">Armani (10)
										</label>
									</div>
									<div class="checkbox">
										<label> <input type="checkbox">Versace (12)
										</label>
									</div>
									<div class="checkbox">
										<label> <input type="checkbox">Carlo Bruni
											(15)
										</label>
									</div>
									<div class="checkbox">
										<label> <input type="checkbox">Jack Honey (14)
										</label>
									</div>
								</div>

								<button class="btn btn-default btn-sm btn-primary">
									<i class="fa fa-pencil"></i> Apply
								</button>

							</form>

						</div>
					</div>

					<div class="panel panel-default sidebar-menu">

						<div class="panel-heading">
							<h3 class="panel-title">
								Colours <a class="btn btn-xs btn-danger pull-right" href="#"><i
									class="fa fa-times-circle"></i> Clear</a>
							</h3>
						</div>

						<div class="panel-body">

							<form>
								<div class="form-group">
									<div class="checkbox">
										<label> <input type="checkbox"> <span
											class="colour white"></span> White (14)
										</label>
									</div>
									<div class="checkbox">
										<label> <input type="checkbox"> <span
											class="colour blue"></span> Blue (10)
										</label>
									</div>
									<div class="checkbox">
										<label> <input type="checkbox"> <span
											class="colour green"></span> Green (20)
										</label>
									</div>
									<div class="checkbox">
										<label> <input type="checkbox"> <span
											class="colour yellow"></span> Yellow (13)
										</label>
									</div>
									<div class="checkbox">
										<label> <input type="checkbox"> <span
											class="colour red"></span> Red (10)
										</label>
									</div>
								</div>

								<button class="btn btn-default btn-sm btn-primary">
									<i class="fa fa-pencil"></i> Apply
								</button>

							</form>

						</div>
					</div>

					<!-- *** MENUS AND FILTERS END *** -->

					<div class="banner">
						<a href="#"> <img src="img/banner.jpg" alt="sales 2014"
							class="img-responsive">
						</a>
					</div>
				</div>

				<div class="col-md-9">
					<div class="box">
						<h1>Resultado de la busqueda: "${busqueda}"</h1>
					</div>

					<div class="box info-bar">
						<div class="row">
							<div class="col-sm-12 col-md-4 products-showing">
								Showing <strong>12</strong> of <strong>25</strong> products
							</div>

							<div class="col-sm-12 col-md-8  products-number-sort">
								<div class="row">
									<form class="form-inline">
										<div class="col-md-6 col-sm-6">
											<div class="products-number">
												<strong>Show</strong> <a href="#"
													class="btn btn-default btn-sm btn-primary">12</a> <a
													href="#" class="btn btn-default btn-sm">24</a> <a href="#"
													class="btn btn-default btn-sm">All</a> products
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="products-sort-by">
												<strong>Sort by</strong> <select name="sort-by"
													class="form-control">
													<option>Price</option>
													<option>Name</option>
													<option>Sales first</option>
												</select>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="row products">
						<c:forEach items="${publicaciones}" var="publicacion"
							varStatus="cont">
							<div class="col-md-4 col-sm-6">
								<div class="product">
									<div class="flip-container">
										<div class="flipper">
											<div class="front">
												<a href="detail-publicacion-${publicacion.id}.html"> <img
													src="images-upload/${publicacion.foto1}" alt="" class="img-responsive">
												</a>
											</div>
											<div class="back">
												<a href="detail-publicacion-${publicacion.id}.html"> <img
													src="images-upload/${publicacion.foto2}" alt="" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<a href="detail.html" class="invisible"> <img
										src="images-upload/${publicacion.foto1}" alt="" class="img-responsive">
									</a>
									<div class="text">
										<h3>
											<a href="detail-publicacion-${publicacion.id}.html">${publicacion.titulo}</a>
										</h3>
										<c:choose>
											<c:when test="${fn:length(publicacion.cuerpo) > 100}">
												<p class="price">${fn:substring(publicacion.cuerpo,1,97)}...</p>
											</c:when>
											<c:otherwise>
												<p class="price">${publicacion.cuerpo}</p>
											</c:otherwise>
										</c:choose>
										<p class="buttons">
											<a href="detail-publicacion-${publicacion.id}.html"
												class="btn btn-default">View detail</a> <a
												href="basket.html" class="btn btn-primary"><i
												class="fa fa-shopping-cart"></i>Contratar</a>
										</p>
									</div>
									<!-- /.text -->
								</div>
								<!-- /.product -->
							</div>
						</c:forEach>
					</div>

					<div class="pages">

						<p class="loadMore">
							<a href="#" class="btn btn-primary btn-lg"><i
								class="fa fa-chevron-down"></i> Load more</a>
						</p>

						<ul class="pagination">
							<li><a href="#">&laquo;</a></li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">&raquo;</a></li>
						</ul>
					</div>


				</div>
				<!-- /.col-md-9 -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***_______________________________ -->
		<%@include file="footer.jsp" %>
</body>

</html>