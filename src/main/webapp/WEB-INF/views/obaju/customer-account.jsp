<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<style>
/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
#map {
	height: 100%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}
</style>

<script type="text/javascript">
		window.onload=function() {
			$("#liPerfil").attr("class", "active");
		}
	</script>

</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Perfil</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9">
					<div class="box">
						<h1>Mi perfil</h1>
						<p class="lead">Cambie los datos de su perfil o su contraseña
							aquí.</p>
						<p class="text-muted">Se recomienda que mantenga sus datos
							personales actualizados, para una mejor experiencia.</p>

						<h3>Cambiar contraseña</h3>

						<!--                         <form> -->
						<form:form method="POST" action="perfil"
							onSubmit="return comprobarClave()" acceptCharset="ISO-8859-1">

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="password_old">Contraseña antigua</label> <input
											type="password" class="form-control" id="oldPassword"
											name="oldPassword" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="password_1">Nueva contraseña</label> <input
											type="password" class="form-control" id="newPassword"
											name="newPassword">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="password_2">Repetir contraseña nueva</label> <input
											type="password" class="form-control"
											id="newPasswordRepeticion" name="newPasswordRepeticion">
									</div>
								</div>

							</div>
							<!-- /.row -->
							<div class="alert alert-danger" id="alertContrasenasDistintas"
								hidden>
								<strong>No se pudo actualizar la contraseña. </strong>Las
								contraseñas no coinciden.
							</div>
							<div class="alert alert-danger" id="alertContrasenas"
								${mostrarContrasenaIncorrecta}>
								<strong>No se pudo actualizar la contraseña. </strong>Su clave
								actual es incorrecta.
							</div>
							<div class="col-sm-12 text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-save"></i> Actualizar contraseña
								</button>
							</div>
							<%--                         </form> --%>
						</form:form>

						<hr>

						<h3>Perfil personal</h3>
						<form:form method="POST" modelAttribute="cliente"
							action="actualizadoPerfil" acceptCharset="ISO-8859-1">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="firstname">Nombre</label>
										<form:input maxlength="40" style="text-transform:uppercase"
											type="text" class="form-control" path="nombre" id="nombre"/>
										<form:errors path="nombre" cssClass="error" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="lastname">Apellido</label>
										<form:input maxlength="40" style="text-transform:uppercase"
											type="text" class="form-control" path="apellido"
											id="apellido" />
										<form:errors path="apellido" cssClass="error" />
									</div>
								</div>
							</div>
							<!-- /.row -->

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="company">Calle</label>
										<form:input style="text-transform:uppercase" maxlength="30"
											type="text" class="form-control" id="direccion.calle"
											path="direccion.calle" />
										<form:errors path="direccion.calle" cssClass="error" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="street">Numero</label>
										<form:input maxlength="4" type="text"
											onkeypress='validate(event)' class="form-control"
											id="direccion.numero" path="direccion.numero" />
										<form:errors path="direccion.numero" cssClass="error" />
									</div>
								</div>
							</div>
							<!-- /.row -->

							<div class="row">
								<div class="col-sm-6 col-md-3">
									<div class="form-group">
										<label for="city">Piso</label>
										<form:input maxlength="2" onkeypress='validate(event)'
											type="text" class="form-control" path="direccion.piso"
											id="direccion.piso" />
										<form:errors path="direccion.piso" cssClass="error" />
									</div>
								</div>
								<div class="col-sm-6 col-md-3">
									<div class="form-group">
										<label for="country">Departamento</label>
										<form:input style="text-transform:uppercase" maxlength="1"
											type="text" class="form-control"
											path="direccion.departamento" id="direccion.departamento" />
										<form:errors path="direccion.departamento" cssClass="error" />
									</div>
								</div>
								<!--                                 <div class="col-sm-6 col-md-3"> -->
								<!--                                     <div class="form-group"> -->
								<!--                                         <label for="zip">Codigo Postal</label> -->
								<%--                                         <form:input type="text" class="form-control" id="direccion.ciudad.codPostal" path="direccion.ciudad.codPostal"/> --%>
								<!--                                     </div> -->
								<!--                                 </div> -->
								<div class="col-sm-6 col-md-3">
									<div class="form-group">
										<label for="state">Provincia</label>
										<form:select onchange="setearItemsCiudades(this);"
											class="form-control" itemValue="id"
											path="direccion.ciudad.provincia.id"
											items="${itemsProvincias}" itemLabel="nombre"
											id="selectOptionProvincia">
										</form:select>
									</div>
								</div>
								<div class="col-sm-6 col-md-3">
									<div class="form-group">
										<label for="state">Ciudad</label>
										<form:select class="form-control" itemValue="codPostal"
											path="direccion.ciudad.codPostal" items="${itemsCiudades}"
											itemLabel="nombre" id="selectOptionCiudad">
										</form:select>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label for="phone">Telefono</label>
										<form:input maxlength="10" onkeypress='validate(event)'
											type="text" class="form-control" id="phone" path="telefono" />
										<form:errors path="telefono" cssClass="error" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="email">Email</label>
										<form:input type="text" class="form-control" path="user.mail"
											id="user.mail" />
										<form:errors path="user.mail" cssClass="error" />
									</div>
								</div>
								<div class="col-sm-12 text-center" id="map"></div>
								<form:hidden path="latitud" id="latitud" />
								<form:hidden path="longitud" id="longitud" />
								<form:hidden path="user.id" id="user.id" />
								<form:hidden path="id" id="id" />
								<div class="col-sm-12 text-center">
									<button type="submit" name="Save" value="Save"
										class="btn btn-primary">
										<i class="fa fa-save"></i> Guardar cambios
									</button>
								</div>
							</div>
						</form:form>
					</div>
				</div>

			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->

	</div>
	<%@include file="footer.jsp"%>

</body>

<script LANGUAGE="JavaScript">
function comprobarClave(){
    var clave1 = document.getElementById('newPassword').value;
    var clave2 = document.getElementById('newPasswordRepeticion').value;

    if (clave1 !="" && clave2 != "" && clave1 == clave2){       //alert("Las dos claves son iguales...\nRealizaríamos las acciones del caso positivo");
       return true;
    }
    else{
       alert("Las claves no coinciden, por favor re ingrese las claves");
       $("#alertContrasenasDistintas").show();
       return false;
    }
}
</script>


<script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.

      function initMap() {

    	  
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 15
        });
        var infoWindow = new google.maps.InfoWindow({map: map});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            
            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                draggable:true,
                title:"Posicion actual"
            });
            
            marker.addListener('dragend', function() {
	   		    alert(marker.getPosition());
	   		 	document.getElementById('latitud').value = marker.getPosition().lat();
	   		 	document.getElementById('longitud').value = marker.getPosition().lng();
	   		  });

            //infoWindow.setPosition(pos);
            //infoWindow.setContent('Posición actual');
            map.setCenter(pos);
            document.getElementById('latitud').value = marker.getPosition().lat();
   		 	document.getElementById('longitud').value = marker.getPosition().lng();
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
        
//         setearPosicionMapa("Corrientes", "Goya", map);
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
      }
      
      
      
//       function setearPosicionMapa(provincia, ciudad, map){
//     	  var address = provincia+","+ciudad;
          
//           geocoder = new google.maps.Geocoder();
//           if (geocoder) {
//             geocoder.geocode( { 'address': address}, function(results, status) {
//               if (status == google.maps.GeocoderStatus.OK) {
//                 if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
//                 map.setCenter(results[0].geometry.location);

         
//                   var marker = new google.maps.Marker({
//                       position: results[0].geometry.location,
//                       map: map, 
//                       title:address,
//                       draggable:true,
//                   }); 
        

//                 } else {
//                   alert("No results found");
//                 }
//               } else {
//                 alert("Geocode was not successful for the following reason: " + status);
//               }
//             });
          
//         }
//       }
      
      
    </script>
<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkJNjUz1ss0ikB_RhZXQu3z0NmL_gOa58&callback=initMap">
    </script>

<script type="text/javascript">

    function validate(evt) {
    	  var theEvent = evt || window.event;
    	  var key = theEvent.keyCode || theEvent.which;
    	  
    	  BACKSPACE = 8;
          DEL = 46;
          TABKEY = 9;
          ARROW_KEYS = {left: 37, right: 39};
          regex = /[0-9]|\./; 
          if (key === BACKSPACE || key === DEL || key === ARROW_KEYS.left || key === ARROW_KEYS.right ||  key===TABKEY) { 
          	return true;
          }
    	  
    	  key = String.fromCharCode( key );
    	  var regex = /[0-9]|\./;
    	  if( !regex.test(key) ) {
    	    theEvent.returnValue = false;
    	    if(theEvent.preventDefault) theEvent.preventDefault();
    	  }
    	}
    
    function setearItemsCiudades(obj){
    	var idProvincia = $( "#selectOptionProvincia option:selected" ).val();
    	alert(idProvincia);
    	getCiudades(idProvincia);
    	
    }
    
    function getCiudades(idProvincia) {
		//var token = $("meta[name='_csrf']").attr("content");
		//var header = $("meta[name='_csrf_header']").attr("content");
// 		var text = document.getElementById("textAreaComentario").value;
// 		var idServicio = $('#buttonEliminarModal').attr("name");
		//var idUrgencia = obj.getAttribute("value");
// 		var idPublicacion = obj;
// 		var datos = {
// 			id : idPublicacion
// 		};
		$.ajax({
			type : "GET",
			//contentType : "application/json",
			url : 'ciudades-'+idProvincia,

			//dataType : 'json',
			//timeout : 100000,
// 			beforeSend : function(request) {
// 				request.setRequestHeader(header, token);
// 			},
			success : function(datos) {
				console.log("SUCCESS: ", datos);
				$('#selectOptionCiudad')
	            .find('option')
	            .remove()
	            .end()
				$.each(datos, function(index) {
// 		            alert(datos[index].nombre);
		            $('#selectOptionCiudad').append('<option value="'+datos[index].codPostal+'">'+datos[index].nombre+'</option>')
		        });
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
    </script>

</html>