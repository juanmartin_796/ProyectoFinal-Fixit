<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>

<div class="col-md-3">
					<!-- *** CUSTOMER MENU ***
 _________________________________________________________ -->
					<div class="panel panel-default sidebar-menu">

						<div class="panel-heading">
							<h3 class="panel-title">Nombre del usuario</h3>
						</div>

						<div class="panel-body">

							<ul class="nav nav-pills nav-stacked">
							<li><i class="fa fa-list"></i> Menu cliente</li>
								<li id="liMiHistorial"><a href="servicioscontratados.html"><i
										class="fa fa-list"></i> Mi historial</a></li>
								<li id="liMisUrgencias"><a href="serviciosurgentespedidos"><i class="fa fa-exclamation-circle"></i> Mis urgencias 
										<c:if test="${contNotificacionesUrgentes!= 0}">
											<span class="badge pull-right">${contNotificacionesUrgentes}</span>
										</c:if>
										</a></li>
								<li id="liPerfil"><a href="perfil"><i
										class="fa fa-user"></i> Perfil</a></li>
								<li id="liMensajes"><a href="mensajes-recibidos-cliente"><i
										class="fa fa-envelope"></i> Mensajes</a></li>

								<li><i> <br></i></li>
								<li ${activarLateralEspecialista}><i class="fa fa-list"></i> Menu Especialista</li>
								<li id="liServiciosUrgentes" ${activarLateralEspecialista}><a href="misserviciosurgentes.html"><i
										class="fa fa-exclamation-triangle"></i> Servicios urgentes !
										<c:if test="${contNotificacionesMisServiciosUrgentes!= 0}">
											<span class="badge pull-right">${contNotificacionesMisServiciosUrgentes}</span>
										</c:if>
										</a></li>
								<li id="liServicios normales" ${activarLateralEspecialista}><a href="trabajos-realizados.html"><i
										class="fa fa-gavel"></i> Servicios normales</a></li>
								<li id="liMisPublicaciones" ${activarLateralEspecialista}><a href="customer-wishlist.html"><i
										class="fa fa-th-large"></i> Mis publicaciones</a></li>
								<li id="liMensajesEspecialista" ${activarLateralEspecialista}><a href="mensajes-recibidos-especialista"><i
										class="fa fa-envelope"></i> Mensajes</a></li>
							</ul>
						</div>

					</div>
					<!-- /.col-md-3 -->

					<!-- *** CUSTOMER MENU END *** -->
				</div>
</body>
</html>