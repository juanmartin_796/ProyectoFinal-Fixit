<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

</head>

<body>
	<%@include file="header.jsp" %>
	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="#">Hogar</a></li>
						<li>Nueva cuenta / Ingresar</li>
					</ul>

				</div>

				<div class="col-md-6">
					<div class="box">
						<h1>Nueva cuenta</h1>

						<p class="lead">�No es nuestro cliente registrado todav�a?</p>
						<p>�Todo el proceso no te llevar� m�s de cinco minutos!</p>
						<p class="text-muted">
							Si tiene alguna pregunta, no dude en comunicarse con nosotros, nuestro centro de atenci�n al cliente est� trabajando para usted 24/7. <a
								href="contact.html">Contactanos!</a>
						</p>

						<hr>

						<form:form modelAttribute="user" method="POST" acceptCharset="ISO-8859-1">
							<form:input type="hidden" path="id" id="id" />
							<div class="form-group">
								<label for="name">Nombre de usuario</label>
								<form:input maxlength="20" type="text" path="username" id="username"
									class="form-control" />
								<form:errors path="username" cssClass="error"/>
							</div>
							  <div class="form-group">
                                <label for="email">Email</label>
                                <form:input style="text-transform: lowercase;" maxlength="40" type="text" path="mail" class="form-control"></form:input>
                                <form:errors path="mail" cssClass="error"/>
                            </div>
							<div class="form-group">
								<label for="password">Contrase�a</label>
								<form:input maxlength="40" type="password" path="password" id="password"
									class="form-control" />
								<form:errors path="password" cssClass="error"/>
							</div>
							<div class="form-group">
								<label for="tipoUsuario"> �Vas a ser un?</label>
<%-- 								<form:select path="roles" items="${roles}" multiple="true" --%>
<%-- 									itemValue="id" itemLabel="type" class="form-control" /> --%>

								<div style="list-style: none">
									<form:radiobuttons  element="li" items="${roles}" path="roles" itemValue="id" itemLabel="type"/>
									<form:errors path="roles" cssClass="error"/>
								</div>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-user-md"></i> Registrarme
								</button>
							</div>
						</form:form>
					</div>
				</div>

				<div class="col-md-6">
					<div class="box">
						<h1>Iniciar sesi�n</h1>

						<p class="lead">Es gratis y lo ser� siempre</p>
						<p class="text-muted">�Gracias por elegirnos!</p>

						<hr>

						<form action="customer-orders.html" method="post">
							<div class="form-group">
								<label for="email">Email</label> <input type="text"
									class="form-control" id="email">
							</div>
							<div class="form-group">
								<label for="password">Contrase�a</label> <input type="password"
									class="form-control" id="password">
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-sign-in"></i> Entrar a mi cuenta
								</button>
							</div>
						</form>
					</div>
				</div>


			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		 <%@include file="footer.jsp" %>




</body>

</html>
