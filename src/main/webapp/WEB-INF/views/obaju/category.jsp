<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">



</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<%@include file="lateral-encabezado.jsp"%>

				<div class="col-md-9">
					<div class="box">
						<h3>${subcategoriaSeleccionada.nombre}</h3>
<!-- 						<p>Aca va una descripcion de la categoria seleccionada</p> -->
					</div>

					<div class="box info-bar">
						<div class="row">
							<div class="col-sm-12 col-md-4 products-showing">
<%-- 								Mostrando <strong>12</strong> de <strong>${cantPublicaciones}</strong> servicios --%>
							</div>

							<div class="col-sm-12 col-md-8  products-number-sort">
								<div class="row">
									<form class="form-inline">
										<div class="col-md-6 col-sm-6">
											<div class="products-number">
												<strong>Pagina</strong> 
												<c:forEach begin="1" end="${cantPaginas}" varStatus="loop">
													<a href="category-${publicaciones[0].categoria.id}-${(loop.index-1)*12}-${(loop.index-1)*12 + 12}" class="btn btn-default btn-sm">${loop.index}</a>
<!-- 													<a href="#" class="btn btn-default btn-sm">Todos</a> -->
												</c:forEach>
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
<!-- 											<div class="products-sort-by"> -->
<!-- 												<strong>Ordenar por</strong> <select name="sort-by" -->
<!-- 													class="form-control"> -->
<!-- 													<option>Distancia</option> -->
<!-- 													<option>Precio</option> -->
<!-- 													<option>Calificaciones</option> -->
<!-- 												</select> -->
<!-- 											</div> -->
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="row products">
						<c:forEach items="${publicaciones}" var="publicacion"
							varStatus="cont">
							<div class="col-md-4 col-sm-6">
								<div class="product">
									<div class="flip-container">
										<div class="flipper">
											<div class="front">
												<a href="detail-publicacion-${publicacion.id}.html"> <img
													src="images-upload/${publicacion.foto1}" alt="" class="img-responsive center-block" style= "min-height: 150px; max-height:150px;">
												</a>
											</div>
											<div class="back">
												<a href="detail-publicacion-${publicacion.id}.html"> <img
													src="images-upload/${publicacion.foto2}" alt="" class="img-responsive center-block" style= "min-height: 150px; max-height:150px;">
												</a>
											</div>
										</div>
									</div>
									<a href="detail.html" class="invisible"> <img
										src="images-upload/${publicacion.foto1}" alt="" class="img-responsive center-block" style= "min-height: 150px; max-height:150px;;">
									</a>
									<div class="text">
										<h3 style="margin-bottom: 0px">
											<a href="detail-publicacion-${publicacion.id}.html" title="${publicacion.cuerpo}">${publicacion.titulo}</a>
										</h3>
										<p class="price">$${publicacion.precio}</p>
<%-- 										<c:choose> --%>
<%-- 											<c:when test="${fn:length(publicacion.cuerpo) > 100}"> --%>
<%-- 												<p class="price">${fn:substring(publicacion.cuerpo,1,97)}...</p> --%>
<%-- 											</c:when> --%>
<%-- 											<c:otherwise> --%>
<%-- 												<p class="price">${publicacion.cuerpo}</p> --%>
<%-- 											</c:otherwise> --%>
<%-- 										</c:choose> --%>
										<p class="buttons">
											<a href="detail-publicacion-${publicacion.id}.html"
												class="btn btn-default">Ver detalle</a> <a
												href="contratar-${publicacion.id}" class="btn btn-primary"><i
												class="fa fa-shopping-cart"></i>Contratar</a>
										</p>
									</div>
									<!-- /.text -->
								</div>
								<!-- /.product -->
							</div>
						</c:forEach>
					</div>

					<div class="pages">

<!-- 						<p class="loadMore"> -->
<!-- 							<a href="#" class="btn btn-primary btn-lg"><i class="fa fa-chevron-down"></i> Cargar m�s</a> -->
<!-- 						</p> -->

						<ul class="pagination"> 
								<c:forEach begin="1" end="${cantPaginas}" varStatus="loop">
									<li><a href="category-${publicaciones[0].categoria.id}-${(loop.index-1)*12}-${(loop.index-1)*12 + 12}">${loop.index}</a></li>
								</c:forEach>
<!-- 							<li><a href="#">&laquo;</a></li> -->
<!-- 							<li class="active"><a href="#">1</a></li> -->
<!-- 							<li><a href="#">2</a></li> -->
<!-- 							<li><a href="#">3</a></li> -->
<!-- 							<li><a href="#">4</a></li> -->
<!-- 							<li><a href="#">5</a></li> -->
<!-- 							<li><a href="#">&raquo;</a></li> -->
						</ul>
					</div>


				</div>
				<!-- /.col-md-9 -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->

		<%@include file="footer.jsp"%>
</body>

</html>