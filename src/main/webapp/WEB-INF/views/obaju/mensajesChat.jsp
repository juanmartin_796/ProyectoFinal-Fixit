<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<script type="text/javascript">
	window.onload = function() {
		if (${esCliente==false}){
			$("#liMensajesEspecialista").attr("class", "active");
		} else if (${esCliente==true}){
			$("#liMensajes").attr("class", "active");
		}
	}
</script>

<link rel="stylesheet" href="css/chat.css">



</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Mis urgencias</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9" id="customer-orders">
					<div id="chatContactos">
					
					
<!-- 						<div class="menuChatContactos"> -->
<!-- 							<div class="foto"> -->
<!-- 								 <img  onclick=""  src="https://i.imgur.com/DY6gND0.png" draggable="false" /> -->
<!-- 							</div> -->
<!-- 							<div class="nombreUsuario">cliente</div> -->
<!-- 							<div class="ultimaVez">algun mensaje</div> -->
<!-- 							<div class="asunto">lalsjdlfj asjflasjdlfj adjflasñdjflña</div> -->
<!-- 						</div> -->
						
						
							
						<c:forEach items="${mensajes}" var="mensaje">
							<div class="menuChatContactos" name="cahtContactos">
								<div class="foto">
									<c:choose>
										<c:when test="${mensaje.servicioUrgente!= null}">
											<img value="${mensaje.servicioUrgente.id}" onclick="ajaxRecuperarMensajes(${mensaje.servicioUrgente.id});"  src="https://i.imgur.com/DY6gND0.png" draggable="false" />
											
										</c:when>
										<c:when test="${mensaje.publicacion!= null}">
										<img value="${mensaje.publicacion.id}" onclick="ajaxRecuperarMensajesDePublicacion(${mensaje.publicacion.id});"  src="https://i.imgur.com/DY6gND0.png" draggable="false" />
										</c:when>
									</c:choose>
								</div>
								<c:choose>
									<c:when test="${esCliente== true}">
									<div class="nombreUsuario">${mensaje.especialista.user.username}</div>
									</c:when>
									<c:otherwise>
										<div class="nombreUsuario">${mensaje.cliente.user.username}</div>
									</c:otherwise>
								</c:choose>
								<div class="ultimaVez">Ult. mje. el <fmt:formatDate value="${mensaje.fechaHora }" pattern="dd/MM/YY HH:mm" /></div>
								<div class="asunto">${mensaje.asunto}</div>
							</div>
						</c:forEach>
					</div>

					<!-- 					<div class="panel panel-primary" name="chatContactos"> -->
					<!-- 						<div class="panel-heading"> -->
					<!-- 							<a href="#"> <span class="badge">Martin_796</span></a> <small>02/01/2018 </small>  -->
					<!-- 						</div> -->
					<!-- 						<div class="panel-body" > -->
					<!-- 							Urgencia 1 bine larga y la puta madere qu eaj dlfjal djflaj sdlfñaj ljl  asjdfñlajsdlñ fjañsldj flñasj dl -->
					<!-- 						</div> -->
					<!-- 					</div> -->


					<div id="chat"></div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***____________________ -->
		<%@include file="footer.jsp"%>
</body>

<script type="text/javascript">
	function ajaxRecuperarMensajes(obj) {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
// 		var text = document.getElementById("textAreaComentario").value;
// 		var idServicio = $('#buttonEliminarModal').attr("name");
		//var idUrgencia = obj.getAttribute("value");
		var idUrgencia = obj;
		var datos = {
			id : idUrgencia
		};
		$.ajax({
			type : "GET",
			//contentType : "application/json",
			url : 'chat-'+idUrgencia,

			//dataType : 'json',
			//timeout : 100000,
			beforeSend : function(request) {
				request.setRequestHeader(header, token);
			},
			success : function(datos) {
				console.log("SUCCESS: ", datos);
				$('#chatContactos').hide(1000);
				$("#chat").html( datos );
				$('#chat').show(1000);
				//window.setTimeout('ajaxRecuperarMensajes('+idUrgencia+')', 5000)
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}

	function ajaxRecuperarMensajesDePublicacion(obj) {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
// 		var text = document.getElementById("textAreaComentario").value;
// 		var idServicio = $('#buttonEliminarModal').attr("name");
		//var idUrgencia = obj.getAttribute("value");
		var idPublicacion = obj;
		var datos = {
			id : idPublicacion
		};
		$.ajax({
			type : "GET",
			//contentType : "application/json",
			url : 'chat-publicacion-'+idPublicacion,

			//dataType : 'json',
			//timeout : 100000,
			beforeSend : function(request) {
				request.setRequestHeader(header, token);
			},
			success : function(datos) {
				console.log("SUCCESS: ", datos);
				$('#chatContactos').hide(1000);
				$("#chat").html( datos );
				$('#chat').show(1000);
				//window.setTimeout('ajaxRecuperarMensajes('+idUrgencia+')', 5000)
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}

	
	
	function setValorBotonEliminarModal(obj) {
		$("#buttonEliminarModal").attr("href", obj.getAttribute("value"));
		return false;
	}
</script>

</html>

