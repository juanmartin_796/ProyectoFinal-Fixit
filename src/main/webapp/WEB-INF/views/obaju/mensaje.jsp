<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- Estilo para la calificacion -->
<link href="css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">


<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<!-- Estilo para la calificacion -->
<link rel="stylesheet" href="css/starrr.css">

<script type="text/javascript">
		window.onload=function() {
			var remitente = "${mensaje.remitente}";
			if (remitente=="E") {
				$("#liMensajesEspecialista").attr("class", "active");
			} else {
				$("#liMensajes").attr("class", "active");
			}
		}
	</script>

</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Mi historial</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9" id="customer-orders">
					<div class="box">
						<h2>Enviar mensaje a <a href="trabajos-realizados-${mensaje.especialista.id}">${mensaje.especialista.user.username} </a></h2>
						<h3>Por la publicación "${mensaje.asunto}"</h3>
						<!--                         <p class="lead">Seleccione los servicios urgentes a los cuales puede atender inmediatamente.</p> -->
						<!--                         <p class="text-muted"> Deberá acudir lo antes posible, en caso contrario podria ser penalizado con el bloqueo de su cuenta <a href="contact.html">contact us</a>, our customer service center is working for you 24/7.</p> -->

						<hr>

<!-- 						<div class="table-responsive"> -->
							<form:form action="mensaje" type="POST" modelAttribute="mensaje" acceptCharset="ISO-8859-1">
							<div clas="col-md-12">
								<form:textarea class="form-control" path="cuerpo" rows="4" placeholder="Ingrese un mensaje de hasta 250 caracteres" maxlength="250" required="required" pattern="[A-Za-z0-9]{1,20}"></form:textarea>
							</div>
							<form:hidden path="especialista.id"/>
							<form:hidden path="especialista.user.mail"/>
							<form:hidden path="cliente.id"/>
							<form:hidden path="cliente.user.mail"/>
							<form:hidden path="remitente"/>
							<form:hidden path="asunto"/>
							<form:hidden path="urgencia"/>
							
							<form:hidden path="servicio.id"/>
							<form:hidden path="servicioUrgente.id"/>
							<form:hidden path="publicacion.id"/>
							</br>
							<button type="submit" class="btn btn-primary center-block">Enviar mensaje</button>
							</form:form>
<!-- 						</div> -->
					</div>
				</div>

			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***
 _________________________________________________________ -->
		<%@include file="footer.jsp" %>

	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->

	
		<script src="js/star-rating.js" type="text/javascript"></script>

	<script>
		function setCalificacion(obj) {
		    $('#'+obj.getAttribute('id')).on('rating:change', function(event, value, caption) {
		    	
		        console.log(value);
		        console.log(caption);
		        var idServicio = obj.getAttribute("name");
		        
		        var token = $("meta[name='_csrf']").attr("content");
				var header = $("meta[name='_csrf_header']").attr("content");
				
				var datos = {
					id : idServicio,
					calificacion : value
				};

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : 'calificar-servicio2',
					data : JSON.stringify(datos),
					dataType : 'json',
					timeout : 100000,
					beforeSend: function(request) {
					    request.setRequestHeader(header, token);
					 	},
					success : function(datos) {
						console.log("SUCCESS: ", datos);
						display(data);
					},
					error : function(e) {
						console.log("ERROR: ", e);
						display(e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
		    });
		}
	</script>

	 

</body>

</html>

