<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<script type="text/javascript">
		window.onload=function() {
			$("#liMisPublicaciones").attr("class", "active");
		}
	</script>

</head>

<body>
	<%@include file="header.jsp"%>
	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="index.html">Home</a></li>
						<li>Mis publicaciones</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9" id="wishlist">
					<div class="box">
						<h2>Mis publicaciones</h2>
						<p class="lead">�stas son las publicaciones que tenes actualmente!
						<a href="publicar-0" style="float: right;" class="btn btn-success glyphicon glyphicon-plus"> Agregar nueva publicacion</a>
						</p>
					</div>

					<div class="row products">
						<c:forEach items="${publicaciones}" var="publicacion">


							<div class="col-md-3 col-sm-4">
								<div class="product">
									<div class="flip-container">
										<div class="flipper">
											<div class="front">
												<a href="detail-publicacion-${publicacion.id}.html"> <img src="images-upload/${publicacion.foto1}"
													alt="" class="img-responsive center-block" style="min-height:100px; max-height:100px">
												</a>
											</div>
											<div class="back">
												<a href="detail-publicacion-${publicacion.id}.html"> <img src="images-upload/${publicacion.foto2}"
													alt="" class="img-responsive" style="min-height:100px; max-height:100px">
												</a>
											</div>
										</div>
									</div>
									<a href="detail-publicacion-${publicacion.id}.html" class="invisible"> <img
										src="images-upload/${publicacion.foto1}" alt="" class="img-responsive" style="min-height:100px; max-height:100px">
									</a>
									<div class="text">
										<h3>
											<a href="detail-publicacion-${publicacion.id}.html" title="${publicacion.titulo }">${publicacion.titulo }</a>
										</h3>
										<p class="price">$${publicacion.precio }</p>
										<p class="buttons">
											<a href="detail-publicacion-${publicacion.id }.html" class="btn btn-default">Ver publicacion</a>
											<a href="publicar-${publicacion.id}" class="btn btn-primary"><i
												class="fa fa-pencil-square-o"></i>Editar</a>
												<a onclick="setValorBotonEliminarModal(this); return false;" value="delete-${publicacion.id}.html" id="botonEliminar"  class="btn btn-danger btn-ok" data-toggle="modal" data-target="#confirm-delete"><i
												class="fa fa-trash-o"></i>Eliminar</a>
										</p>
									</div>
									<!-- /.text -->
								</div>
								<!-- /.product -->
							</div>
						</c:forEach>
						

					</div>
					<!-- /.products -->

				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***
 _________________________________________________________ -->
 		<%@include file="footer.jsp" %>
		
	
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Eliminar publicacion
            </div>
            <div class="modal-body">
                �Esta seguro que desea eliminar la publicacion?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a href="#" id="buttonEliminarModal" class="btn btn-danger btn-ok">Eliminar</a>
            </div>
        </div>
    </div>
</div>


<!-- 	Script para que se setee el valor del href del boton eliminar del modal -->
	<script type="text/javascript">
	 function setValorBotonEliminarModal(obj) {
		    $("#buttonEliminarModal").attr("href", obj.getAttribute("value"));
	        return false;
	    }
	
	
	</script>

</body>

</html>
