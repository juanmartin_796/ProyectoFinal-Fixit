<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body>

	<div class="col-md-12">
		<ul class="breadcrumb">
			<li><a href="#">Inicio</a></li>
			<li>${subcategoriaSeleccionada.nombre}</li>
		</ul>
	</div>

	<div class="col-md-3">
		<!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
		<div class="panel panel-default sidebar-menu">

			<div class="panel-heading">
				<h3 class="panel-title">Categorias</h3>
			</div>

			<div class="panel-body">
				<ul class="nav nav-pills nav-stacked category-menu">
					<!-- 								<li><a href="category.html">Men <span -->
					<!-- 										class="badge pull-right">42</span></a> -->
					<!-- 									<ul> -->
					<!-- 										<li><a href="category.html">T-shirts</a></li> -->
					<!-- 										<li><a href="category.html">Shirts</a></li> -->
					<!-- 										<li><a href="category.html">Pants</a></li> -->
					<!-- 										<li><a href="category.html">Accessories</a></li> -->
					<!-- 									</ul></li> -->



					<c:forEach items="${categoriasPadres}" var="categoriaPadre">
						<c:choose>
							<c:when
								test="${categoriaPadre.id == subcategoriaSeleccionada.id}">
								<li class="active"><a
									href="category-${categoriaPadre.id}-0-12">${subcategoriaSeleccionada.nombre}
<!-- 										<span class="badge pull-right">123</span> -->
								</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="category-${categoriaPadre.id}-0-12">${categoriaPadre.nombre}
<!-- 										<span class="badge pull-right">42</span> -->
								</a></li>
							</c:otherwise>
						</c:choose>
						<ul>
							<c:forEach items="${categoriaPadre.subCategorias}"
								var="subcategoria">
								<li><a href="category-${subcategoria.id }-0-12">${subcategoria.nombre }</a>
								</li>
							</c:forEach>
						</ul>
						</li>
					</c:forEach>
				</ul>

			</div>
		</div>

		<div class="panel panel-default sidebar-menu">

			<div class="panel-heading">
				<h3 class="panel-title">
					Buscar en 
<!-- 					<a class="btn btn-xs btn-danger pull-right" href="#"><i -->
<!-- 						class="fa fa-times-circle"></i> Limpiar</a> -->
				</h3>
			</div>

			<div class="panel-body">

				<form>
					<div class="form-group">
						<c:forEach items="${ciudades}" var="ciudad">
							<div class="checkbox">
								<label> <a href="categoria-${categoria}-0-12-${ciudad.codPostal}">${ciudad.nombre}</a>
								</label>
							</div>
						</c:forEach>
					</div>

<!-- 					<button class="btn btn-default btn-sm btn-primary"> -->
<!-- 						<i class="fa fa-pencil"></i> Aplicar -->
<!-- 					</button> -->

				</form>

			</div>
		</div>

	
	</div>

</body>
</html>