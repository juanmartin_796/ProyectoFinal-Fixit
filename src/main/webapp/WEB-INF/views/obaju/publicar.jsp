<!DOCTYPE html>
<html lang="es">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="css/select.css"> -->

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

</head>

<body>

	<%@include file="header.jsp"%>
	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Publicar</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9">


					<div class="box" id="contact2">
						<h1>Publicar nuevo servicio</h1>

						<p class="lead">Elige que quieres publicar y trata de
							explicarlo lo mejor posible</p>


						<form:form action="?${_csrf.parameterName}=${_csrf.token}"
							method="POST" modelAttribute="publicacion"
							enctype="multipart/form-data" acceptCharset="ISO-8859-1">

							<div class="col-md-3">
								<h5>Categoria</h5>
								<form:select path="categoria.id" id="categoria.id"
									class="form-control" items="${subcategorias}"
									itemLabel="nombre" itemValue="id">
									<option value="hide">-- Categoria --</option>
								</form:select>
							</div>
							<div class="col-md-9">
								<h5>T��tulo del servicio</h5>
								<form:input class="form-control" path="titulo"
									placeholder="Escribe un titulo descriptivo de su servicio de hasta 80 caracteres"
									type="text" name="ti�tulo" maxlength="80"
									style="text-transform:uppercase" />
							</div>

							<div class="col-md-12">
								<h5>Descripci�n</h5>
								<form:textarea path="cuerpo" class="form-control"
									placeholder="Escribe una descripci�n sobre el servicio que est�s ofreciendo de hasta 250 caracteres"
									name="descripcion" rows="3" cols="20" maxlength="250" />
							</div>

							<!-- 							<div class="col-md-3"> -->
							<!-- 								<h5>Zona de cobertura</h5> -->

							<!-- 								<input class="form-control" -->
							<!-- 									placeholder="Codigo postal de la ciudad de cobertura" -->
							<!-- 									maxlength="4" onkeypress='validate(event)'> -->

							<!-- 							</div> -->
							<div class="col-md-3">
								<h5>Precio</h5>

								<div class="input-group">
									<span class="input-group-addon">$</span>
									<form:input onkeypress='validate(event)' path="precio"
										type="number" min="0.00" step="0.01" name="precio"
										class="form-control" />
								</div>
							</div>
							<div style="visibility: hidden;" class="col-md-9">
								<input type="text"> <input type="text"> <input
									type="text">
							</div>
							<br>
							<p style="visibility: hidden">Artilugio para que se acomode
								todo</p>
							<h5>Fotos</h5>
						Mu�stralo en detalle, con fondo blanco y bien iluminado. No
						incluyas logos, banners ni textos promocionales.
						<br>



							<div style="visibility: hidden;" class="input-group input-file"
								name="Fichier1">
								<form:input path="foto1" type="text" class="form-control"
									placeholder='Elige una imagen...' id="foto1" />
								<span class="input-group-btn">
									<button value="1" class="btn btn-warning btn-reset"
										type="button">Borrar</button>
								</span>
							</div>
							<div>
								<form:input path="fotoFile1" type="file" id="file1"
									class="form-control-file" placeholder='Choose a file...' />
								<c:if
									test="${publicacion.foto1!= '' and publicacion.foto1!= null}">
									<img id="fotoMiniatura1"
										src="images-upload/${publicacion.foto1}" class="img-thumbnail"
										alt="Responsive image" width="100px" height="100px"
										onmouseover="mouseOver(1)" onmouseout="mouseOut(1);">
									<span class="input-group-btn" onclick="borrarImagen(1)">
										<button style="position: absolute; top: -45px; left: 20px;"
											value="1" class="btn btn-warning btn-reset" type="button"
											id="borrar1">Borrar</button> <script type="text/javascript">
												$("#borrar1").hide();
											</script>
									</span>

								</c:if>
							</div>

							<div style="visibility: hidden;" class="input-group input-file"
								name="Fichier2">
								<form:input path="foto2" type="text" class="form-control"
									placeholder='Elige una imagen...' />
								<span class="input-group-btn">
									<button value="2" class="btn btn-warning btn-reset"
										type="button">Borrar</button>
								</span>
							</div>
							<form:input path="fotoFile2" type="file" id="file2"
								class="form-control-file" placeholder='Elige una imagen...' />
							<c:if
								test="${publicacion.foto2!= '' and publicacion.foto1!= null}">
								<img id="fotoMiniatura2"
									src="images-upload/${publicacion.foto2}" class="img-thumbnail"
									alt="Responsive image" width="100px" height="100px"
									onmouseover="mouseOver(2)" onmouseout="mouseOut(2);">
								<span class="input-group-btn" onclick="borrarImagen(2)">
									<button style="position: absolute; top: -45px; left: 20px;"
										value="2" class="btn btn-warning btn-reset" type="button"
										id="borrar2">Borrar</button> <script type="text/javascript">
											$("#borrar2").hide();
										</script>
								</span>
							</c:if>

							<div style="visibility: hidden;" class="input-group input-file"
								name="Fichier3">
								<form:input path="foto3" type="text" class="form-control"
									placeholder='Elige una imagen...' />
								<span class="input-group-btn">
									<button value="3" class="btn btn-warning btn-reset"
										type="button">Borrar</button>
								</span>
							</div>
							<form:input path="fotoFile3" type="file" id="file3"
								class="form-control-file" placeholder='Choose a file...' />
							<c:if
								test="${publicacion.foto3!= '' and publicacion.foto1!= null}">
								<img id="fotoMiniatura3"
									src="images-upload/${publicacion.foto3}" class="img-thumbnail"
									alt="Responsive image" width="100px" height="100px"
									onmouseover="mouseOver(3)" onmouseout="mouseOut(3);">
								<span class="input-group-btn" onclick="borrarImagen(3)">
									<button style="position: absolute; top: -45px; left: 20px;"
										value="3" class="btn btn-warning btn-reset" type="button"
										id="borrar3">Borrar</button> <script type="text/javascript">
											$("#borrar3").hide();
										</script>
								</span>
							</c:if>



							<!-- 						<div class="picture-uploader-preview " id="picture-uploader-preview" style="position: relative; z-index: 1;"> -->
							<!--                                 <ul> -->
							<!--                                         <li data-picture-status="off"> -->
							<!--                                             <p class="picture-uploader-add">Agregar</p> -->
							<!--                                             <div class="picture-uploader-controls"> -->
							<!--                                                 <a role="button" class="ch-close ch-hide" href="#"><span class="ch-hide">Borrar</span></a> -->
							<!--                                             </div> -->
							<!--                                             <p class="picture-uploader-principal">Foto principal</p> -->
							<!--                                         </li> -->

							<!--                                         <li data-picture-status="off"> -->
							<!--                                             <p class="picture-uploader-add">Agregar</p> -->
							<!--                                             <div class="picture-uploader-controls"> -->
							<!--                                                 <a role="button" class="ch-close ch-hide" href="#"><span class="ch-hide">Borrar</span></a> -->
							<!--                                             </div> -->

							<!--                                         </li> -->

							<!--                                 </ul> -->
							<!--                             </div> -->
							<!--                             </p> -->

							<h5>Ingresa un video</h5>
							<div>
								<input class="form-control" type="url" name="video"
									placeholder="Link video de Youtube">
							</div>
							<br>




							<h4>
								Aseg�rate de que tu publicaci�n cumpla con las <a
									href="index.html">pol�ticas de Fix It!</a>
							</h4>


							<form:hidden path="especialista.id" />
							<form:hidden path="id" />

							<button type="submit" class="btn btn-primary center-block">Publicar</button>

						</form:form>
					</div>
				</div>
			</div>
			<!-- /.col-md-9 -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /#content -->

	<hr>

	<div id="footer" data-animate="fadeInUp">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<h4>Pages</h4>

					<ul>
						<li><a href="text.html">About us</a></li>
						<li><a href="text.html">Terms and conditions</a></li>
						<li><a href="faq.html">FAQ</a></li>
						<li><a href="contact.html">Contact us</a></li>
					</ul>

					<hr>

					<h4>User section</h4>

					<ul>
						<li><a href="#" data-toggle="modal"
							data-target="#login-modal">Login</a></li>
						<li><a href="register.html">Regiter</a></li>
					</ul>

					<hr class="hidden-md hidden-lg hidden-sm">

				</div>
				<!-- /.col-md-3 -->

				<div class="col-md-3 col-sm-6">

					<h4>Top categories</h4>

					<h5>Men</h5>

					<ul>
						<li><a href="category.html">T-shirts</a></li>
						<li><a href="category.html">Shirts</a></li>
						<li><a href="category.html">Accessories</a></li>
					</ul>

					<h5>Ladies</h5>
					<ul>
						<li><a href="category.html">T-shirts</a></li>
						<li><a href="category.html">Skirts</a></li>
						<li><a href="category.html">Pants</a></li>
						<li><a href="category.html">Accessories</a></li>
					</ul>

					<hr class="hidden-md hidden-lg">

				</div>
				<!-- /.col-md-3 -->

				<div class="col-md-3 col-sm-6">

					<h4>Where to find us</h4>

					<p>
						<strong>Obaju Ltd.</strong> <br>13/25 New Avenue <br>New
						Heaven <br>45Y 73J <br>England <br> <strong>Great
							Britain</strong>
					</p>

					<a href="contact.html">Go to contact page</a>

					<hr class="hidden-md hidden-lg">

				</div>
				<!-- /.col-md-3 -->



				<div class="col-md-3 col-sm-6">

					<h4>Get the news</h4>

					<p class="text-muted">Pellentesque habitant morbi tristique
						senectus et netus et malesuada fames ac turpis egestas.</p>

					<form>
						<div class="input-group">

							<input type="text" class="form-control"> <span
								class="input-group-btn">

								<button class="btn btn-default" type="button">Subscribe!</button>

							</span>

						</div>
						<!-- /input-group -->
					</form>
					<hr>

					<h4>Stay in touch</h4>

					<p class="social">
						<a href="#" class="facebook external" data-animate-hover="shake"><i
							class="fa fa-facebook"></i></a> <a href="#" class="twitter external"
							data-animate-hover="shake"><i class="fa fa-twitter"></i></a> <a
							href="#" class="instagram external" data-animate-hover="shake"><i
							class="fa fa-instagram"></i></a> <a href="#" class="gplus external"
							data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
						<a href="#" class="email external" data-animate-hover="shake"><i
							class="fa fa-envelope"></i></a>
					</p>
				</div>
			</div>
		</div>


		<div id="copyright">
			<div class="container">
				<div class="col-md-6">
					<p class="pull-left">© 2015 Your name goes here.</p>

				</div>
				<div class="col-md-6">
					<p class="pull-right">
						Template by <a
							href="https://bootstrapious.com/e-commerce-templates">Bootstrapious</a>
						& <a href="https://fity.cz">Fity</a>
						<!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
					</p>
				</div>
			</div>
		</div>
	</div>

	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/bootstrap-hover-dropdown.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/front.js"></script>

</body>

<script type="text/javascript">
	function mouseOver(indice) {
		// 		$('#fotoMiniatura1').fadeTo(0, 0.4);
		$('#borrar' + indice).show();
	}

	function mouseOut(indice) {
		// 		$('#fotoMiniatura1').fadeTo(0, 1);
		$('#borrar' + indice).hide();
	}

	function borrarImagen(indice) {
		$("#borrar" + indice).hide();
		$("#fotoMiniatura" + indice).hide();
		$("#foto" + indice).val("");

	}

	function bs_input_file() {
		$(".input-file")
				.before(
						function() {
							if (!$(this).prev().hasClass('input-ghost')) {
								var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
								element.attr("name", $(this).attr("name"));
								element.change(function() {
									element.next(element).find('input').val(
											(element.val()).split('\\').pop());
								});
								$(this).find("button.btn-choose").click(
										function() {
											element.click();
										});
								$(this).find("button.btn-reset").click(
										function() {
											element.val(null);
											$(this).parents(".input-file")
													.find('input').val('');

											$("#fotoMiniatura" + $(this).val())
													.hide();
										});
								$(this).find('input').css("cursor", "pointer");
								$(this).find('input').mousedown(
										function() {
											$(this).parents('.input-file')
													.prev().click();
											return false;
										});
								return element;
							}
						});
	}
	$(function() {
		bs_input_file();
	});

	function validate(evt) {
		var theEvent = evt || window.event;
		var key = theEvent.keyCode || theEvent.which;

		BACKSPACE = 8;
		DEL = 46;
		TABKEY = 9;
		ARROW_KEYS = {
			left : 37,
			right : 39
		};
		regex = /[0-9]|\./;
		if (key === BACKSPACE || key === DEL || key === ARROW_KEYS.left
				|| key === ARROW_KEYS.right || key === TABKEY) {
			return true;
		}

		key = String.fromCharCode(key);
		var regex = /[0-9,]|\./;
		if (!regex.test(key)) {
			theEvent.returnValue = false;
			if (theEvent.preventDefault)
				theEvent.preventDefault();
		}
	}
</script>



</html>
