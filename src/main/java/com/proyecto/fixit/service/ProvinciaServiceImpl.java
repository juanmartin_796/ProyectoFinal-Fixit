package com.proyecto.fixit.service;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.ProvinciaDao;
import com.proyecto.fixit.model.Provincia;

@Service("provinciaService")
@Transactional
public class ProvinciaServiceImpl implements ProvinciaService{
	@Autowired
	ProvinciaDao provinciaDao;

	@Override
	public List<Provincia> findAllProvincias() {
		return provinciaDao.findAllCategorias();
	}

	@Override
	public Provincia getById(int id) {
		return provinciaDao.getById(id);
	}

}
