package com.proyecto.fixit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.CiudadDao;
import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Provincia;

@Service("ciudadService")
@Transactional
public class CiudadServiceImpl implements CiudadService{
	@Autowired
	CiudadDao dao;
	
	@Override
	public Ciudad getByKey(int key) {
		return dao.getByKey(key);
	}

	@Override
	public List<Ciudad> findAll() {
		return dao.findAll();
	}

	@Override
	public List<Ciudad> findAll(Provincia provincia) {
		return dao.findAll(provincia);
	}

}
