package com.proyecto.fixit.service;

import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.User;

public interface ClienteService {
	Cliente getByUser(User user);
	void updateCliente(Cliente cliente);
	void saveCliente(Cliente cliente);
}
