package com.proyecto.fixit.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.CategoriaDao;
import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.User;

@Service("categoriaService")
@Transactional
public class CategoriaServiceImpl implements CategoriaService {
	@Autowired
	CategoriaDao dao;

	@Override
	public List<Categoria> findAllCategorias() {
		return dao.findAllCategoria();
	}

	@Override
	public List<Categoria> findAllCategoriasPadres() {
		List<Categoria> listCategorias =  findAllCategorias();
		List<Categoria> listCategoriasPadres =  new ArrayList<Categoria>();
		for (int i=0; i< listCategorias.size(); i++) {
			System.out.println("la categoria es: "+ listCategorias.get(i).getNombre());
			if (listCategorias.get(i).getSubCategorias().size()>0) {
				
				listCategoriasPadres.add(listCategorias.get(i));
			}
		}
		return listCategoriasPadres;
	}
	
	@Override
	public List<Categoria> findAllSubcategorias() {
		List<Categoria> listCategorias =  findAllCategorias();
		List<Categoria> listCategoriasPadres =  new ArrayList<Categoria>();
		for (int i=0; i< listCategorias.size(); i++) {
			System.out.println("la categoria es: "+ listCategorias.get(i).getNombre());
			if (listCategorias.get(i).getSubCategorias().size()==0) {
				listCategoriasPadres.add(listCategorias.get(i));
			}
		}
		return listCategoriasPadres;
	}

	@Override
	public Categoria getByKeyCategoria(int key) {
		return dao.getById(key);
	}
	
	@Override
	public Categoria getByNombre(String nombre) {
		return dao.getByNombre	(nombre);
	}
	
	@Override
	public void saveCategoria(Categoria categoria) {
		dao.save(categoria);
	}
	
}
