package com.proyecto.fixit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.MensajeDao;
import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Mensaje;
import com.proyecto.fixit.model.Publicacion;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;

@Transactional
@Service("MensajeService")
public class MensajeServiceImpl implements MensajeService {
	@Autowired
	MensajeDao dao;
	@Override
	public void save(Mensaje mensaje) {
		dao.save(mensaje);
	}

	@Override
	public List<Mensaje> findAll(Especialista especialista, Cliente cliente) {
		return dao.findAll(especialista, cliente);
	}

	@Override
	public List<Mensaje> findAllByServicioUrgente(ServicioUrgente servicioUrgente) {
		return dao.findAllByServicioUrgente(servicioUrgente);
	}

	@Override
	public List<Mensaje> findAll(Especialista especialista) {
		return dao.findAll(especialista);
	}

	@Override
	public List<Mensaje> findAll(Cliente cliente) {
		return dao.findAll(cliente);
	}

	@Override
	public List<Mensaje> findAllDeServicio(Cliente cliente) {
		return dao.findAllByServicios(cliente);
	}

	@Override
	public List<Mensaje> findAllByPublicacion(Publicacion publicacion) {
		return dao.findAllByPublicacion(publicacion);
	}
	

}
