package com.proyecto.fixit.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.PublicacionDao;
import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Publicacion;

@Service("publicacionService")
@Transactional
public class PublicacionServiceImpl implements PublicacionService {
	@Autowired
	PublicacionDao dao;
	
	@Override
	public List<Publicacion> findAllPublicaciones() {
		return dao.findAllPublicacion();
	}

	@Override
	public Publicacion getByKeyPublicaciones(int key) {
		return dao.getById(key);
	}

	@Override
	public List<Publicacion> findAllPublicacionesByCategoriaKey(int keyCategoria) {
		List<Publicacion> listPublicaciones = dao.findAllPublicacion();
		List<Publicacion> listResult = new ArrayList<Publicacion>();
		for (int i=0; i< listPublicaciones.size(); i++) {
			if (listPublicaciones.get(i).getCategoria().getId()== keyCategoria) {
				listResult.add(listPublicaciones.get(i));
			}
		}
		return listResult;
	}

	@Override
	public List<Publicacion> findAllPublicacionesActivasByEspecialista(Especialista especialista) {
		return (List<Publicacion>) dao.findAllActivasByEspecialista(especialista);
	}

	@Override
	public List<Publicacion> getByCriterioBusqueda(String busqueda) {
		return dao.getByCriterioBusqueda(busqueda);
	}

	@Override
	public void deleteById(int id) {
		dao.deleteById(id);
	}

	@Override
	public List<Publicacion> findAllPublicacionesActivas() {
		return dao.findAllPublicacionesActivas();
	}

	@Override
	public void update(Publicacion publicacion) {
		//dao.update(publicacion);
		Publicacion entity= dao.getById(publicacion.getId());
		if(entity!=null){
			System.out.println("entro a la actualizacion de la publicacion");
			entity.setCategoria(publicacion.getCategoria());
			entity.setCuerpo(publicacion.getCuerpo());
			entity.setEspecialista(publicacion.getEspecialista());
			entity.setFoto1(publicacion.getFoto1());
			entity.setFoto2(publicacion.getFoto2());
			entity.setFoto3(publicacion.getFoto3());
			entity.setFoto4(publicacion.getFoto4());
			entity.setPrecio(publicacion.getPrecio());
			entity.setServicios(publicacion.getServicios());
			entity.setTitulo(publicacion.getTitulo());
			entity.setBajaLogica(publicacion.isBajaLogica());
			dao.savePublicacion(entity);
		}
	}

	@Override
	public Double getPromCalifPub(int idPub) {
		return dao.getPromCalifPub(idPub);
	}

	@Override
	public void savePublicacion(Publicacion publicacion) {
		dao.savePublicacion(publicacion);
	}

	@Override
	public List<Publicacion> findAllByCategory(Categoria category, int desde, int hasta) {
		return dao.findAllByCategory(category, desde, hasta);
	}

	@Override
	public List<Publicacion> findAllByCategoryByCiudad(Categoria category, int desde, int hasta, Ciudad ciudad) {
		return dao.findAllByCategoryByCiudad(category, desde, hasta, ciudad);
	}

	@Override
	public List<Publicacion> findAllByCategoryByCiudad(Categoria category, Ciudad ciudad) {
		return dao.findAllByCategoryByCiudad(category, ciudad);
	}

	@Override
	public List<Publicacion> findAll8ultimasActivas() {
		return dao.findAll8ultimasActivas();
	}
}
