package com.proyecto.fixit.service;

import java.util.List;
import java.util.Set;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Servicio;

public interface servicioService {
	void updateServicio(Servicio servicio);
	void save(Servicio servicio);
	List<Servicio> getServiciosByEspecialista(Especialista especialista);
	Servicio findByKey(int key);
}
