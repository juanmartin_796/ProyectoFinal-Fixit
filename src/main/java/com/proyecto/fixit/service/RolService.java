package com.proyecto.fixit.service;

import java.util.List;

import com.proyecto.fixit.model.Rol;

public interface RolService {
	Rol findById(int id);
	Rol findByType(String type);
	List<Rol> findAll();
}
