package com.proyecto.fixit.service;

import com.proyecto.fixit.model.User;

public interface UserService {
	void saveUser(User user);
	User getbyUsername(String username);
	User getById(int id);
}
