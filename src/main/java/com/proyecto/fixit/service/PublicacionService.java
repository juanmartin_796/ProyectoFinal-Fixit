package com.proyecto.fixit.service;

import java.util.List;
import java.util.Locale.Category;

import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Publicacion;

public interface PublicacionService {
	List<Publicacion> findAllPublicaciones();
	List<Publicacion> findAllPublicacionesActivas();
	List<Publicacion> findAll8ultimasActivas();
	List<Publicacion> findAllPublicacionesByCategoriaKey(int keyCategoria);
	Publicacion getByKeyPublicaciones(int key);
	
	List<Publicacion> findAllPublicacionesActivasByEspecialista(Especialista especialista);
	List<Publicacion> getByCriterioBusqueda(String busqueda);
	
	List<Publicacion> findAllByCategory(Categoria category, int desde, int hasta);
	List<Publicacion> findAllByCategoryByCiudad(Categoria category, int desde, int hasta, Ciudad ciudad);
	List<Publicacion> findAllByCategoryByCiudad(Categoria category, Ciudad ciudad);
	
	void deleteById(int id);
	
	void update(Publicacion publicacion);
	void savePublicacion(Publicacion publicacion);
	
	public Double getPromCalifPub(int idPub);

	
}
