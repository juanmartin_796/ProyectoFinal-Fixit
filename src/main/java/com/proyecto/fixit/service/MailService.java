package com.proyecto.fixit.service;

public interface MailService {
	public void sendEmail (String to, String asunto, String text);
}
