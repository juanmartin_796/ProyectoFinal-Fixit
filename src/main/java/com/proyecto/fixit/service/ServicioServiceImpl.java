package com.proyecto.fixit.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.ServicioDao;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Servicio;

@Service("servicioService")
@Transactional
public class ServicioServiceImpl implements servicioService {
	@Autowired
	ServicioDao dao;
	
	@Override
	public void updateServicio(Servicio servicio) {
		dao.updateServicio(servicio);
	}

	@Override
	public List<Servicio> getServiciosByEspecialista(Especialista especialista) {
		return dao.getServiciosByEspecialista(especialista);
	}

	@Override
	public Servicio findByKey(int key) {
		return dao.findByKey(key);
	}

	@Override
	public void save(Servicio servicio) {
		dao.save(servicio);
	}
}
