package com.proyecto.fixit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("mailService")
@Transactional
public class MailServiceImpl implements MailService{
	@Autowired
	JavaMailSender mailSender;
	
	public void sendEmail (String to, String asunto, String text) {
		SimpleMailMessage message= new SimpleMailMessage();
		message.setTo(to);
		message.setSubject(asunto);
		message.setText(text);
		mailSender.send(message);
	}
	
	
}
