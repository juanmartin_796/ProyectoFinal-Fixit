package com.proyecto.fixit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.UserDao;
import com.proyecto.fixit.model.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	UserDao dao;
	
	@Override
	public void saveUser(User user) {
		dao.save(user);
	}

	@Override
	public User getbyUsername(String username) {
		return dao.getByUsername(username);
	}

	@Override
	public User getById(int id) {
		return dao.getById(id);
	}

}
