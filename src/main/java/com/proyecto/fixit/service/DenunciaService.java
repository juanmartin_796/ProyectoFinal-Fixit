package com.proyecto.fixit.service;

import com.proyecto.fixit.model.Denuncia;

public interface DenunciaService {
	void save(Denuncia denuncia);
}
