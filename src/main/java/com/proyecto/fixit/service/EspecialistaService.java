package com.proyecto.fixit.service;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.User;

public interface EspecialistaService {
	Especialista getByKeyEspecialista(int key);
	Especialista getByUsernameEspecialista(User user);
	void save(Especialista especialista);
	void update(Especialista especialista);
}
