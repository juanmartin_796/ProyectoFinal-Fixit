package com.proyecto.fixit.service;

import java.util.List;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;

public interface ServicioUrgenteService {
	List<ServicioUrgente> findAllMenores24Hs();
	void saveServicioUrgente(ServicioUrgente servicioUrgente) throws Exception;
	List<ServicioUrgente> getServiciosUrgentesMenoresDistancia(float latActual, float lngActual, int idEspecialista);
	ServicioUrgente getById(int id);
	List<ServicioUrgente> findAllByUser(User user);
	void update(ServicioUrgente servicioUrgente);
	void delete(ServicioUrgente servicioUrgente);
}
