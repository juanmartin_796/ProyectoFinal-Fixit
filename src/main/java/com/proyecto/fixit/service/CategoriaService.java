package com.proyecto.fixit.service;

import java.util.List;

import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.User;

public interface CategoriaService {
	List<Categoria> findAllCategorias();	
	Categoria getByKeyCategoria(int key);
	Categoria getByNombre(String nombre);
	List<Categoria> findAllCategoriasPadres();	
	List<Categoria> findAllSubcategorias();
	void saveCategoria(Categoria categoria);
}
