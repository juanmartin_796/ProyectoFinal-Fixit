package com.proyecto.fixit.service;

import java.util.List;

import com.proyecto.fixit.model.Provincia;

public interface ProvinciaService {
	List<Provincia> findAllProvincias();
	Provincia getById(int id);
}
