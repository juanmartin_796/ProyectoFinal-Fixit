package com.proyecto.fixit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.EspecialistaDao;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.User;

@Transactional
@Service
public class EspecialistaServiceImpl implements EspecialistaService {
	@Autowired
	EspecialistaDao dao;
	
	@Override
	public Especialista getByKeyEspecialista(int key) {
		return dao.getByKey(key);
	}

	@Override
	public Especialista getByUsernameEspecialista(User user) {
		return dao.getByUsername(user);
	}

	@Override
	public void save(Especialista especialista) {
		dao.save(especialista);
	}

	@Override
	public void update(Especialista especialista) {
		dao.update(especialista);
	}

}
