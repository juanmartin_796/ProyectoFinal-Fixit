package com.proyecto.fixit.service;

import java.util.List;

import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Provincia;

public interface CiudadService {
	Ciudad getByKey(int key);
	List<Ciudad> findAll();
	List<Ciudad> findAll(Provincia provincia);
}
