package com.proyecto.fixit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.DenunciaDao;
import com.proyecto.fixit.model.Denuncia;

@Transactional
@Service("denunciaService")
public class DenunciaServiceImpl implements DenunciaService{
	@Autowired
	DenunciaDao dao;
	
	@Override
	public void save(Denuncia denuncia) {
		dao.save(denuncia);
	}

}
