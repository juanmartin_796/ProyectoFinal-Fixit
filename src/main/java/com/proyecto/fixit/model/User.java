package com.proyecto.fixit.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "User")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotEmpty(message="No puede estar vacio")
	@Column(name = "Username", unique = true, nullable = false)
	private String username;
	
	@Email(message="La direccion de correo electrónico no es valida")
	@NotEmpty(message="No puede estar vacio")
	private String mail;

	//@NotEmpty
	@NotEmpty(message="No puede estar vacio")
	@NotNull(message="No puede estar vacio")
	@Column(name = "Password", nullable = false)
	private String password;

	//@NotEmpty //No puede ir notEmpty para un booleano
	@NotNull
	@Column(name = "Enabled", nullable = false)
	private boolean enabled;

	//@NotEmpty(message="Debe elegir una de las opciones")
	@NotNull
	//@ManyToMany(fetch = FetchType.LAZY)
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "User_Rol", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "rol_id") })
	private Set<Rol> roles = new HashSet<Rol>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Rol> getRoles() {
		return roles;
	}

	public void setRoles(Set<Rol> roles) {
		this.roles = roles;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
}
