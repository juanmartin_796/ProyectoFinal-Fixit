package com.proyecto.fixit.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="ServicioUrgente")
public class ServicioUrgente {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@DateTimeFormat(pattern="yyyy-MM-dd'T'hh:mm")
	Timestamp dateTime;
	
	@DateTimeFormat(pattern="yyyy-MM-dd'T'hh:mm")
	Timestamp tiempoLimiteParaRespuesta;

	@NotNull
	@ManyToOne
	Categoria categoria;
	
	@NotNull
	@ManyToOne
	User user;
	
	@ManyToOne (cascade=CascadeType.ALL)
	Especialista especialista;
	
	@NotNull
	@Length(min=1, max=300, message="No debe superar los 150 caracteres")
	String descripcion;
	
	@NotNull
	Float latitud;
	
	@NotNull
	Float longitud;
	
	int distanciaMax;
	
	boolean notificacionEnviada=false;
	
	boolean notificacionLeida= false;
		
	public boolean isNotificacionEnviada() {
		return notificacionEnviada;
	}

	public void setNotificacionEnviada(boolean notificacionEnviada) {
		this.notificacionEnviada = notificacionEnviada;
	}

	public boolean isNotificacionLeida() {
		return notificacionLeida;
	}

	public void setNotificacionLeida(boolean notificacionLeida) {
		this.notificacionLeida = notificacionLeida;
	}

	public int getDistanciaMax() {
		return distanciaMax;
	}

	public void setDistanciaMax(int distanciaMax) {
		this.distanciaMax = distanciaMax;
	}

	public Float getLatitud() {
		return latitud;
	}

	public void setLatitud(Float latitud) {
		this.latitud = latitud;
	}

	public Float getLongitud() {
		return longitud;
	}

	public void setLongitud(Float longitud) {
		this.longitud = longitud;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Especialista getEspecialista() {
		return especialista;
	}

	public void setEspecialista(Especialista especialista) {
		this.especialista = especialista;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

//	public Timestamp getDateTime() {
//		return dateTime;
//	}
	public String getDiferenciaHoraActual() {
		Timestamp actual = new Timestamp(System.currentTimeMillis());
		double diferenciaMinutos= (actual.getTime()-dateTime.getTime())/1000/60;
		if (diferenciaMinutos <60) { //menos de una hora
			return (int)diferenciaMinutos+" minutos";
		} else{
			int difHoras=  (int)(diferenciaMinutos/60);
			int difMinEnHora = 60 - (int) (( ( (difHoras)+1 ) -(diferenciaMinutos/60))*60);
			return (difHoras + "hs " + difMinEnHora + "min");
		}
	}
	
	public Timestamp getDateTime() {
		return dateTime; 
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

	public Timestamp getTiempoLimiteParaRespuesta() {
		return tiempoLimiteParaRespuesta;
	}

	public void setTiempoLimiteParaRespuesta(Timestamp tiempoLimiteParaRespuesta) {
		this.tiempoLimiteParaRespuesta = tiempoLimiteParaRespuesta;
	}
}
