package com.proyecto.fixit.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Publicacion")
public class Publicacion {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	int id;
	
	boolean bajaLogica=false;
	
	@NotNull
	@ManyToOne
	Especialista especialista;
	
	@JsonIgnore
	@OneToMany(mappedBy="publicacion")
	Set<Servicio> servicios= new HashSet<Servicio>();
	
	@NotNull
	@ManyToOne
	Categoria categoria;

	@Size(min=2, max=200)
	String titulo;
	
	@Size(min=0, max=2000)
	String cuerpo;
	
	double precio;
	
	String foto1;
	
	String foto2;
	
	String foto3;
	
	String foto4;
	
	@Transient //Ignora el matcheo con la base de datos
	MultipartFile fotoFile4;
	
	@Transient //Ignora el matcheo con la base de datos
	MultipartFile fotoFile3;
	
	@Transient //Ignora el matcheo con la base de datos
	MultipartFile fotoFile2;
	
	@Transient //Ignora el matcheo con la base de datos
	MultipartFile fotoFile1;
	
	@ManyToOne(optional=true)
	Ciudad ciudad;

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public MultipartFile getFotoFile3() {
		return fotoFile3;
	}

	public void setFotoFile3(MultipartFile fotoFile3) {
		this.fotoFile3 = fotoFile3;
	}

	public MultipartFile getFotoFile2() {
		return fotoFile2;
	}

	public void setFotoFile2(MultipartFile fotoFile2) {
		this.fotoFile2 = fotoFile2;
	}

	public MultipartFile getFotoFile1() {
		return fotoFile1;
	}

	public void setFotoFile1(MultipartFile fotoFile1) {
		this.fotoFile1 = fotoFile1;
	}

	public MultipartFile getFotoFile4() {
		return fotoFile4;
	}

	public void setFotoFile4(MultipartFile foto5) {
		this.fotoFile4 = foto5;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Especialista getEspecialista() {
		return especialista;
	}

	public void setEspecialista(Especialista especialista) {
		this.especialista = especialista;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public String getFoto1() {
		return foto1;
	}

	public void setFoto1(String foto1) {
		this.foto1 = foto1;
	}

	public String getFoto2() {
		return foto2;
	}

	public void setFoto2(String foto2) {
		this.foto2 = foto2;
	}

	public String getFoto3() {
		return foto3;
	}

	public void setFoto3(String foto3) {
		this.foto3 = foto3;
	}

	public String getFoto4() {
		return foto4;
	}

	public void setFoto4(String foto4) {
		this.foto4 = foto4;
	}

	public boolean isBajaLogica() {
		return bajaLogica;
	}

	public void setBajaLogica(boolean bajaLogica) {
		this.bajaLogica = bajaLogica;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Set<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(Set<Servicio> servicios) {
		this.servicios = servicios;
	}
	
	
	
	
	
}
