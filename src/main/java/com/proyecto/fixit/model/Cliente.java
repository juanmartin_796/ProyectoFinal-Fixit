package com.proyecto.fixit.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="Cliente")
public class Cliente {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
		
	@NotNull(message="No puede estar vacio")
	@Length(min=2, max=40, message="La longitud debe ser mayor a 1 y menor a 40")
	String nombre;
	
	@NotNull
	@Length(min=2, max=40, message="La longitud debe ser mayor a 1 y menor a 40")
	String apellido;
	
	@NotNull
	@OneToOne
	User user;
	
	@OneToMany(mappedBy = "cliente", fetch=FetchType.EAGER)
	@JsonManagedReference
	Set<Servicio> serviciosContratados= new HashSet<Servicio>();
	
	@Valid
	@NotNull
	@OneToOne(cascade=CascadeType.ALL) //puse el cascadeType.all porque o sino me saltaba un error al actualizar los datos del cliente
	Direccion direccion;
 
	@Pattern(regexp="(^$|[0-9]{10})", message="El numero de telefono debe contener 10 digitos. Ej. 3794816263")
	String telefono;
	
	@Email(message="La direccion de correo electrónico no es valida")
	String email;
	
	float latitud;
	
	float longitud;
	
	public float getLatitud() {
		return latitud;
	}

	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}

	public float getLongitud() {
		return longitud;
	}

	public void setLongitud(float longitud) {
		this.longitud = longitud;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Servicio> getServiciosContratados() {
		return serviciosContratados;
	}

	public void setServiciosContratados(Set<Servicio> serviciosContratados) {
		this.serviciosContratados = serviciosContratados;
	}
}
