package com.proyecto.fixit.dao;

import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Denuncia;

@Repository("denunciaDao")
public class DenunciaDaoImpl extends AbstractDao<Integer, Denuncia> implements DenunciaDao{

	@Override
	public void save(Denuncia denuncia) {
		getSession().save(denuncia);
	}
}
