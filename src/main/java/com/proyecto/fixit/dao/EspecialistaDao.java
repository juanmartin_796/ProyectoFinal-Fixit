package com.proyecto.fixit.dao;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.User;

public interface EspecialistaDao {
	Especialista getByKey(int key);
	Especialista getByUsername(User user);
	void save(Especialista especialista);
	void update(Especialista especialista);

}
