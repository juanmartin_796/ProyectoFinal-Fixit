package com.proyecto.fixit.dao;

import java.util.List;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Servicio;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;

public interface ServicioUrgenteDao {
	List<ServicioUrgente> findAll();
	void saveServicioUrgente(ServicioUrgente servicioUrgente) throws Exception;
	ServicioUrgente getById(int id);
	List<ServicioUrgente> findAllByUser(User user);
	void update(ServicioUrgente servicioUrgente);
	void delete(ServicioUrgente servicioUrgente);
}
