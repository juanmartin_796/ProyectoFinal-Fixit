package com.proyecto.fixit.dao;

import java.util.List;

import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.User;

public interface CategoriaDao {
	List<Categoria> findAllCategoria();
	Categoria getByNombre(String nombre);
	Categoria getById(int key);
	void save (Categoria categoria);
}
