package com.proyecto.fixit.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.spi.TypedValue;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Servicio;

@Repository("servicioDao")
public class ServicioDaoImpl extends AbstractDao<Integer, Servicio> implements ServicioDao {

	@Override
	public void updateServicio(Servicio servicio) {
		super.getSession().update(servicio);
	}

	@Override
	public List<Servicio> getServiciosByEspecialista(Especialista especialista) {
		Criteria criteria= createEntityCriteria();
		String aliasTablaServicio = criteria.getAlias();
		criteria.createCriteria(aliasTablaServicio+".publicacion", "aliasTablaPublicacion");
		criteria.add(Restrictions.eq("aliasTablaPublicacion.especialista", especialista));
		return (List<Servicio>) criteria.list();
	}

	@Override
	public Servicio findByKey(int key) {
		return super.getByKey(key);
	}

	@Override
	public void save(Servicio servicio) {
		getSession().merge(servicio);
	}
}
