package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Provincia;

@Repository("ciudadDao")
public class CiudadDaoImpl extends AbstractDao<Integer, Ciudad> implements CiudadDao{

	@Override
	public Ciudad getByKey(int key) {
		return super.getByKey(key);
	}

	@Override
	public List<Ciudad> findAll() {
		Criteria criteria= createEntityCriteria();
		List<Ciudad> list = (List<Ciudad>) criteria.list();
		return list;
	}

	@Override
	public List<Ciudad> findAll(Provincia provincia) {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("provincia", provincia));
		return criteria.list();
	}

}
