package com.proyecto.fixit.dao;

import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.User;

public interface ClienteDao {
	Cliente findById(int id);
	Cliente getByUser(User user);
	void save(Cliente cliente);
}
