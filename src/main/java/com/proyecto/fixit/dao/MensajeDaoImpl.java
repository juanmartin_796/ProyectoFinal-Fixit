package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Mensaje;
import com.proyecto.fixit.model.Publicacion;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;

@Repository("mensajeDao")
public class MensajeDaoImpl extends AbstractDao<Integer, Mensaje> implements MensajeDao {

	@Override
	public List<Mensaje> findAll(Especialista especialista, Cliente cliente) {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("cliente", cliente)).add(Restrictions.eq("especialista", especialista));
		return criteria.list();
	}

	@Override
	public void save(Mensaje mensaje) {
		getSession().save(mensaje);
	}

	@Override
	public List<Mensaje> findAllByServicioUrgente(ServicioUrgente servicioUrgente) {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("servicioUrgente", servicioUrgente));
		criteria.addOrder(Order.asc("FechaHora"));
		//criteria.setProjection(Projections.countDistinct("id"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@Override
	public List<Mensaje> findAll(Especialista especialista) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("especialista", especialista));
		criteria.add(Restrictions.isNotNull("servicioUrgente"));
		ProjectionList projection = Projections.projectionList();
		projection.add(Projections.groupProperty("servicioUrgente"), "servicioUrgente");
		projection.add(Projections.max("FechaHora"), "FechaHora");
		projection.add(Projections.property("asunto"),"asunto");
		projection.add(Projections.property("cliente"),"cliente");
		criteria.setProjection(projection);		
		criteria.addOrder(Order.desc("FechaHora"));
		criteria.setResultTransformer(Transformers.aliasToBean(Mensaje.class));
		
		Criteria criteriaPublicacion = createEntityCriteria();
		criteriaPublicacion.add(Restrictions.eq("especialista", especialista));
		criteriaPublicacion.add(Restrictions.isNotNull("publicacion"));
		ProjectionList projectionPublicacion = Projections.projectionList();
		projectionPublicacion.add(Projections.groupProperty("publicacion"), "publicacion");
		projectionPublicacion.add(Projections.max("FechaHora"), "FechaHora");
		projectionPublicacion.add(Projections.property("asunto"),"asunto");
		projectionPublicacion.add(Projections.property("cliente"),"cliente");
		criteriaPublicacion.setProjection(projectionPublicacion);		
		criteriaPublicacion.addOrder(Order.desc("FechaHora"));
		criteriaPublicacion.setResultTransformer(Transformers.aliasToBean(Mensaje.class));
		
		List<Mensaje> lista = criteria.list();
		lista.addAll(criteriaPublicacion.list());
		
		return lista;
	}

	@Override
	public List<Mensaje> findAll(Cliente cliente) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("cliente", cliente));
		ProjectionList projection = Projections.projectionList();
		projection.add(Projections.groupProperty("servicioUrgente"), "servicioUrgente");
		criteria.add(Restrictions.isNotNull("servicioUrgente"));
		projection.add(Projections.max("FechaHora"), "FechaHora");
		projection.add(Projections.property("asunto"),"asunto");
		projection.add(Projections.property("cliente"),"cliente");
		projection.add(Projections.property("especialista"),"especialista");
		criteria.setProjection(projection);		
		criteria.addOrder(Order.desc("FechaHora"));
		criteria.setResultTransformer(Transformers.aliasToBean(Mensaje.class));
		
		System.out.println("Los mensajes son del daoooooooooooooooooooooooooooooo criteria: "+criteria.list());
				
		Criteria criteriaPublicacion = createEntityCriteria();
		criteriaPublicacion.add(Restrictions.eq("cliente", cliente));
		criteriaPublicacion.add(Restrictions.isNotNull("publicacion"));
		ProjectionList projectionPublicacion = Projections.projectionList();
		projectionPublicacion.add(Projections.groupProperty("publicacion"), "publicacion");
		projectionPublicacion.add(Projections.max("FechaHora"), "FechaHora");
		projectionPublicacion.add(Projections.property("asunto"),"asunto");
		projectionPublicacion.add(Projections.property("cliente"),"cliente");
		projectionPublicacion.add(Projections.property("especialista"),"especialista");
		criteriaPublicacion.setProjection(projectionPublicacion);		
		criteriaPublicacion.addOrder(Order.desc("FechaHora"));
		criteriaPublicacion.setResultTransformer(Transformers.aliasToBean(Mensaje.class));
		
		System.out.println("Los mensajes son del daoooooooooooooooooooooooooooooo criteriaPublicacion: "+criteriaPublicacion.list());
		
		List<Mensaje> lista= criteria.list();
		lista.addAll(criteriaPublicacion.list());
		
		return lista;

	}

	@Override
	public List<Mensaje> findAllByServicios(Cliente cliente) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("cliente", cliente));
		ProjectionList projection = Projections.projectionList();
		projection.add(Projections.groupProperty("servicio"), "servicio");
		
		projection.add(Projections.max("FechaHora"), "FechaHora");
		projection.add(Projections.property("asunto"),"asunto");
		projection.add(Projections.property("cliente"),"cliente");
		projection.add(Projections.property("especialista"),"especialista");
		criteria.setProjection(projection);		
		criteria.addOrder(Order.desc("FechaHora"));
		criteria.setResultTransformer(Transformers.aliasToBean(Mensaje.class));
		return criteria.list();
	}

	@Override
	public List<Mensaje> findAllByPublicacion(Publicacion publicacion) {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("publicacion", publicacion));
		criteria.addOrder(Order.asc("FechaHora"));
		//criteria.setProjection(Projections.countDistinct("id"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

}
