package com.proyecto.fixit.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale.Category;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CountProjection;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Map;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.expression.spel.ast.Projection;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Mensaje;
import com.proyecto.fixit.model.Publicacion;

@Repository("publicacionDao")
public class PublicacionDaoImpl extends AbstractDao<Integer, Publicacion> implements PublicacionDao{
	
	@Override
	public List<Publicacion> findAllPublicacion() {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("bajaLogica", false));
		return (List<Publicacion>)criteria.list();
	}
	
	@Override
	public List<Publicacion> findAllPublicacionesActivas() {
		Criteria criteria= createEntityCriteria();
		return (List<Publicacion>)criteria.list();
	}
	

	@Override
	public Publicacion getById(int key) {
		return getByKey(key);
	}

	@Override
	public List<Publicacion> findAllActivasByEspecialista(Especialista especialista) {
		Criteria criteria= createEntityCriteria();
		Criteria lista = criteria.add(Restrictions.eq("especialista", especialista));
		criteria.add(Restrictions.eq("bajaLogica", false));
		return (List<Publicacion>)lista.list();
	}
	
	@Override
	public List<Publicacion> getByCriterioBusqueda(String criterioBusqueda){
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.like("titulo", "%"+criterioBusqueda+"%"));
				
		Criteria criteria2 = createEntityCriteria();
		criteria2.add(Restrictions.like("cuerpo", "%"+criterioBusqueda+"%"));
		
		Set<Publicacion> hs = new HashSet<Publicacion>(); //Utilizao esto para que nose carguen por duplicado
		hs.addAll(criteria.list());
		hs.addAll(criteria2.list());
		
		List<Publicacion> listPublicacion = new ArrayList<Publicacion>();
		listPublicacion.addAll(hs);
		return listPublicacion;
	}

	@Override
	public void deleteById(int id) {
		super.delete(getById(id));
	}

	@Override
	public void update(Publicacion publicacion) {
		getSession().update(publicacion);
	}
	
	public Double getPromCalifPub(int idPub) {
		Criteria criteria= super.createEntityCriteria();
		String aliasPublicacion = criteria.add(Restrictions.eq("id", idPub)).getAlias();
		criteria.createCriteria(aliasPublicacion+".servicios", "aliasTablaServicios");
		criteria.setProjection(Projections.avg("aliasTablaServicios.calificacion"));
		return (Double) criteria.uniqueResult();

		
	}

	@Override
	public void savePublicacion(Publicacion publicacion) {
		getSession().save(publicacion);
	}

	@Override
	public List<Publicacion> findAllByCategory(Categoria category, int desde, int hasta) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("categoria", category));
		criteria.add(Restrictions.eq("bajaLogica", false));
		criteria.setFirstResult(desde);
		criteria.setMaxResults(hasta);
		return criteria.list();
	}

	@Override
	public List<Publicacion> findAllByCategoryByCiudad(Categoria category, int desde, int hasta, Ciudad ciudad) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("categoria", category));
		criteria.add(Restrictions.eq("bajaLogica", false));
		//criteria.add(Restrictions.eq("ciudad", ciudad));
		
		criteria.createAlias("especialista", "e");
		criteria.createAlias("e.direccion", "d");
		criteria.add(Restrictions.eq("d.ciudad", ciudad));

		
		

		
		criteria.setFirstResult(desde);
		criteria.setMaxResults(hasta);
		//criteria.setResultTransformer(Transformers.aliasToBean(Publicacion.class));
		//criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		//criteria.setResultTransformer(Criteria.ROOT_ENTITY);
		//criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		List<Publicacion> list = criteria.list();
		System.out.println("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Publicacion publicacion = (Publicacion) iterator.next();
			System.out.println("La publicacion es "+ publicacion.getCuerpo()+ publicacion.getCiudad());
		}
		return criteria.list();
	}

	@Override
	public List<Publicacion> findAllByCategoryByCiudad(Categoria category, Ciudad ciudad) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("categoria", category));
		criteria.add(Restrictions.eq("bajaLogica", false));
		criteria.add(Restrictions.eq("ciudad", ciudad));
		return criteria.list();
	}

	@Override
	public List<Publicacion> findAll8ultimasActivas() {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("bajaLogica", false));
		criteria.setMaxResults(15);
		criteria.addOrder(Order.desc("id"));
		return (List<Publicacion>)criteria.list();
	}
	
}
