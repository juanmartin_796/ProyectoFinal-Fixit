package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.User;

@Repository("categoriaDao")
public class CategoriaDaoImp extends AbstractDao<Integer, Categoria> implements CategoriaDao {

	@Override
	public List<Categoria> findAllCategoria() {
		Criteria criteria= createEntityCriteria();
		List<Categoria> list = (List<Categoria>) criteria.list();
		return list;
	}

	@Override
	public Categoria getById(int key) {
		return getByKey(key);
	}
	
	@Override
	public Categoria getByNombre(String nombre) {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("nombre", nombre));
		return (Categoria) criteria.uniqueResult();
	}
	
	@Override
	public void save(Categoria categoria) {
		persist(categoria);
	}

}
