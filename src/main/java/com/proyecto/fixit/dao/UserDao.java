package com.proyecto.fixit.dao;

import com.proyecto.fixit.model.User;

public interface UserDao {
	void save (User user);
	User getByUsername(String username);
	User getById(int id);
}
