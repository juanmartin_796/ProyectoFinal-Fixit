package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Servicio;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;

@Repository("servicioUrgente")
public class ServicioUrgenteDaoImpl extends AbstractDao<Integer, ServicioUrgente> implements ServicioUrgenteDao {

	@Override
	public List<ServicioUrgente> findAll() {
		Criteria criteria= createEntityCriteria();
		List<ServicioUrgente> list= criteria.list();
		return list;
	}

	@Override
	public void saveServicioUrgente(ServicioUrgente servicioUrgente) throws Exception {
		ServicioUrgente servicioUrgenteViejo= getById(servicioUrgente.getId());
		if (servicioUrgenteViejo== null || servicioUrgenteViejo.getEspecialista()==null) {
			getSession().merge(servicioUrgente);				
		} else {
			System.out.println("No se pudo asignar el trabajo, porque otra persona ya la acepto");
			throw new Exception("No se pudo asignar el trabajo, porque otra persona ya la acepto");
		}
	}

	@Override
	public ServicioUrgente getById(int id) {
		return this.getByKey(id);
	}
	
	@Override
	public List<ServicioUrgente> findAllByUser(User user) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user", user));
		return criteria.list();
	}

	@Override
	public void update(ServicioUrgente servicioUrgente) {
		getSession().merge(servicioUrgente);
	}
	
	@Override
	public void delete(ServicioUrgente servicioUrgente) {
		getSession().delete(servicioUrgente);
	}
}
