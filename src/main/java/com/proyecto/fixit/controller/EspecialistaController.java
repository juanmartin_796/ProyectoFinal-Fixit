package com.proyecto.fixit.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonView;
import com.proyecto.fixit.dao.UserDao;
import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.FileBucket;
import com.proyecto.fixit.model.Mensaje;
import com.proyecto.fixit.model.Publicacion;
import com.proyecto.fixit.model.Rol;
import com.proyecto.fixit.model.Servicio;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;
import com.proyecto.fixit.service.CategoriaService;
import com.proyecto.fixit.service.ClienteService;
import com.proyecto.fixit.service.EspecialistaService;
import com.proyecto.fixit.service.MailService;
import com.proyecto.fixit.service.MensajeService;
import com.proyecto.fixit.service.ProvinciaService;
import com.proyecto.fixit.service.PublicacionService;
import com.proyecto.fixit.service.ServicioUrgenteService;
import com.proyecto.fixit.service.UserService;
import com.proyecto.fixit.service.servicioService;

@Controller
public class EspecialistaController {
	@Autowired
	ServicioUrgenteService servicioUrgenteService;

	@Autowired
	servicioService servicioService;

	@Autowired
	CategoriaService categoriaService;

	@Autowired
	PublicacionService publicacionService;

	@Autowired
	UserService userService;

	@Autowired
	EspecialistaService especialistaService;

	@Autowired
	ClienteService clienteService;

	@Autowired
	MailService mailService;

	@Autowired
	MensajeService mensajeService;

	@RequestMapping(value = "/customer-wishlist", method = RequestMethod.GET)
	public String getPublicacionesEspecialista(Model model) {
		crearHeader(model);
		String usernameEspecialista = getPrincipal();

		User user = userService.getbyUsername(usernameEspecialista);
		Especialista especialista = especialistaService.getByUsernameEspecialista(user);
		List<Publicacion> listPublicacionDeEspecialista = publicacionService
				.findAllPublicacionesActivasByEspecialista(especialista);
		// System.out.println(listPublicacionDeEspecialista.get(0).getId());
		// System.out.println(listPublicacionDeEspecialista.get(0).getCuerpo());
		// System.out.println(especialista.getNombre());
		model.addAttribute("especialista", especialista);
		model.addAttribute("publicaciones", listPublicacionDeEspecialista);
		return "obaju/customer-wishlist";
	}

	@RequestMapping(value = "misserviciosurgentes", method = RequestMethod.GET)
	public String misServiciosUrgentes(Model model) {
		// ArrayList<ServicioUrgente> listUrgentes= (ArrayList<ServicioUrgente>)
		// servicioUrgenteService.findAllMenores24Hs();
		User user = userService.getbyUsername(getPrincipal());
		Cliente cliente = clienteService.getByUser(user);
		Especialista especialista = especialistaService.getByUsernameEspecialista(user);
		ArrayList<ServicioUrgente> listUrgentes = (ArrayList<ServicioUrgente>) servicioUrgenteService
				.getServiciosUrgentesMenoresDistancia(cliente.getLatitud(), cliente.getLongitud(),
						especialista.getId());
		List<Publicacion> listPublicacionesEsp = publicacionService
				.findAllPublicacionesActivasByEspecialista(especialista);
		ArrayList<ServicioUrgente> listUrgentesFiltrado = new ArrayList<ServicioUrgente>();
		for (ServicioUrgente servicioUrgente : listUrgentes) {
			boolean band = false;
			for (Iterator iterator = listPublicacionesEsp.iterator(); iterator.hasNext();) {
				Publicacion publicacion = (Publicacion) iterator.next();
				if (publicacion.getCategoria().getId() == servicioUrgente.getCategoria().getId()) {
					band = true;
				}
			}
			if (band == true) {
				listUrgentesFiltrado.add(servicioUrgente);
			}
		}
		Long fechaHoraActual = System.currentTimeMillis();
		model.addAttribute("listaUrgentes", listUrgentesFiltrado);
		model.addAttribute("fechaHoraActual", fechaHoraActual);
		crearHeader(model);
		return "obaju/customer-orders";
	}

	@RequestMapping(value = { "aceptarUrgencia-{idPublicacionUrgente}" }, method = RequestMethod.GET)
	public String aceptarUrgencia(@PathVariable int idPublicacionUrgente, Model model) {
		ServicioUrgente servicioUrgente = servicioUrgenteService.getById(idPublicacionUrgente);
		User user = userService.getbyUsername(getPrincipal());
		Especialista especialista = especialistaService.getByUsernameEspecialista(user);
		servicioUrgente.setEspecialista(especialista);
		servicioUrgente.setNotificacionEnviada(true);
		try {
			// mailService.sendEmail(servicioUrgente.getUser().getMail(), "Tu solicitud de
			// urgencia ha sido aceptada", especialista.getUser().getUsername() + " ha
			// aceptado tu solicitud. Enviale un mensaje desde AQUI");
			servicioUrgenteService.saveServicioUrgente(servicioUrgente);
		} catch (Exception e) {
			model.addAttribute("mensajeError", e.getMessage());
			return "obaju/error";
		}
		// return "obaju/sucess";
		return "redirect:misserviciosurgentes";
	}

	@RequestMapping(value = { "/delete-{idPublicacion}" }, method = RequestMethod.GET)
	public String deletePublicacion(@PathVariable int idPublicacion, Model model) {
		crearHeader(model);
		Publicacion publicacion = publicacionService.getByKeyPublicaciones(idPublicacion);
		if (publicacion.getEspecialista().getUser().getUsername().equals(getPrincipal())) {
			// publicacionService.deleteById(idPublicacion);
			publicacion.setBajaLogica(true);
			publicacionService.update(publicacion);
			System.out.println("entro a borrar supuestamente");
			return "redirect:/customer-wishlist";
		} else {
			return "obaju/index";
		}
	}

	// @RequestMapping(value="/publicar", method=RequestMethod.GET)
	// public String publicarServicio(Model model) {
	@RequestMapping(value = { "/publicar-{idPublicacion}" }, method = RequestMethod.GET)
	public String publicarServicio(@PathVariable int idPublicacion, Model model) {
		crearHeader(model);
		Publicacion publicacion;
		publicacion = publicacionService.getByKeyPublicaciones(idPublicacion);
		if (publicacion == null) {
			publicacion = new Publicacion();
		}
		model.addAttribute("publicacion", publicacion);

		List<Categoria> categoriasPadres = categoriaService.findAllCategoriasPadres();
		List<Categoria> categoriasAll = categoriaService.findAllCategorias();
		List<Categoria> subcategorias = categoriaService.findAllSubcategorias();
		model.addAttribute("subcategorias", subcategorias);
		return "obaju/publicar";
	}

	private static String UPLOAD_LOCATION = "/home/juanmartin/imagenesUploadFixit";

	@RequestMapping(value = { "/publicar-{idPublicacion}" }, method = RequestMethod.POST)
	public String publicarServicioPost(@Valid Publicacion publicacion, BindingResult result, ModelMap model,
			@PathVariable String idPublicacion) {
		if (Integer.valueOf(idPublicacion) == 0) { // Nueva publicacion
			User user = userService.getbyUsername(getPrincipal());
			Especialista especialista = especialistaService.getByUsernameEspecialista(user);
			publicacion.setEspecialista(especialista);

			MultipartFile multipartFile = publicacion.getFotoFile4();
			// Now do something with file...
			try {
				// System.out.println("el nombre del archivo es:
				// "+publicacion.getFotoFile4().getOriginalFilename());
				// if (publicacion.getFotoFile4().getOriginalFilename().isEmpty()==false) {
				// //publicacion.setFoto4(UPLOAD_LOCATION
				// +"/"+publicacion.getFotoFile4().getOriginalFilename());;
				// publicacion.setFoto4(publicacion.getFotoFile4().getOriginalFilename());;
				// FileCopyUtils.copy(publicacion.getFotoFile4().getBytes(), new
				// File(UPLOAD_LOCATION+"/"+publicacion.getFotoFile4().getOriginalFilename()));
				// System.out.println("entro aca al guardar foto supuestamente");
				// }
				if (publicacion.getFotoFile3().getOriginalFilename().isEmpty() == false) {
					publicacion.setFoto3(publicacion.getFotoFile3().getOriginalFilename());
					;
					FileCopyUtils.copy(publicacion.getFotoFile3().getBytes(),
							new File(UPLOAD_LOCATION + "/" + publicacion.getFotoFile3().getOriginalFilename()));
					System.out.println("entro aca al guardar foto supuestamente");
				}
				if (publicacion.getFotoFile2().getOriginalFilename().isEmpty() == false) {
					publicacion.setFoto2(publicacion.getFotoFile2().getOriginalFilename());
					;
					FileCopyUtils.copy(publicacion.getFotoFile2().getBytes(),
							new File(UPLOAD_LOCATION + "/" + publicacion.getFotoFile2().getOriginalFilename()));
					System.out.println("entro aca al guardar foto supuestamente");
				}
				if (publicacion.getFotoFile1().getOriginalFilename().isEmpty() == false) {
					publicacion.setFoto1(publicacion.getFotoFile1().getOriginalFilename());
					;
					FileCopyUtils.copy(publicacion.getFotoFile1().getBytes(),
							new File(UPLOAD_LOCATION + "/" + publicacion.getFotoFile1().getOriginalFilename()));
					System.out.println("entro aca al guardar foto supuestamente");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			publicacionService.savePublicacion(publicacion);
		} else { // Es una actualizacion de la publicacion
			MultipartFile multipartFile = publicacion.getFotoFile4();
			// Now do something with file...
			try {
				// System.out.println("el nombre del archivo es:
				// "+publicacion.getFotoFile4().getOriginalFilename());
				// if (publicacion.getFotoFile4().getOriginalFilename().isEmpty()==false) {
				// //publicacion.setFoto4(UPLOAD_LOCATION
				// +"/"+publicacion.getFotoFile4().getOriginalFilename());;
				// publicacion.setFoto4(publicacion.getFotoFile4().getOriginalFilename());;
				// FileCopyUtils.copy(publicacion.getFotoFile4().getBytes(), new
				// File(UPLOAD_LOCATION +"/"+publicacion.getFotoFile4().getOriginalFilename()));
				// System.out.println("entro aca al guardar foto supuestamente en el update");
				// }
				if (publicacion.getFotoFile3().getOriginalFilename().isEmpty() == false) {
					publicacion.setFoto3(publicacion.getFotoFile3().getOriginalFilename());
					;
					FileCopyUtils.copy(publicacion.getFotoFile3().getBytes(),
							new File(UPLOAD_LOCATION + "/" + publicacion.getFotoFile3().getOriginalFilename()));
					System.out.println("entro aca al guardar foto supuestamente");
				}
				if (publicacion.getFotoFile2().getOriginalFilename().isEmpty() == false) {
					publicacion.setFoto2(publicacion.getFotoFile2().getOriginalFilename());
					;
					FileCopyUtils.copy(publicacion.getFotoFile2().getBytes(),
							new File(UPLOAD_LOCATION + "/" + publicacion.getFotoFile2().getOriginalFilename()));
					System.out.println("entro aca al guardar foto supuestamente");
				}
				if (publicacion.getFotoFile1().getOriginalFilename().isEmpty() == false) {
					publicacion.setFoto1(publicacion.getFotoFile1().getOriginalFilename());
					;
					FileCopyUtils.copy(publicacion.getFotoFile1().getBytes(),
							new File(UPLOAD_LOCATION + "/" + publicacion.getFotoFile1().getOriginalFilename()));
					System.out.println("entro aca al guardar foto supuestamente");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			publicacionService.update(publicacion);
		}
		// return "obaju/transaccion-correcta";
		return "redirect:customer-wishlist";
	}

	

	
	@RequestMapping(value = { "/mensaje-urgente-esp-{idUser}-{idPublicacionUrgente}" }, method = RequestMethod.GET)
	public String mensajeUrgenteGet(@PathVariable int idUser, @PathVariable int idPublicacionUrgente, Model model) {
		User user = userService.getById(idUser);
		Cliente cliente = clienteService.getByUser(user);
		Especialista especialista = especialistaService
				.getByUsernameEspecialista(userService.getbyUsername(getPrincipal()));
		ServicioUrgente publicacionUrgente = servicioUrgenteService.getById(idPublicacionUrgente);
		Mensaje mensaje = new Mensaje();
		System.out.println("Entro en el enviar mensaje desde un esp a un cliente por un servicio urgente");
		System.out.println(especialista.getUser().getMail());
		System.out.println(cliente.getUser().getMail());
		mensaje.setCliente(cliente);
		mensaje.setEspecialista(especialista);
		mensaje.setAsunto("Urgencia: '" + publicacionUrgente.getDescripcion() + "'");
		mensaje.setRemitente('E');
		mensaje.setUrgencia(true);

		mensaje.setServicioUrgente(publicacionUrgente);

		model.addAttribute("mensaje", mensaje);
		crearHeader(model);
		return "obaju/mensaje";
	}

	@RequestMapping(value = "mensajes-recibidos-especialista", method = RequestMethod.GET)
	public String mensajesRecibidosEspecialista(Model model) {
		crearHeader(model);
		User user = userService.getbyUsername(getPrincipal());
		Especialista especialista = especialistaService.getByUsernameEspecialista(user);
		List<Mensaje> mensajes = mensajeService.findAll(especialista);
		ArrayList<Mensaje> list = new ArrayList<Mensaje>(mensajes);
		list.sort(new Comparator<Mensaje>() {

			@Override
			public int compare(Mensaje o1, Mensaje o2) {
				if (o1.getFechaHora().getTime() < o2.getFechaHora().getTime()) {
					return 1;
				} else if (o1.getFechaHora().getTime() > o2.getFechaHora().getTime()) {
					return -1;
				} else {
					return 0;
				}
			}
		});

		model.addAttribute("esCliente", false);
		model.addAttribute("mensajes", list);
		System.out.println("Los mensajes son " + list);
		for (Iterator iterator = mensajes.iterator(); iterator.hasNext();) {
			Mensaje mensaje = (Mensaje) iterator.next();
			if (mensaje != null) {
				System.out.println("Los mensajes son " + mensaje.getAsunto());
				System.out.println(mensaje.getServicioUrgente());
			}
		}
		return "obaju/mensajesChat";
	}

	// @RequestMapping(value="/chat-{idUrgencia}", method = RequestMethod.GET)
	// @ResponseBody
	// public List<Mensaje> chatIdUrgencia(@PathVariable int idUrgencia) {
	// User user = userService.getbyUsername(getPrincipal()) ;
	// Especialista especialista =
	// especialistaService.getByUsernameEspecialista(user);
	// ServicioUrgente servicioUrgente = servicioUrgenteService.getById(idUrgencia);
	// List<Mensaje> list =
	// mensajeService.findAllByServicioUrgente(servicioUrgente);
	// for (Iterator iterator = list.iterator(); iterator.hasNext();) {
	// Mensaje mensaje = (Mensaje) iterator.next();
	// System.out.println(mensaje.getCuerpo());
	// System.out.println(mensaje.getServicio());
	// }
	// //return "{\"msg\":\"success\"}";
	// return list;
	// }
	@RequestMapping(value = "/chat-{idUrgencia}", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView chatIdUrgencia(@PathVariable int idUrgencia, Model model) {
		User user = userService.getbyUsername(getPrincipal());
		Especialista especialista = especialistaService.getByUsernameEspecialista(user);
		ServicioUrgente servicioUrgente = servicioUrgenteService.getById(idUrgencia);
		List<Mensaje> list = mensajeService.findAllByServicioUrgente(servicioUrgente);
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Mensaje mensaje = (Mensaje) iterator.next();
			System.out.println(mensaje.getCuerpo());
			System.out.println(mensaje.getServicio());
			System.out.println(mensaje.getServicioUrgente());
		}
		// return "{\"msg\":\"success\"}";
		Set<Rol> roles = user.getRoles();
		Rol rolLogueado = null;
		for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
			Rol rol = (Rol) iterator.next();
			rolLogueado = rol;
			break;
		}

		Mensaje mensajeNuevo = new Mensaje();
		mensajeNuevo.setAsunto(list.get(0).getAsunto());
		mensajeNuevo.setCliente(list.get(0).getCliente());
		mensajeNuevo.setEspecialista(list.get(0).getEspecialista());
		if (rolLogueado.getType().equals("Cliente")) {
			mensajeNuevo.setRemitente('C');
		} else if (rolLogueado.getType().equals("Especialista")) {
			mensajeNuevo.setRemitente('E');
		}
		mensajeNuevo.setServicio(list.get(0).getServicio());
		mensajeNuevo.setServicioUrgente(list.get(0).getServicioUrgente());
		mensajeNuevo.setUrgencia(list.get(0).isUrgencia());
		model.addAttribute("rolLogueado", rolLogueado.getType());
		System.out.println("roles.toArray()[0]");
		model.addAttribute("mensajeNuevo", mensajeNuevo);
		model.addAttribute("listaMensajes", list);
		return new ModelAndView("obaju/chat");
	}

	@RequestMapping(value = "/chat-publicacion-{idPublicacion}", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView chatIdPublicacion(@PathVariable int idPublicacion, Model model) {
		User user = userService.getbyUsername(getPrincipal());
		Publicacion publicacion = publicacionService.getByKeyPublicaciones(idPublicacion);
		List<Mensaje> list = mensajeService.findAllByPublicacion(publicacion);
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Mensaje mensaje = (Mensaje) iterator.next();
			System.out.println("Entro en el chat-publicacion-" + idPublicacion);
			System.out.println(mensaje.getCuerpo());
			System.out.println(mensaje.getPublicacion());
			System.out.println(mensaje.getServicioUrgente());
		}
		// return "{\"msg\":\"success\"}";
		Set<Rol> roles = user.getRoles();
		Rol rolLogueado = null;
		for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
			Rol rol = (Rol) iterator.next();
			rolLogueado = rol;
			break;
		}

		Mensaje mensajeNuevo = new Mensaje();
		mensajeNuevo.setAsunto(list.get(0).getAsunto());
		mensajeNuevo.setCliente(list.get(0).getCliente());
		mensajeNuevo.setEspecialista(list.get(0).getEspecialista());
		if (rolLogueado.getType().equals("Cliente")) {
			mensajeNuevo.setRemitente('C');
		} else if (rolLogueado.getType().equals("Especialista")) {
			mensajeNuevo.setRemitente('E');
		}
		mensajeNuevo.setServicio(list.get(0).getServicio());
		// mensajeNuevo.setServicioUrgente(null);
		mensajeNuevo.setUrgencia(list.get(0).isUrgencia());
		mensajeNuevo.setPublicacion(list.get(0).getPublicacion());
		model.addAttribute("rolLogueado", rolLogueado.getType());
		System.out.println("roles.toArray()[0]");
		model.addAttribute("mensajeNuevo", mensajeNuevo);
		model.addAttribute("listaMensajes", list);
		return new ModelAndView("obaju/chat");
	}

	@RequestMapping(value = "/chat-refresh-{idUrgencia}", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView chatRefreshIdUrgencia(@PathVariable int idUrgencia, Model model) {
		User user = userService.getbyUsername(getPrincipal());
		Especialista especialista = especialistaService.getByUsernameEspecialista(user);
		ServicioUrgente servicioUrgente = servicioUrgenteService.getById(idUrgencia);
		List<Mensaje> list = mensajeService.findAllByServicioUrgente(servicioUrgente);
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Mensaje mensaje = (Mensaje) iterator.next();
			System.out.println(mensaje.getCuerpo());
			System.out.println(mensaje.getServicio());
			System.out.println(mensaje.getServicioUrgente());
		}
		// return "{\"msg\":\"success\"}";
		Set<Rol> roles = user.getRoles();
		Rol rolLogueado = null;
		for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
			Rol rol = (Rol) iterator.next();
			rolLogueado = rol;
			break;
		}

		Mensaje mensajeNuevo = new Mensaje();
		mensajeNuevo.setAsunto(list.get(0).getAsunto());
		mensajeNuevo.setCliente(list.get(0).getCliente());
		mensajeNuevo.setEspecialista(list.get(0).getEspecialista());
		if (rolLogueado.getType().equals("Cliente")) {
			mensajeNuevo.setRemitente('C');
		} else if (rolLogueado.getType().equals("Especialista")) {
			mensajeNuevo.setRemitente('E');
		}
		mensajeNuevo.setServicio(list.get(0).getServicio());
		mensajeNuevo.setServicioUrgente(list.get(0).getServicioUrgente());
		mensajeNuevo.setUrgencia(list.get(0).isUrgencia());
		model.addAttribute("rolLogueado", rolLogueado.getType());
		model.addAttribute("mensajeNuevo", mensajeNuevo);
		model.addAttribute("listaChatMensajes", list);
		return new ModelAndView("obaju/chatMensajes");
	}

	@RequestMapping(value = "/chat-refresh-publicacion-{idPublicacion}", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView chatRefreshIdPublicacion(@PathVariable int idPublicacion, Model model) {
		User user = userService.getbyUsername(getPrincipal());
		Especialista especialista = especialistaService.getByUsernameEspecialista(user);
		Publicacion publicacion = publicacionService.getByKeyPublicaciones(idPublicacion);
		List<Mensaje> list = mensajeService.findAllByPublicacion(publicacion);
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Mensaje mensaje = (Mensaje) iterator.next();
			System.out.println(mensaje.getCuerpo());
			System.out.println(mensaje.getPublicacion());
			System.out.println(mensaje.getServicioUrgente());
		}
		// return "{\"msg\":\"success\"}";
		Set<Rol> roles = user.getRoles();
		Rol rolLogueado = null;
		for (Iterator iterator = roles.iterator(); iterator.hasNext();) {
			Rol rol = (Rol) iterator.next();
			rolLogueado = rol;
			break;
		}

		Mensaje mensajeNuevo = new Mensaje();
		mensajeNuevo.setAsunto(list.get(0).getAsunto());
		mensajeNuevo.setCliente(list.get(0).getCliente());
		mensajeNuevo.setEspecialista(list.get(0).getEspecialista());
		if (rolLogueado.getType().equals("Cliente")) {
			mensajeNuevo.setRemitente('C');
		} else if (rolLogueado.getType().equals("Especialista")) {
			mensajeNuevo.setRemitente('E');
		}
		mensajeNuevo.setServicio(list.get(0).getServicio());
		mensajeNuevo.setServicioUrgente(list.get(0).getServicioUrgente());
		mensajeNuevo.setUrgencia(list.get(0).isUrgencia());
		mensajeNuevo.setPublicacion(publicacion);
		model.addAttribute("rolLogueado", rolLogueado.getType());
		model.addAttribute("mensajeNuevo", mensajeNuevo);
		model.addAttribute("listaChatMensajes", list);
		return new ModelAndView("obaju/chatMensajes");
	}

	@RequestMapping(value = "/enviar-mensaje", method = RequestMethod.POST)
	@ResponseBody
	// public String enviarMensaje(@RequestBody Mensaje mensaje) {
	public String enviarMensaje(@ModelAttribute("mensajeNuevo") Mensaje mensaje) {

		System.out.println("entro al ajax del comentar servicio");
		System.out.println(mensaje.getId());
		System.out.println(mensaje.getCuerpo());
		System.out.println(mensaje.getAsunto());
		System.out.println(mensaje.getServicioUrgente());
		System.out.println(mensaje.getPublicacion());
		Timestamp fechaHora = new Timestamp(System.currentTimeMillis());
		mensaje.setFechaHora(fechaHora);
		mensaje.setServicio(null);
		if (mensaje.getPublicacion().getId() == 0) {
			mensaje.setPublicacion(null);
		} else if (mensaje.getServicioUrgente().getId() == 0) {
			mensaje.setServicioUrgente(null);
		}
		mensajeService.save(mensaje);

		return "{\"msg\":\"success\"}";
	}

	@RequestMapping(value = "/trabajos-realizados", method = RequestMethod.GET)
	public String getTrabajosDeEspecialista(Model model) {
		crearHeader(model);
		String usernameEspecialista = getPrincipal();
		User user = userService.getbyUsername(usernameEspecialista);
		Especialista especialista = especialistaService.getByUsernameEspecialista(user);
		List<Servicio> listaTrabajosRealizados = servicioService.getServiciosByEspecialista(especialista);
		Cliente cliente = clienteService.getByUser(user);
		model.addAttribute("especialista", especialista);
		model.addAttribute("cliente", cliente);
		model.addAttribute("listTrabajosRealizados", listaTrabajosRealizados);

		// model.addAttribute("especialista", especialista);
		// model.addAttribute("publicaciones", listPublicacionDeEspecialista);
		return "obaju/trabajos-realizados";
	}

	public void crearHeader(Model model) {
		model.addAttribute("userLogged", getPrincipal());
		model.addAttribute("categoriasPadre", categoriaService.findAllCategoriasPadres());
	}

	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			// userName = principal.toString();
			userName = "Login";
		}
		return userName;
	}

	@ModelAttribute("contNotificacionesUrgentes")
	public int getcantidadNotificacionesUrgentes() {
		User user = userService.getbyUsername(getPrincipal());
		List<ServicioUrgente> listaUrgentes = servicioUrgenteService.findAllByUser(user);
		int contNotificaciones = 0;
		for (Iterator iterator = listaUrgentes.iterator(); iterator.hasNext();) {
			ServicioUrgente servicioUrgente = (ServicioUrgente) iterator.next();
			if (servicioUrgente.isNotificacionLeida() == false & servicioUrgente.getEspecialista() != null) {
				contNotificaciones++;
			}
		}
		return contNotificaciones;
	}

	@ModelAttribute("contNotificacionesMisServiciosUrgentes")
	public int getcantidadNotificacionesMisServiciosUrgentes() {
		if (!getPrincipal().equals("Login")) {
			User user = userService.getbyUsername(getPrincipal());
			ArrayList<Rol> userArray = new ArrayList<Rol>(user.getRoles());
			if (userArray.get(0).getType().equals("Especialista")) {
				Cliente cliente = clienteService.getByUser(user);
				Especialista especialista = especialistaService.getByUsernameEspecialista(user);
				ArrayList<ServicioUrgente> listUrgentes = (ArrayList<ServicioUrgente>) servicioUrgenteService
						.getServiciosUrgentesMenoresDistancia(cliente.getLatitud(), cliente.getLongitud(),
								especialista.getId());
				List<Publicacion> listPublicacionesEsp = publicacionService
						.findAllPublicacionesActivasByEspecialista(especialista);
				ArrayList<ServicioUrgente> listUrgentesFiltrado = new ArrayList<ServicioUrgente>();
				for (ServicioUrgente servicioUrgente : listUrgentes) {
					boolean band = false;
					for (Iterator iterator = listPublicacionesEsp.iterator(); iterator.hasNext();) {
						Publicacion publicacion = (Publicacion) iterator.next();
						if (publicacion.getCategoria().getId() == servicioUrgente.getCategoria().getId()
								&& servicioUrgente.getEspecialista() == null) {
							band = true;
						}
					}
					if (band == true) {
						listUrgentesFiltrado.add(servicioUrgente);
					}
				}
				return listUrgentesFiltrado.size();
			}

		}
		return 0;
	}

}
