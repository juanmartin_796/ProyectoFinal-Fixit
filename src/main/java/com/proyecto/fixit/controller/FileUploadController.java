package com.proyecto.fixit.controller;

import java.io.File;
import java.io.IOException;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.proyecto.fixit.model.FileBucket;

@Controller
public class FileUploadController {
private static String UPLOAD_LOCATION="/home/juanmartin/imagenesUploadFixit/";
	
	@RequestMapping(value="singleupload", method = RequestMethod.GET)
    public String getSingleUploadPage(ModelMap model) {
        FileBucket fileModel = new FileBucket();
        fileModel.setNombre("holanda");
        
        model.addAttribute("fileBucket", fileModel);
        return "singleFileUploader";
    }
	
	@RequestMapping(value="singleupload", method = RequestMethod.POST)
    public String singleFileUpload(@Valid FileBucket fileBucket, BindingResult result, ModelMap model) throws IOException {
		System.out.println("Entro al post del subir archivo");
        if (result.hasErrors()) {
            System.out.println("validation errors");
            return "singleFileUploader";
        } else {            
            System.out.println("Fetching file");
            MultipartFile multipartFile = fileBucket.getFile();
 
            //Now do something with file...
            FileCopyUtils.copy(fileBucket.getFile().getBytes(), new File(UPLOAD_LOCATION +fileBucket.getFile().getOriginalFilename()));
             
            String fileName = multipartFile.getOriginalFilename();
            model.addAttribute("fileName", fileName);
            return "success";
        }
    }
	
}
