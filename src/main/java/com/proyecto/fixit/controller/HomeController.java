package com.proyecto.fixit.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.proyecto.fixit.dao.MensajeDao;
import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.Denuncia;
import com.proyecto.fixit.model.Direccion;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Mensaje;
import com.proyecto.fixit.model.Provincia;
import com.proyecto.fixit.model.Publicacion;
import com.proyecto.fixit.model.Rol;
import com.proyecto.fixit.model.Servicio;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;
import com.proyecto.fixit.service.CategoriaService;
import com.proyecto.fixit.service.CiudadService;
import com.proyecto.fixit.service.ClienteService;
import com.proyecto.fixit.service.DenunciaService;
import com.proyecto.fixit.service.EspecialistaService;
import com.proyecto.fixit.service.MailService;
import com.proyecto.fixit.service.MensajeService;
import com.proyecto.fixit.service.ProvinciaService;
import com.proyecto.fixit.service.PublicacionService;
import com.proyecto.fixit.service.RolService;
import com.proyecto.fixit.service.ServicioUrgenteService;
import com.proyecto.fixit.service.UserService;
import com.proyecto.fixit.service.servicioService;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes("roles")
public class HomeController {
	@Autowired
	CategoriaService categoriaService;

	@Autowired
	UserService userService;

	@Autowired
	RolService rolService;

	@Autowired
	PublicacionService publicacionService;

	@Autowired
	EspecialistaService especialistaService;

	@Autowired
	MailService mailService;

	@Autowired
	ClienteService clienteService;

	@Autowired
	servicioService servicioService;

	@Autowired
	ProvinciaService provinciaService;

	@Autowired
	MensajeService mensajeService;

	@Autowired
	DenunciaService denunciaService;

	@Autowired
	ServicioUrgenteService serviciosUrgentesService;

	@Autowired
	CiudadService ciudadService;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String home(Model model) {
		List<Categoria> categorias = categoriaService.findAllCategorias();
		List<Publicacion> ultimasPublicaciones = publicacionService.findAll8ultimasActivas();
		model.addAttribute("categorias", categorias);
		model.addAttribute("ultimasPublicaciones", ultimasPublicaciones);
		crearHeader(model);
		return "obaju/index";
	}


//	@RequestMapping(value = "register", method = RequestMethod.GET)
	@RequestMapping(value = "crearcategoria", method = RequestMethod.GET)
//	public String newUser(Model model) {
	public String newCategoria(Model model) {
		//User user = new User();
		Categoria categoria = new Categoria();
		model.addAttribute("categorias", categoriaService.findAllCategorias());
//		model.addAttribute("user", user);
		model.addAttribute("categoria", categoria);
		//model.addAttribute("roles", initializeProfiles());
		crearHeader(model);
//		return "obaju/register";
		return "obaju/CrearCategoria";
	}


	@RequestMapping(value = "register", method = RequestMethod.GET)
	public String newUser(Model model) {
		User user = new User();
		model.addAttribute("categorias", categoriaService.findAllCategorias());
		model.addAttribute("user", user);
		//model.addAttribute("roles", initializeProfiles());
		crearHeader(model);
		return "obaju/register";
	}
	
	
//	@RequestMapping(value = "register", method = RequestMethod.POST)
	@RequestMapping(value = "crearcategoria", method = RequestMethod.POST)
//	public String saveUser(@Valid User user, BindingResult result, ModelMap model) {
	public String saveUser(@Valid Categoria categoria, BindingResult result, ModelMap model) {
//		if (result.hasErrors()) {
//			//System.out.println("los errores de rol son "+ user.getRoles());
//			if (user.getRoles().size()==0) {
//				FieldError rolError = new FieldError("user", "roles",
//						"Debe seleccionar una de las opciones");
//				result.addError(rolError);
//				
//			}
//			System.out.println(result.toString());
//			return "obaju/register";
//		}
//		user.setEnabled(true);
//		if (userService.getbyUsername(user.getUsername()) != null) {
		if (categoriaService.getByNombre(categoria.getNombre()) != null) {
//			FieldError ssnError = new FieldError("user", "username", "Ya existe el nombre. Elija otro nombre de usuario por favor");
			FieldError ssnError = new FieldError("categoria", "nombre", "Ya existe el nombre. Elija otro nombre de categoria por favor");
			result.addError(ssnError);
			return "obaju/CrearCategoria";
		}

//		userService.saveUser(user);		
        categoriaService.saveCategoria(categoria);
        
		//System.out.println("entro bien!!!!!!!!!!!!!");
		// mailService.sendEmail("juanmartin_796@hotmail.com", "Registro de usuario en
		// Fixit!", "Usted se ha registrado correctamente. Aca click en el siguiete
		// enlace para activar la cuenta");

//		Cliente cliente = new Cliente();
//		cliente.setUser(userService.getbyUsername(user.getUsername()));
//		cliente.setApellido("desc");
//		cliente.setNombre("desc");
//		Direccion direccion = new Direccion();
//		direccion.setCalle("desc");
//		direccion.setNumero(9999);
//		Ciudad ciudad = new Ciudad();
//		ciudad.setCodPostal(3500);
//		direccion.setCiudad(ciudad);
//		cliente.setDireccion(direccion);
//		cliente.setEmail(user.getMail());
//		System.out.println("la direccion de correo electronico es :" + user.getMail());
//		clienteService.saveCliente(cliente);

//		Set<Rol> list = user.getRoles();
//		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
//			Rol rol = (Rol) iterator.next();
//			if (rol.getType().equals("Especialista")) {
//				Especialista esp = new Especialista();
//				esp.setApellido("desc");
//				esp.setNombre("desc");
//				esp.setDni("88888888");
//				esp.setUser(userService.getbyUsername(user.getUsername()));
//				Direccion dir = new Direccion();
//				dir.setCalle("desc");
//				dir.setNumero(9999);
//				Ciudad ciu = new Ciudad();
//				ciu.setCodPostal(3500);
//				dir.setCiudad(ciu);
//				esp.setDireccion(dir);
//				esp.setEmail(user.getMail());
//				especialistaService.save(esp);
//			}
//		}

		return "redirect:/index";
	}	
	
	
	
	@RequestMapping(value = "register", method = RequestMethod.POST)
	public String saveUser(@Valid User user, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			System.out.println("los errores de rol son "+ user.getRoles());
			if (user.getRoles().size()==0) {
				FieldError rolError = new FieldError("user", "roles",
						"Debe seleccionar una de las opciones");
				result.addError(rolError);
				
			}
			System.out.println(result.toString());
			return "obaju/register";
		}
		user.setEnabled(true);
		if (userService.getbyUsername(user.getUsername()) != null) {
			FieldError ssnError = new FieldError("user", "username",
					"Ya existe el usuario. Elija otro nombre de usuario por favor");
			result.addError(ssnError);
			return "obaju/register";
		}
		userService.saveUser(user);
		System.out.println("entro bien!!!!!!!!!!!!!");
		// mailService.sendEmail("juanmartin_796@hotmail.com", "Registro de usuario en
		// Fixit!", "Usted se ha registrado correctamente. Aca click en el siguiete
		// enlace para activar la cuenta");

		Cliente cliente = new Cliente();
		cliente.setUser(userService.getbyUsername(user.getUsername()));
		cliente.setApellido("desc");
		cliente.setNombre("desc");
		Direccion direccion = new Direccion();
		direccion.setCalle("desc");
		direccion.setNumero(9999);
		Ciudad ciudad = new Ciudad();
		ciudad.setCodPostal(3500);
		direccion.setCiudad(ciudad);
		cliente.setDireccion(direccion);
		cliente.setEmail(user.getMail());
		System.out.println("la direccion de correo electronico es :" + user.getMail());
		clienteService.saveCliente(cliente);

		Set<Rol> list = user.getRoles();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Rol rol = (Rol) iterator.next();
			if (rol.getType().equals("Especialista")) {
				Especialista esp = new Especialista();
				esp.setApellido("desc");
				esp.setNombre("desc");
				esp.setDni("88888888");
				esp.setUser(userService.getbyUsername(user.getUsername()));
				Direccion dir = new Direccion();
				dir.setCalle("desc");
				dir.setNumero(9999);
				Ciudad ciu = new Ciudad();
				ciu.setCodPostal(3500);
				dir.setCiudad(ciu);
				esp.setDireccion(dir);
				esp.setEmail(user.getMail());
				especialistaService.save(esp);
			}
		}

		return "redirect:/servicioscontratados";
	}

	@ModelAttribute("roles")
	public List<Rol> initializeProfiles() {
		System.out.println("Entro en los roles -------------------------------------------------");
		List<Rol> listDevolver = new ArrayList<Rol>();
		List<Rol> list = rolService.findAll();
		for (Rol rol : list) {
			System.out.println("El tipo del rol es: " + rol.getType());
			if (rol.getType().equals("Cliente") || rol.getType().equals("Especialista")) {
				listDevolver.add(rol);
			}
		}
		return listDevolver;
	}

	@RequestMapping(value = "login")
	public String login(HttpServletRequest request, @RequestParam(value = "error", required = false) String error,
			Model model) {
		// model.addAttribute("userLogged", getPrincipal());
		// model.addAttribute("categoriasPadre",
		// categoriaService.findAllCategoriasPadres());
		if (request.getUserPrincipal() == null) {
			if (error != null) {
				model.addAttribute("errorCampo", "Nombre de usuario o contrase�a invalidos");
			}
		} else {
			return "redirect:index";
		}
		crearHeader(model);
		// return "redirect:/login";
		return "obaju/login";
	}

	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			// userName = principal.toString();
			userName = "Login";
		}
		return userName;
	}

	@RequestMapping(value = { "/category-{idCategoria}-{desde}-{hasta}" }, method = RequestMethod.GET)
	public String getSubcategoria(@PathVariable int idCategoria, @PathVariable int desde, @PathVariable int hasta,
			Model model) {
		// model.addAttribute("subcategoriaSeleccionada",
		// categoriaService.getByKeyCategoria(idCategoria));
		// model.addAttribute("categoriasPadres",
		// categoriaService.findAllCategoriasPadres());
		crearLateralEncabezado(model, idCategoria);
		List<Publicacion> listaPublicaciones = publicacionService.findAllPublicacionesByCategoriaKey(idCategoria);
		int cantPublicaciones = listaPublicaciones.size();
		int cantPaginas = (int) Math.ceil(cantPublicaciones / 12.00);

		Categoria category = categoriaService.getByKeyCategoria(idCategoria);
		List<Publicacion> listaPublicacionesDesdeHasta = publicacionService.findAllByCategory(category, desde, hasta);

		model.addAttribute("cantPaginas", cantPaginas);
		model.addAttribute("cantPublicaciones", cantPublicaciones);
		model.addAttribute("publicaciones", listaPublicacionesDesdeHasta);
		model.addAttribute("categoria", category.getId());
		crearHeader(model);
		return "obaju/category";
	}

	@RequestMapping(value = { "categoria-{idCategoria}-{desde}-{hasta}-{idCiudad}" }, method = RequestMethod.GET)
	public String getPublicacionesByCategoriaByCiudad(@PathVariable int idCategoria, @PathVariable int desde,
			@PathVariable int hasta, @PathVariable int idCiudad, Model model) {
		// model.addAttribute("subcategoriaSeleccionada",
		// categoriaService.getByKeyCategoria(idCategoria));
		// model.addAttribute("categoriasPadres",
		// categoriaService.findAllCategoriasPadres());
		crearLateralEncabezado(model, idCategoria);

		Categoria category = categoriaService.getByKeyCategoria(idCategoria);
		Ciudad ciudad = ciudadService.getByKey(idCiudad);
		List<Publicacion> listaPublicacionesDesdeHasta = publicacionService.findAllByCategoryByCiudad(category, desde,
				hasta, ciudad);
		List<Publicacion> listPublicacionesTotal = publicacionService.findAllByCategoryByCiudad(category, ciudad);
		int cantPublicaciones = listPublicacionesTotal.size();
		int cantPaginas = (int) Math.ceil(cantPublicaciones / 12.00);

		model.addAttribute("cantPaginas", cantPaginas);
		model.addAttribute("cantPublicaciones", cantPublicaciones);
		model.addAttribute("publicaciones", listaPublicacionesDesdeHasta);
		model.addAttribute("categoria", category.getId());
		crearHeader(model);
		return "obaju/category";
	}

	@RequestMapping(value = { "/detail-publicacion-{idPublicacion}" }, method = RequestMethod.GET)
	public String getDetailPublicacion(@PathVariable int idPublicacion, Model model) {
		Publicacion publicacion = publicacionService.getByKeyPublicaciones(idPublicacion);
		if (publicacion == null) {
			System.out.println("error, no existe la publicacion");
		} else {
			model.addAttribute("publicacion", publicacion);
			crearHeader(model);
			crearLateralEncabezado(model, publicacion.getCategoria().getId());
			Double califProm = null;
			califProm = publicacionService.getPromCalifPub(idPublicacion);
			System.out.println("La calificacion promedio de la publicacion es : " + califProm);
			model.addAttribute("califPromedio", califProm);
			return "obaju/detail";
		}
		return null;

	}

	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public String getBusqueda(@RequestParam("busqueda") String busqueda, Model model) {
		System.out.println("la busqueda es: " + busqueda);
		List<Publicacion> listPublicaciones;
		listPublicaciones = publicacionService.getByCriterioBusqueda(busqueda);
		crearHeader(model);
		crearLateralEncabezado(model, 0);
		model.addAttribute("busqueda", busqueda);
		model.addAttribute("publicaciones", listPublicaciones);
		return "obaju/category";
	}

	@RequestMapping(value = "servicioscontratados", method = RequestMethod.GET)
	public String misServiciosUrgentes(Model model) {
		crearHeader(model);
		activarFuncionesEspecialistas(model);
		Set<Servicio> listServiciosContratados = new HashSet<Servicio>();
		List<Servicio> list = null;
		try { // Si no tiene ningun servicio contratado, captura el error
			listServiciosContratados = clienteService.getByUser(userService.getbyUsername(getPrincipal()))
					.getServiciosContratados();
			list = new ArrayList<Servicio>(listServiciosContratados);
			list.sort(new Comparator<Servicio>() {

				@Override
				public int compare(Servicio o1, Servicio o2) {
					if (o1.getFechaHora().getTime() < o2.getFechaHora().getTime()) {
						System.out.println("entro por el menor-----------------------------------------------");
						return 1;
					} else if (o1.getFechaHora().getTime() > o2.getFechaHora().getTime()) {
						System.out.println("entro por el mayor --------------------------------------------------");
						return -1;
					} else {
						return 0;
					}
				}
			});
			model.addAttribute("listServiciosContratados", list);
		} catch (Exception e) {
			Servicio servicio = null;
			list.add(servicio);
		}
		// model.addAttribute("listServiciosContratados", listServiciosContratados);

		return "obaju/servicios-contratados";

	}

	@RequestMapping(value = "/calificar-servicio")
	@ResponseBody
	public String calificacion(@RequestParam("calificacion") String calificacion) {
		System.out.println("Se hizo la peticion ajax");
		return "Resp Ajax: " + calificacion;
	}

	@RequestMapping(value = "/calificar-servicio2", method = RequestMethod.POST)
	@ResponseBody
	public String calificacion2(@RequestBody Servicio servicio) {
		Servicio serv = servicioService.findByKey(servicio.getId());
		servicio.setFechaHora(serv.getFechaHora());
		servicio.setPrecio(serv.getPrecio());
		servicio.setPublicacion(serv.getPublicacion());
		servicio.setCliente(serv.getCliente());

		servicioService.updateServicio(servicio);

		System.out.println("servicio id: " + serv.getId());
		System.out.println("servicio calif: " + serv.getCalificacion());
		System.out.println("servicio precio: " + serv.getPrecio());
		System.out.println("servicio fecha: " + serv.getFechaHora());
		System.out.println("servicio fecha: " + serv.getCliente().getUser());
		return "{\"msg\":\"success\"}";
	}

	@RequestMapping(value = "/comentar-servicio", method = RequestMethod.POST)
	@ResponseBody
	public String comentarServicio(@RequestBody Servicio servicio) {
		System.out.println("entro al ajax del comentar servicio");
		System.out.println(servicio.getId());
		System.out.println(servicio.getComentario());
		Servicio serv = servicioService.findByKey(servicio.getId());
		servicio.setFechaHora(serv.getFechaHora());
		servicio.setPrecio(serv.getPrecio());
		servicio.setPublicacion(serv.getPublicacion());
		servicio.setCliente(serv.getCliente());
		servicio.setCalificacion(serv.getCalificacion());

		servicioService.updateServicio(servicio);

		System.out.println("servicio id: " + serv.getId());
		System.out.println("servicio calif: " + serv.getCalificacion());
		System.out.println("servicio precio: " + serv.getPrecio());
		System.out.println("servicio fecha: " + serv.getFechaHora());
		System.out.println("servicio fecha: " + serv.getCliente().getUser());
		return "{\"msg\":\"success\"}";
	}

	@RequestMapping(value = "/trabajos-realizados-{idEspecialista}", method = RequestMethod.GET)
	public String getTrabajosDeEspecialista(@PathVariable int idEspecialista, Model model) {
		crearHeader(model);
		String usernameEspecialista = getPrincipal();

		// User user= userService.getbyUsername(usernameEspecialista);
		Especialista especialista = especialistaService.getByKeyEspecialista(idEspecialista);
		// List<Publicacion> listPublicacionDeEspecialista =
		// publicacionService.findAllPublicacionesActivasByEspecialista(especialista);
		// System.out.println(listPublicacionDeEspecialista.get(0).getId());
		// System.out.println(listPublicacionDeEspecialista.get(0).getCuerpo());
		// System.out.println(especialista.getNombre());
		List<Servicio> listaTrabajosRealizados = servicioService.getServiciosByEspecialista(especialista);
		ArrayList<Servicio> list = new ArrayList<Servicio>(listaTrabajosRealizados);
		list.sort(new Comparator<Servicio>() {

			@Override
			public int compare(Servicio o1, Servicio o2) {
				if (o1.getFechaHora().getTime() < o2.getFechaHora().getTime()) {
					return 1;
				} else if (o1.getFechaHora().getTime() > o2.getFechaHora().getTime()) {
					return -1;
				} else {
					return 0;
				}
			}
		});

		User user = especialista.getUser();
		Cliente cliente = clienteService.getByUser(user);
		model.addAttribute("especialista", especialista);
		model.addAttribute("cliente", cliente);
		// model.addAttribute("listTrabajosRealizados", listaTrabajosRealizados);
		model.addAttribute("listTrabajosRealizados", list);

		// model.addAttribute("especialista", especialista);
		// model.addAttribute("publicaciones", listPublicacionDeEspecialista);
		return "obaju/trabajos-realizados";
	}

	@RequestMapping(value = { "/perfil" }, method = RequestMethod.GET)
	public String perfilUsuario(Model model) {
		User usuarioLogueado = userService.getbyUsername(getPrincipal());
		Cliente cliente = clienteService.getByUser(usuarioLogueado);
		model.addAttribute("cliente", cliente);
		model.addAttribute("usuarioLogueado", usuarioLogueado);
		crearHeader(model);

		System.out.println("el perfil es: " + getPrincipal());
		model.addAttribute("mostrarContrasenaIncorrecta", "style=\"display: none;\"");
		List<Provincia> listProvincia = provinciaService.findAllProvincias();
		List<Ciudad> listCiudades = ciudadService.findAll(cliente.getDireccion().getCiudad().getProvincia());
		model.addAttribute("itemsProvincias", listProvincia);
		model.addAttribute("itemsCiudades", listCiudades);
		System.out.println(listProvincia.get(1).getNombre());

		return "obaju/customer-account";
	}

	@RequestMapping(value = { "/perfil" }, method = RequestMethod.POST)
	public String ActualizarContrasena(@RequestParam("oldPassword") String oldPassword,
			@RequestParam("newPassword") String newPassword,
			@RequestParam("newPasswordRepeticion") String newPasswordRepeticion, @Valid User usuarioLogueado,
			BindingResult result, Model model) {
		if (result.hasErrors()) {
			System.out.println(result.toString());
		}
		User usuario = userService.getbyUsername(getPrincipal());
		boolean coincidenciaContrasena = coincidenciaContrasena(newPassword, newPasswordRepeticion);
		if (usuario.getPassword().equals(oldPassword)) {
			if (coincidenciaContrasena == true) {
				usuario.setPassword(newPassword);
				userService.saveUser(usuario);
			}
		} else {
			model.addAttribute("mostrarContrasenaIncorrecta", "style=\"display: visible;\"");
			return "obaju/customer-account";
		}
		System.out.println("entro a el actualizar contraseña");
		return "redirect:/perfil";
	}

	private boolean coincidenciaContrasena(String newPassword, String newPasswordRepeticion) {
		if (newPassword.equals(newPasswordRepeticion)) {
			return true;
		}
		return false;
	}

	@RequestMapping(value = { "/actualizadoPerfil" }, method = RequestMethod.POST)
	public String ActualizarPerfil(@Valid Cliente cliente, BindingResult result, Model model) {
		crearHeader(model);
		if (result.hasErrors()) {
			System.out.println(result.toString());
			// result.rejectValue("direccion.calle", "direccion.calle", "Please Fill in a
			// value");
			model.addAttribute("mostrarContrasenaIncorrecta", "style=\"display: none;\"");
			System.out.println("El departamento es " + cliente.getDireccion().getDepartamento());
			List<Provincia> listProvincia = provinciaService.findAllProvincias();
			List<Ciudad> listCiudades = ciudadService.findAll(cliente.getDireccion().getCiudad().getProvincia());
			model.addAttribute("itemsProvincias", listProvincia);
			model.addAttribute("itemsCiudades", listCiudades);
			return "obaju/customer-account";
		} else {
			System.out.println(cliente.getDireccion().getCiudad().getCodPostal());
			System.out.println(cliente.getDireccion().getCiudad());
			System.out.println(cliente.getNombre());
			System.out.println(cliente.getId());
			System.out.println(cliente.getUser().getUsername());

			User user = cliente.getUser();
			Especialista especialista = especialistaService.getByUsernameEspecialista(user);
			if (especialista != null) {
				especialista.setDireccion(cliente.getDireccion());
				especialistaService.update(especialista);
			}
			clienteService.updateCliente(cliente);
			return "redirect:/perfil";
			// return "obaju/transaccion-correcta";
		}

	}

	@RequestMapping(value = "/ciudades-{idProvincia}", method = RequestMethod.GET)
	@ResponseBody
	public List<Ciudad> getCiudades(@PathVariable int idProvincia, Model model) {
		Provincia provincia = provinciaService.getById(idProvincia);
		List<Ciudad> listCiudades = ciudadService.findAll(provincia);
		return listCiudades;
	}

	@RequestMapping(value = { "/mensaje-{idEspecialista}-{idPublicacion}" }, method = RequestMethod.GET)
	public String mensajeGet(@PathVariable int idEspecialista, @PathVariable int idPublicacion, Model model) {
		User user = userService.getbyUsername(getPrincipal());
		Cliente cliente = clienteService.getByUser(user);
		Especialista especialista = especialistaService.getByKeyEspecialista(idEspecialista);
		Publicacion publicacion = publicacionService.getByKeyPublicaciones(idPublicacion);
		Mensaje mensaje = new Mensaje();
		System.out.println("empieza el testeo previo engau");
		System.out.println(especialista.getUser().getMail());
		System.out.println(cliente.getUser().getMail());
		mensaje.setCliente(cliente);
		mensaje.setEspecialista(especialista);
		mensaje.setAsunto(publicacion.getTitulo());
		mensaje.setPublicacion(publicacion);
		mensaje.setRemitente('C');

		model.addAttribute("mensaje", mensaje);
		crearHeader(model);
		return "obaju/mensaje";
	}

	@RequestMapping(value = { "/mensaje" }, method = RequestMethod.POST)
	public String mensajePost(@Valid Mensaje mensaje, BindingResult result, Model model) {
		if (result.hasErrors()) {
			System.out.println(result.getAllErrors());
		}
		Timestamp fechaHora = new Timestamp(System.currentTimeMillis());
		mensaje.setFechaHora(fechaHora);
		if (mensaje.getServicio().getId() == 0) {
			mensaje.setServicio(null);
			System.out.println("Entro en el servicio null del mensaje");
		}
		if (mensaje.getServicioUrgente().getId() == 0) {
			mensaje.setServicioUrgente(null);
			System.out.println("Entro en el servicio null del mensaje");
		}
		if (mensaje.getPublicacion().getId() == 0) {
			mensaje.setPublicacion(null);
		}

		mensajeService.save(mensaje);
		if (mensaje.getRemitente() == 'C') {
			if (mensaje.isUrgencia() == true) {
//				mailService.sendEmail(mensaje.getEspecialista().getUser().getMail(),
//						"¡Urgente! Un cliente te ha enviado un mensaje sobre una urgencia que aceptaste",
//						mensaje.getAsunto() + "\"\n" + mensaje.getCuerpo());
			} else {
//				mailService.sendEmail(mensaje.getEspecialista().getUser().getMail(),
//						"Un cliente te ha enviado un mensaje sobre una publicacion",
//						"Sobre \"" + mensaje.getAsunto() + "\"\n" + mensaje.getCuerpo());
			}
		} else {
			// mailService.sendEmail(mensaje.getCliente().getUser().getMail(), "Un
			// especialista te ha enviado un mensaje", "Sobre
			// \""+mensaje.getAsunto()+"\"\n"+mensaje.getCuerpo());
		}
		return "obaju/sucess";
	}

	@RequestMapping(value = { "/denunciar-{idPublicacion}" }, method = RequestMethod.GET)
	public String denunciarGet(@PathVariable int idPublicacion, Model model) {
		crearHeader(model);
		User user = userService.getbyUsername(getPrincipal());
		Cliente cliente = clienteService.getByUser(user);
		Publicacion publicacion = publicacionService.getByKeyPublicaciones(idPublicacion);
		Denuncia denuncia = new Denuncia();
		denuncia.setCliente(cliente);
		denuncia.setPublicacion(publicacion);
		model.addAttribute("denuncia", denuncia);
		return "obaju/denuncia";
	}

	@RequestMapping(value = { "/denuncia" }, method = RequestMethod.POST)
	public String denunciarPost(Denuncia denuncia, Model model) {
		Timestamp fechaHora = new Timestamp(System.currentTimeMillis());
		denuncia.setFechaHora(fechaHora);
		denunciaService.save(denuncia);
		return "obaju/sucess";
	}

	// @RequestMapping(value="/getNotificaciones")
	// @ResponseBody
	// public String getNotificaciones() {
	// System.out.println("Se hizo la peticion ajax para obtener las
	// notificaciones");
	// return "Resp Ajax: "+ 2;
	// }

	@RequestMapping(value = { "/serviciosurgentespedidos" }, method = RequestMethod.GET)
	public String serviciosUrgentesPedidosGet(Model model) {
		crearHeader(model);
		User user = userService.getbyUsername(getPrincipal());
		List<ServicioUrgente> listaUrgentes = serviciosUrgentesService.findAllByUser(user);

		model.addAttribute("listaUrgentes", listaUrgentes);
		List<ServicioUrgente> listaUrgentesActualizar = serviciosUrgentesService.findAllByUser(user);
		for (Iterator iterator = listaUrgentesActualizar.iterator(); iterator.hasNext();) {
			ServicioUrgente servicioUrgente = (ServicioUrgente) iterator.next();
			if (servicioUrgente.isNotificacionLeida() == false & servicioUrgente.getEspecialista() != null) {
				servicioUrgente.setNotificacionLeida(true);
				try {
					serviciosUrgentesService.update(servicioUrgente);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return "obaju/serviciosurgentespedidos";
	}

	@RequestMapping(value = { "/eliminar-urgente-pedido-{idUrgente}" }, method = RequestMethod.GET)
	public String eliminarUrgentePedidoGet(@PathVariable int idUrgente, Model model) {
		ServicioUrgente servUrg = serviciosUrgentesService.getById(idUrgente);
		serviciosUrgentesService.delete(servUrg);
		return "redirect:/serviciosurgentespedidos";
	}

	@RequestMapping(value = { "/mensaje-urgente-{idEspecialista}-{idPublicacionUrgente}" }, method = RequestMethod.GET)
	public String mensajeUrgenteGet(@PathVariable int idEspecialista, @PathVariable int idPublicacionUrgente,
			Model model) {
		User user = userService.getbyUsername(getPrincipal());
		Cliente cliente = clienteService.getByUser(user);
		Especialista especialista = especialistaService.getByKeyEspecialista(idEspecialista);
		ServicioUrgente publicacionUrgente = serviciosUrgentesService.getById(idPublicacionUrgente);
		Mensaje mensaje = new Mensaje();
		System.out.println("empieza el testeo previo engau");
		System.out.println(especialista.getUser().getMail());
		System.out.println(cliente.getUser().getMail());
		mensaje.setCliente(cliente);
		mensaje.setEspecialista(especialista);
		mensaje.setAsunto("Urgencia: '" + publicacionUrgente.getDescripcion() + "'");
		mensaje.setRemitente('C');
		mensaje.setUrgencia(true);
		mensaje.setServicioUrgente(publicacionUrgente);

		model.addAttribute("mensaje", mensaje);
		crearHeader(model);
		return "obaju/mensaje";
	}

	@RequestMapping(value = { "/contratar-{idPublicacion}" }, method = RequestMethod.GET)
	public String contratarServicio(@PathVariable int idPublicacion, Model model) {
		Servicio servicioContratado = new Servicio();
		Publicacion publicacionContratada = publicacionService.getByKeyPublicaciones(idPublicacion);
		Timestamp fechaHoraActual = new Timestamp(System.currentTimeMillis());
		User user = userService.getbyUsername(getPrincipal());
		Cliente cliente = clienteService.getByUser(user);
		servicioContratado.setPublicacion(publicacionContratada);
		servicioContratado.setFechaHora(fechaHoraActual);
		servicioContratado.setPrecio(publicacionContratada.getPrecio());
		servicioContratado.setCliente(cliente);
		// servicioService.save(servicioContratado);
		// mailService.sendEmail(
		// publicacionContratada.getEspecialista().getUser().getMail(),
		// "Han contratado uno de tus servicios en Fixit",
		// "El usuario '" +cliente.getUser().getUsername() + "' ha contratado tu
		// servicio de ' "+ publicacionContratada.getTitulo()+"'. Por favor enviale un
		// mensaje desde aqui URLDELMENSAje");
		Servicio servicio = servicioContratado;
		Mensaje mensaje = new Mensaje();
		mensaje.setAsunto(servicio.getPublicacion().getTitulo());
		mensaje.setCliente(cliente);
		mensaje.setCuerpo("Felicitaciones! Te he elejido para que lleves a cabo �ste trabajo");
		mensaje.setEspecialista(servicio.getPublicacion().getEspecialista());
		mensaje.setFechaHora(fechaHoraActual);
		mensaje.setPublicacion(servicio.getPublicacion());
		mensaje.setRemitente('C');
		mensaje.setServicio(servicio);
		mensaje.setServicioUrgente(null);
		mensaje.setUrgencia(false);
		mensajeService.save(mensaje);
		return "redirect:/servicioscontratados";
	}
	
	@RequestMapping(value = { "/publicar-servicio-urgente-{idPublicacionUrgente}" }, method = RequestMethod.GET)
	public String publicarServicioUrgente(@PathVariable int idPublicacionUrgente, Model model) {
		crearHeader(model);
		ServicioUrgente servicioUrgente;
		if (idPublicacionUrgente == 0) {
			servicioUrgente = new ServicioUrgente();
		} else {
			servicioUrgente = serviciosUrgentesService.getById(idPublicacionUrgente);
		}
		List<Categoria> subcategorias = categoriaService.findAllSubcategorias();
		model.addAttribute("servicioUrgente", servicioUrgente);
		model.addAttribute("subcategorias", subcategorias);
		return "obaju/publicar-servicio-urgente";
	}
	
	@RequestMapping(value = { "/publicar-servicio-urgente-{idPublicacion}" }, method = RequestMethod.POST)
	public String publicarServicioUrgentePost(@Valid ServicioUrgente servicioUrgente, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			System.out.println(result.getAllErrors());
			List<Categoria> subcategorias = categoriaService.findAllSubcategorias();
			model.addAttribute("subcategorias", subcategorias);
			return "obaju/publicar-servicio-urgente";
		}
		crearHeader(model);
		Timestamp fechaHoraActual = new Timestamp(System.currentTimeMillis());
		Timestamp fechaHoraLimite = new Timestamp(fechaHoraActual.getTime() + ((14 * 60) + 59) * 1000); // 14 min 59 seg
		System.out.println("la fecha y hora es: " + fechaHoraActual.toString());
		servicioUrgente.setDateTime(fechaHoraActual);
		servicioUrgente.setTiempoLimiteParaRespuesta(fechaHoraLimite);

		User user = userService.getbyUsername(getPrincipal());
		servicioUrgente.setUser(user);
		System.out.println("Entro bien en el post del publicar-servicio-urgente del POST");

		servicioUrgente.setEspecialista(null);
		try {
			System.out.println("La descripcion del servicio urgente es : "+ servicioUrgente.getDescripcion()+"--------------------------------------");
			System.out.println("Funciona la �����������");
			serviciosUrgentesService.saveServicioUrgente(servicioUrgente);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:serviciosurgentespedidos";
		// return "obaju/sucess";
	}


	@RequestMapping(value = "mensajes-recibidos-cliente", method = RequestMethod.GET)
	public String mensajesRecibidosCliente(Model model) {
		crearHeader(model);
		User user = userService.getbyUsername(getPrincipal());
		Cliente cliente = clienteService.getByUser(user);
		List<Mensaje> mensajes = mensajeService.findAll(cliente);
		ArrayList<Mensaje> list= new ArrayList<Mensaje>(mensajes);
		list.sort(new Comparator<Mensaje>() {

			@Override
			public int compare(Mensaje o1, Mensaje o2) {
				if (o1.getFechaHora().getTime()< o2.getFechaHora().getTime()) {
					return 1;
				} else if (o1.getFechaHora().getTime()> o2.getFechaHora().getTime()) {
					return -1;
				}
				return 0;
			}
		});
		model.addAttribute("mensajes", list);
		model.addAttribute("esCliente", true);
		System.out.println("Los mensajes son " + list);
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Mensaje mensaje = (Mensaje) iterator.next();
			if (mensaje != null) {
				System.out.println("Los mensajes son " + mensaje.getAsunto());
			}
		}
		return "obaju/mensajesChat";
	}

	public void crearHeader(Model model) {
		model.addAttribute("userLogged", getPrincipal());
		model.addAttribute("categoriasPadre", categoriaService.findAllCategoriasPadres());
		List<Categoria> list = categoriaService.findAllCategoriasPadres();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Categoria categoria = (Categoria) iterator.next();
			System.out.println(categoria.getNombre());
		}
		Publicacion publicacion = new Publicacion();
		model.addAttribute("busqueda", publicacion);
	}

	public void crearLateralEncabezado(Model model, int idCategoria) {
		if (idCategoria==0) {
			Categoria cat= new Categoria();
			cat.setNombre("Resultado de la busqueda");
			cat.setId(0);
			model.addAttribute("subcategoriaSeleccionada", cat);			
		} else {
			model.addAttribute("subcategoriaSeleccionada", categoriaService.getByKeyCategoria(idCategoria));
		}
		model.addAttribute("categoriasPadres", categoriaService.findAllCategoriasPadres());
		model.addAttribute("ciudades", ciudadService.findAll());
	}

	private void activarFuncionesEspecialistas(Model model) {
		boolean agregarFuncionesEspecialista = false;
		Cliente clienteLogged = clienteService.getByUser(userService.getbyUsername(getPrincipal()));
		Set<Rol> rolesUsuario = userService.getbyUsername(getPrincipal()).getRoles();
		for (Iterator iterator = rolesUsuario.iterator(); iterator.hasNext();) {
			Rol rol = (Rol) iterator.next();
			System.out.println("el rol definido es: " + rol.getType());
			if (rol.getType().equals("Especialista")) {
				agregarFuncionesEspecialista = true;
			}
		}
		if (agregarFuncionesEspecialista == false) { // Se hace para que aparezcas las opciones en el menu lateral,
														// segun si es especialista o un cliente
			System.out.println("entro en verdadero el agregar funciones");
			model.addAttribute("activarLateralEspecialista", "style=\"display: none;\"");
		}
	}

	@ModelAttribute("contNotificacionesUrgentes")
	public int getcantidadNotificacionesUrgentes() {
		User user = userService.getbyUsername(getPrincipal());
		List<ServicioUrgente> listaUrgentes = serviciosUrgentesService.findAllByUser(user);
		int contNotificaciones = 0;
		for (Iterator iterator = listaUrgentes.iterator(); iterator.hasNext();) {
			ServicioUrgente servicioUrgente = (ServicioUrgente) iterator.next();
			if (servicioUrgente.isNotificacionLeida() == false & servicioUrgente.getEspecialista() != null) {
				contNotificaciones++;
			}
		}
		return contNotificaciones;
	}

	@ModelAttribute("activarLateralEspecialista")
	public String mostrarLateralEsp() {
		if (!getPrincipal().equals("Login")) {
			boolean agregarFuncionesEspecialista = false;
			Cliente clienteLogged = clienteService.getByUser(userService.getbyUsername(getPrincipal()));
			Set<Rol> rolesUsuario = userService.getbyUsername(getPrincipal()).getRoles();
			for (Iterator iterator = rolesUsuario.iterator(); iterator.hasNext();) {
				Rol rol = (Rol) iterator.next();
				System.out.println("el rol definido es: " + rol.getType());
				if (rol.getType().equals("Especialista")) {
					agregarFuncionesEspecialista = true;
				}
			}
			if (agregarFuncionesEspecialista == false) { // Se hace para que aparezcas las opciones en el menu lateral,
															// segun si es especialista o un cliente
				System.out.println("entro en verdadero el agregar funciones");
				return "style=\"display: none;\"";
			}
		}
		return null;
	}

	@ModelAttribute("contNotificacionesMisServiciosUrgentes")
	public int getcantidadNotificacionesMisServiciosUrgentes() {
		if (!getPrincipal().equals("Login")) {
			User user = userService.getbyUsername(getPrincipal());
			ArrayList<Rol> userArray = new ArrayList<Rol>(user.getRoles());
			if (userArray.get(0).getType().equals("Especialista")) {
				Cliente cliente = clienteService.getByUser(user);
				Especialista especialista = especialistaService.getByUsernameEspecialista(user);
				ArrayList<ServicioUrgente> listUrgentes = (ArrayList<ServicioUrgente>) serviciosUrgentesService
						.getServiciosUrgentesMenoresDistancia(cliente.getLatitud(), cliente.getLongitud(),
								especialista.getId());
				List<Publicacion> listPublicacionesEsp = publicacionService
						.findAllPublicacionesActivasByEspecialista(especialista);
				ArrayList<ServicioUrgente> listUrgentesFiltrado = new ArrayList<ServicioUrgente>();
				for (ServicioUrgente servicioUrgente : listUrgentes) {
					boolean band = false;
					for (Iterator iterator = listPublicacionesEsp.iterator(); iterator.hasNext();) {
						Publicacion publicacion = (Publicacion) iterator.next();
						if (publicacion.getCategoria().getId() == servicioUrgente.getCategoria().getId()
								&& servicioUrgente.getEspecialista() == null) {
							band = true;
						}
					}
					if (band == true) {
						listUrgentesFiltrado.add(servicioUrgente);
					}
				}
				return listUrgentesFiltrado.size();
			}

		}
		return 0;
	}

}
