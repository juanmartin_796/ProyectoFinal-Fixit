<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<link href="css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">



<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">


</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>My orders</li>
					</ul>

				</div>

				<div class="col-md-3">
					<!-- *** CUSTOMER MENU ***
 _________________________________________________________ -->
					<div class="panel panel-default sidebar-menu">

						<div class="panel-heading">
							<h3 class="panel-title">Customer section</h3>
						</div>

						<div class="panel-body">

							<ul class="nav nav-pills nav-stacked">
								<li class="active"><a href="customer-orders.html"><i
										class="fa fa-list"></i> My orders</a></li>
								<li><a href="customer-wishlist.html"><i
										class="fa fa-heart"></i> My wishlist</a></li>
								<li><a href="customer-account.html"><i
										class="fa fa-user"></i> My account</a></li>
								<li><a href="index.html"><i class="fa fa-sign-out"></i>
										Logout</a></li>
							</ul>
						</div>

					</div>
					<!-- /.col-md-3 -->

					<!-- *** CUSTOMER MENU END *** -->
				</div>

				<div class="col-md-9" id="customer-orders">
					<div class="box">
					<h2> ${especialista.nombre}	${especialista.apellido}  <a href="#">(ver perfil)</a></h2>

						<!--                         <p class="lead">Seleccione los servicios urgentes a los cuales puede atender inmediatamente.</p> -->
						<!--                         <p class="text-muted"> Deber� acudir lo antes posible, en caso contrario podria ser penalizado con el bloqueo de su cuenta <a href="contact.html">contact us</a>, our customer service center is working for you 24/7.</p> -->

						<hr>

						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Fecha</th>
										<th>Servicio</th>
										<th>Categoria</th>
										<th>Calificacion</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listTrabajosRealizados}" var="servicio">
										<tr>
											<td>${fn:substring(servicio.fechaHoraFormateada,0,8)}</td>
											<td><a
												href="detail-publicacion-${servicio.publicacion.id}">${servicio.publicacion.titulo}
											</a></td>
											<th>${servicio.publicacion.categoria.nombre}</th>
											<td><input disabled="disabled" id="input-21e" value="${servicio.calificacion }" type="text"
												class="rating" data-min=0 data-max=5 data-step=0.5
												data-size="xs" title=""></td>
										</tr>
									</c:forEach>

									<tr>
										<th># 1735</th>
										<td>22/06/2013</td>
										<td>$ 150.00</td>
										<td><span class="label label-info">Being prepared</span>
										</td>
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***
 _________________________________________________________ -->
		<%@include file="footer.jsp" %>
	
	<script src="js/star-rating.js" type="text/javascript"></script>
	
</body>

</html>
