<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- Estilo para la calificacion -->
<link href="css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">


<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<!-- Estilo para la calificacion -->
<link rel="stylesheet" href="css/starrr.css">

</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Mi historial</li>
					</ul>

				</div>

				<div class="col-md-3">
					<!-- *** CUSTOMER MENU ***
 _________________________________________________________ -->
					<div class="panel panel-default sidebar-menu">

						<div class="panel-heading">
							<h3 class="panel-title">Nombre del usuario</h3>
						</div>

						<div class="panel-body">

							<ul class="nav nav-pills nav-stacked">
								<li class="active"><a href="servicioscontratados.html"><i
										class="fa fa-list"></i> Mi historial</a></li>
								<li><a href="#"><i
										class="fa fa-heart"></i> Favoritos</a></li>
								<li><a href="#"><i
										class="fa fa-user"></i> Perfil</a></li>
								<li><a href="index.html"><i class="fa fa-sign-out"></i>
										Salir</a></li>
							</ul>
						</div>

					</div>
					<!-- /.col-md-3 -->

					<!-- *** CUSTOMER MENU END *** -->
				</div>

				<div class="col-md-9" id="customer-orders">
					<div class="box">
						<h1>Historial de servicios contratados</h1>

						<!--                         <p class="lead">Seleccione los servicios urgentes a los cuales puede atender inmediatamente.</p> -->
						<!--                         <p class="text-muted"> Deber� acudir lo antes posible, en caso contrario podria ser penalizado con el bloqueo de su cuenta <a href="contact.html">contact us</a>, our customer service center is working for you 24/7.</p> -->

						<hr>

						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Fecha y hora</th>
										<th>Servicio</th>
										<th>Categoria</th>
										<th>Pago</th>
										<th>Calificacion</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listServiciosContratados}"
										var="servicioContratado">
										<tr>
											<td>${servicioContratado.fechaHoraFormateada}hs</td>
											<td><a
												href="detail-publicacion-${servicioContratado.publicacion.id}">${servicioContratado.publicacion.titulo}
											</a></td>
											<th>${servicioContratado.publicacion.categoria.nombre}</th>
											<td>$ ${servicioContratado.precio}</td>
											<td width="250px">
<%-- 											<div class='starrr' value="${servicioContratado.id}" --%>
<%-- 													id='stars${servicioContratado.id}' --%>
<!-- 													onclick="setCalificacion(this); return false;"></div> <span -->
<%-- 												id="count${servicioContratado.id}">0</span> --%>
												<input id="calificacion${servicioContratado.id}" name="${servicioContratado.id}" value="${servicioContratado.calificacion }" type="text"
												class="rating" data-min=0 data-max=5 data-step=0.5
												data-size="xs" title="" onchange="setCalificacion(this); return false;"/>
												</td>
										</tr>
									</c:forEach>

									<tr>
										<th># 1735</th>
										<td>22/06/2013</td>
										<td>$ 150.00</td>
										<td><span class="label label-info">Being prepared</span>
										</td>
										<td><a href="customer-order.html"
											class="btn btn-primary btn-sm">View</a></td>
									</tr>
									<tr>
										<th># 1735</th>
										<td>22/06/2013</td>
										<td>$ 150.00</td>
										<td><span class="label label-success">Received</span></td>
										<td><a href="customer-order.html"
											class="btn btn-primary btn-sm">View</a></td>
									</tr>
									<tr>
										<th># 1735</th>
										<td>22/06/2013</td>
										<td>$ 150.00</td>
										<td><span class="label label-danger">Cancelled</span></td>
										<td><a href="customer-order.html"
											class="btn btn-primary btn-sm">View</a></td>
									</tr>
									<tr>
										<th># 1735</th>
										<td>22/06/2013</td>
										<td>$ 150.00</td>
										<td><span class="label label-warning">On hold</span></td>
										<td><a href="customer-order.html"
											class="btn btn-primary btn-sm">View</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***
 _________________________________________________________ -->
		<%@include file="footer.jsp" %>

	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->

	
		<script src="js/star-rating.js" type="text/javascript"></script>

	<script>
		function setCalificacion(obj) {
		    $('#'+obj.getAttribute('id')).on('rating:change', function(event, value, caption) {
		    	
		        console.log(value);
		        console.log(caption);
		        var idServicio = obj.getAttribute("name");
		        
		        var token = $("meta[name='_csrf']").attr("content");
				var header = $("meta[name='_csrf_header']").attr("content");
				
				var datos = {
					id : idServicio,
					calificacion : value
				};

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : 'calificar-servicio2',
					data : JSON.stringify(datos),
					dataType : 'json',
					timeout : 100000,
					beforeSend: function(request) {
					    request.setRequestHeader(header, token);
					 	},
					success : function(datos) {
						console.log("SUCCESS: ", datos);
						display(data);
					},
					error : function(e) {
						console.log("ERROR: ", e);
						display(e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
		    });
		}
	</script>

	 

</body>

</html>

