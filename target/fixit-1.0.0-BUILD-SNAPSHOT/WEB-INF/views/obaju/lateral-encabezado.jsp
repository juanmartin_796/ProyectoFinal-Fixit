<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body>

	<div class="col-md-12">
		<ul class="breadcrumb">
			<li><a href="#">Inicio</a></li>
			<li>${subcategoriaSeleccionada.nombre}</li>
		</ul>
	</div>

	<div class="col-md-3">
		<!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
		<div class="panel panel-default sidebar-menu">

			<div class="panel-heading">
				<h3 class="panel-title">Categorias</h3>
			</div>

			<div class="panel-body">
				<ul class="nav nav-pills nav-stacked category-menu">
					<!-- 								<li><a href="category.html">Men <span -->
					<!-- 										class="badge pull-right">42</span></a> -->
					<!-- 									<ul> -->
					<!-- 										<li><a href="category.html">T-shirts</a></li> -->
					<!-- 										<li><a href="category.html">Shirts</a></li> -->
					<!-- 										<li><a href="category.html">Pants</a></li> -->
					<!-- 										<li><a href="category.html">Accessories</a></li> -->
					<!-- 									</ul></li> -->



					<c:forEach items="${categoriasPadres}" var="categoriaPadre">
						<c:choose>
							<c:when
								test="${categoriaPadre.id == subcategoriaSeleccionada.id}">
								<li class="active"><a
									href="category-${categoriaPadre.id} }.html">${subcategoriaSeleccionada.nombre}
										<span class="badge pull-right">123</span>
								</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="category-${categoriaPadre.id}.html">${categoriaPadre.nombre}
										<span class="badge pull-right">42</span>
								</a></li>
							</c:otherwise>
						</c:choose>
						<ul>
							<c:forEach items="${categoriaPadre.subCategorias}"
								var="subcategoria">
								<li><a href="category-${subcategoria.id }.html">${subcategoria.nombre }</a>
								</li>
							</c:forEach>
						</ul>
						</li>
					</c:forEach>
				</ul>

			</div>
		</div>

		<div class="panel panel-default sidebar-menu">

			<div class="panel-heading">
				<h3 class="panel-title">
					Filtros <a class="btn btn-xs btn-danger pull-right" href="#"><i
						class="fa fa-times-circle"></i> Limpiar</a>
				</h3>
			</div>

			<div class="panel-body">

				<form>
					<div class="form-group">
						<div class="checkbox">
							<label> <input type="checkbox">Resistencia (10)
							</label>
						</div>
						<div class="checkbox">
							<label> <input type="checkbox">Corrientes (12)
							</label>
						</div>
						<div class="checkbox">
							<label> <input type="checkbox">Misiones (15)
							</label>
						</div>
						<div class="checkbox">
							<label> <input type="checkbox">Saenz Pe�a (14)
							</label>
						</div>
					</div>

					<button class="btn btn-default btn-sm btn-primary">
						<i class="fa fa-pencil"></i> Aplicar
					</button>

				</form>

			</div>
		</div>

		<!-- 					<div class="panel panel-default sidebar-menu"> -->

		<!-- 						<div class="panel-heading"> -->
		<!-- 							<h3 class="panel-title"> -->
		<!-- 								Colours <a class="btn btn-xs btn-danger pull-right" href="#"><i -->
		<!-- 									class="fa fa-times-circle"></i> Clear</a> -->
		<!-- 							</h3> -->
		<!-- 						</div> -->

		<!-- 						<div class="panel-body"> -->

		<!-- 							<form> -->
		<!-- 								<div class="form-group"> -->
		<!-- 									<div class="checkbox"> -->
		<!-- 										<label> <input type="checkbox"> <span -->
		<!-- 											class="colour white"></span> White (14) -->
		<!-- 										</label> -->
		<!-- 									</div> -->
		<!-- 									<div class="checkbox"> -->
		<!-- 										<label> <input type="checkbox"> <span -->
		<!-- 											class="colour blue"></span> Blue (10) -->
		<!-- 										</label> -->
		<!-- 									</div> -->
		<!-- 									<div class="checkbox"> -->
		<!-- 										<label> <input type="checkbox"> <span -->
		<!-- 											class="colour green"></span> Green (20) -->
		<!-- 										</label> -->
		<!-- 									</div> -->
		<!-- 									<div class="checkbox"> -->
		<!-- 										<label> <input type="checkbox"> <span -->
		<!-- 											class="colour yellow"></span> Yellow (13) -->
		<!-- 										</label> -->
		<!-- 									</div> -->
		<!-- 									<div class="checkbox"> -->
		<!-- 										<label> <input type="checkbox"> <span -->
		<!-- 											class="colour red"></span> Red (10) -->
		<!-- 										</label> -->
		<!-- 									</div> -->
		<!-- 								</div> -->

		<!-- 								<button class="btn btn-default btn-sm btn-primary"> -->
		<!-- 									<i class="fa fa-pencil"></i> Apply -->
		<!-- 								</button> -->

		<!-- 							</form> -->

		<!-- 						</div> -->
		<!-- 					</div> -->

		<!-- *** MENUS AND FILTERS END *** -->

		<!-- 					<div class="banner"> -->
		<!-- 						<a href="#"> <img src="img/banner.jpg" alt="sales 2014" -->
		<!-- 							class="img-responsive"> -->
		<!-- 						</a> -->
		<!-- 					</div> -->
	</div>

</body>
</html>